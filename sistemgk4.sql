/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.11 : Database - sistemgk4
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sistemgk4` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sistemgk4`;

/*Table structure for table `audit` */

DROP TABLE IF EXISTS `audit`;

CREATE TABLE `audit` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(50) DEFAULT NULL,
  `column_name` varchar(50) DEFAULT NULL,
  `old_data` varchar(50) DEFAULT NULL,
  `new_data` varchar(50) DEFAULT NULL,
  `time_stamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `butir_kontrak` */

DROP TABLE IF EXISTS `butir_kontrak`;

CREATE TABLE `butir_kontrak` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_indent` varchar(50) DEFAULT NULL,
  `no_kontrak` varchar(50) DEFAULT NULL,
  `id_syarikat` int(10) DEFAULT NULL,
  `had_bumbung` double DEFAULT NULL,
  `revenue_kontrak` double DEFAULT NULL,
  `tarikh_mula` date DEFAULT NULL,
  `tarikh_tamat_kontrak` date DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_indent` (`no_indent`),
  KEY `no_kontrak` (`no_kontrak`),
  KEY `no_indent` (`no_indent`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

/*Table structure for table `maklumat_agsv_agse` */

DROP TABLE IF EXISTS `maklumat_agsv_agse`;

CREATE TABLE `maklumat_agsv_agse` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_daftar` varchar(50) DEFAULT NULL,
  `jenis_agsv_agse` varchar(200) DEFAULT NULL,
  `url_gambar` varchar(200) DEFAULT NULL,
  `tarikh_masuk_khidmat` date DEFAULT NULL,
  `tarikh_serah_terima` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

/*Table structure for table `maklumat_indent` */

DROP TABLE IF EXISTS `maklumat_indent`;

CREATE TABLE `maklumat_indent` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_indent` varchar(50) DEFAULT NULL,
  `no_kontrak` varchar(50) DEFAULT NULL,
  `jenis_indent` varchar(50) DEFAULT NULL,
  `jumlah_indent` double DEFAULT NULL,
  `baki_had_bumbumg` double DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `no_indent` (`no_indent`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `maklumat_syarikat` */

DROP TABLE IF EXISTS `maklumat_syarikat`;

CREATE TABLE `maklumat_syarikat` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama_syarikat` varchar(50) DEFAULT NULL,
  `kod_bidang` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Table structure for table `mesy_jawatan_kuasa` */

DROP TABLE IF EXISTS `mesy_jawatan_kuasa`;

CREATE TABLE `mesy_jawatan_kuasa` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tajuk_mesy` varchar(50) DEFAULT NULL,
  `tarikh_mesy` datetime DEFAULT NULL,
  `ahli1` varchar(50) DEFAULT NULL,
  `ahli2` varchar(50) DEFAULT NULL,
  `ahli3` varchar(50) DEFAULT NULL,
  `siri` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `minit_mesy` */

DROP TABLE IF EXISTS `minit_mesy`;

CREATE TABLE `minit_mesy` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_mesy` int(10) DEFAULT NULL,
  `tajuk_mesy` varchar(50) DEFAULT NULL,
  `mula` datetime DEFAULT NULL,
  `habis` datetime DEFAULT NULL,
  `jangkamasa` double DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `minit_mesy_ibfk_1` (`id_mesy`),
  CONSTRAINT `minit_mesy_ibfk_1` FOREIGN KEY (`id_mesy`) REFERENCES `mesy_jawatan_kuasa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `sejarah_pembaikan` */

DROP TABLE IF EXISTS `sejarah_pembaikan`;

CREATE TABLE `sejarah_pembaikan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_indent` varchar(50) DEFAULT NULL,
  `id_agsv_agse` int(10) DEFAULT NULL,
  `jenis_pembaikan` varchar(50) DEFAULT NULL,
  `lst_alat_ganti` varchar(50) DEFAULT NULL,
  `qty_alat_ganti` int(5) DEFAULT NULL,
  `harga_alat_ganti` double DEFAULT NULL,
  `tarikh_terima` date DEFAULT NULL,
  `tarikh_siap` date DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_agsv_agse` (`id_agsv_agse`),
  KEY `id_indent` (`no_indent`),
  CONSTRAINT `sejarah_pembaikan_ibfk_3` FOREIGN KEY (`id_agsv_agse`) REFERENCES `maklumat_agsv_agse` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `tajaan_mesy` */

DROP TABLE IF EXISTS `tajaan_mesy`;

CREATE TABLE `tajaan_mesy` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_indent` varchar(50) DEFAULT NULL,
  `id_mesy` int(10) DEFAULT NULL,
  `id_syarikat` int(10) DEFAULT NULL,
  `id_agse_agsv` int(10) DEFAULT NULL,
  `no_kontrak` varchar(50) DEFAULT NULL,
  `jenis_tajaan` varchar(50) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `edd_serah` date DEFAULT NULL,
  `edd_terima` date DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tajaan_mesy_ibfk_3` (`id_agse_agsv`),
  KEY `tajaan_mesy_ibfk_1` (`id_mesy`),
  KEY `tajaan_mesy_ibfk_2` (`id_syarikat`),
  KEY `id_kontrak` (`no_kontrak`),
  KEY `no_indent` (`no_indent`),
  CONSTRAINT `tajaan_mesy_ibfk_1` FOREIGN KEY (`id_mesy`) REFERENCES `mesy_jawatan_kuasa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tajaan_mesy_ibfk_2` FOREIGN KEY (`id_syarikat`) REFERENCES `maklumat_syarikat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tajaan_mesy_ibfk_3` FOREIGN KEY (`id_agse_agsv`) REFERENCES `maklumat_agsv_agse` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tajaan_mesy_ibfk_4` FOREIGN KEY (`no_kontrak`) REFERENCES `butir_kontrak` (`no_kontrak`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT '10',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
