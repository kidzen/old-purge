/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.11 : Database - raradb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`raradb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `raradb`;

/*Table structure for table `account2` */

DROP TABLE IF EXISTS `account2`;

CREATE TABLE `account2` (
  `id` int(11) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `item` varchar(50) DEFAULT NULL,
  `description` text,
  `cash_out` double unsigned zerofill DEFAULT NULL,
  `cash_in` double unsigned zerofill DEFAULT NULL,
  `notes` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `id` */

DROP TABLE IF EXISTS `id`;

CREATE TABLE `id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `raw_material2` */

DROP TABLE IF EXISTS `raw_material2`;

CREATE TABLE `raw_material2` (
  `id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(50) NOT NULL,
  `stock` varchar(50) DEFAULT NULL,
  `description` text,
  `brand` varchar(50) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `quantity` int(10) unsigned zerofill DEFAULT NULL,
  `unit_price` double unsigned zerofill DEFAULT NULL,
  `notes` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `t_limiter` */

DROP TABLE IF EXISTS `t_limiter`;

CREATE TABLE `t_limiter` (
  `id` int(10) unsigned NOT NULL,
  `grouper` int(10) unsigned NOT NULL,
  `value` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_limiter_grouper_id` (`grouper`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
