SELECT 
	student.`name`
--	,`relation`.id
--	, relation.`parent_id`
--	, relation.`student_id`
	, relation.`relation`
	,parent.`name`
FROM `person` student
LEFT JOIN `relation` ON student.`id` = `relation`.`student_id` 
LEFT JOIN `person` parent ON parent.`id` = `relation`.`parent_id` 
WHERE student.`type`=1;


SELECT 
	`student`.name 
	,relation.`relation`
	,parent.name
FROM `person` student
LEFT JOIN `relation` ON `student`.`id` = `relation`.`student_id` 
LEFT JOIN `person` `parent` ON `parent`.`id` = `relation`.`parent_id` 
-- WHERE `student`.`type`=1
;

SELECT `person`.* 
FROM `person` 
LEFT JOIN `relation` ON `person`.`id` = `relation`.`parent_id` 
LEFT JOIN `person` `parent` ON `relation`.`student_id` = `parent`.`id`
;

SELECT `person`.name,relation.relation,parent.name 
FROM `person` 
LEFT JOIN `relation` ON `person`.`id` = `relation`.`student_id` 
LEFT JOIN `person` `parent` ON `relation`.`parent_id` = `parent`.`id`
;

SELECT `person`.* 
FROM `person` 
LEFT JOIN `relation` ON `person`.`id` = `relation`.`parent_id` 
LEFT JOIN `person` `parent` ON `relation`.`student_id` = `parent`.`id`
;
 CREATE VIEW studentwithparent AS (
  SELECT 
	 @rownum := @rownum + 1 AS id
	,relation.id relation_id
	,student.id student_id 
	,student.type student_type
	,student.ic_no student_type
	,student.email student_email
	,student.address student_address
	,student.phone_no student_phone_no
	,student.spm student_spm
	,student.status student_status
	,student.deleted student_deleted
	,student.created_at student_created_at
	,student.updated_at student_updated_at
	,student.deleted_at student_deleted_at
	,parent.id student_id 
	,parent.type student_type
	,parent.ic_no student_type
	,parent.email student_email
	,parent.address student_address
	,parent.phone_no student_phone_no
	,parent.spm student_spm
	,parent.status student_status
	,parent.deleted student_deleted
	,parent.created_at student_created_at
	,parent.updated_at student_updated_at
	,parent.deleted_at student_deleted_at
	,relation.id relation_id
	,relation.relation relation_relation
	,relation.student_id relation_student_id
	,relation.parent_id relation_parent_id
  FROM `person` student 
  JOIN (SELECT @rownum := 0) r 
  LEFT JOIN `relation` ON `student`.`id` = `relation`.`student_id` 
  LEFT JOIN `person` `parent` ON `relation`.`parent_id` = `parent`.`id`
  WHERE student.type = 1
  ORDER BY student.id,parent.id
 )
;


