with data1 as (
-- core data
select 
  d."name" dept
  ,ds."name" district
  ,trunc(dc."created_at") "date"
  ,trunc(cr."date") "trial_date"
--  ,trunc(cc."trial_date") "date"
--  ,trunc(dc."commited_date") "date2"
  ,cs."name" "trial_type"
  ,dc."file_no" "file_dept"
--  ,cc."department_file_id"
  ,cc."file_no"
  ,cs."type" "is_done"
from "department_case" dc
left join "department" d on d."id" = dc."department_id"
left join "court_case" cc on cc."id" = dc."id"
left join "case_record" cr on cr."case_id" = dc."id"
left join "case_status" cs on cr."case_status_id" = cs."id"
left join "district" ds on ds."id" = dc."district_id"
)
,  data2 as 
(
-- data processing
select 
  data1.*
  ,count("trial_type") over (partition by "trial_type",district,dept,"trial_date") kes
  ,count("file_no") over (partition by district,dept,"trial_date") kes_mahkamah
  ,count("file_dept") over (partition by district,dept,"trial_date") semua_kes 
  ,count("file_no") over (partition by district,dept,"trial_date") kes_diserah
from data1
)
,  data3 as ( 
-- data manipulation
select  --distinct
  -- data2.*,
  data2.district,data2."trial_date",data2.dept,data2."trial_type",data2.kes,data2.kes_mahkamah,data2.semua_kes,data2.kes_diserah
  ,(semua_kes - kes_diserah) kes_x_diserah
from data2
)
select distinct * from data3
--pivot (
--  sum(kes) for (district,"trial_type","trial_date") in (
--    ('Seberang Perai Tengah','BICARA','01-MAY-17') as "SPT_BICARA_1MAY"
--    ,('Seberang Perai Tengah','BICARA','08-MAY-17') as "SPT_BICARA_8MAY"
--    ,('Seberang Perai Tengah','MENGAKU SALAH','01-MAY-17') as "SPT_SALAH_1MAY"
--    ,('Seberang Perai Tengah','MENGAKU SALAH','08-MAY-17') as "SPT_SALAH_8MAY"
--    ,('Seberang Perai Tengah','TANGGUH','01-MAY-17') as "SPT_TANGGUH_1MAY"
--    ,('Seberang Perai Tengah','TANGGUH','08-MAY-17') as "SPT_TANGGUH_8MAY"
--    ,('Seberang Perai Utara','BICARA','01-MAY-17') as "SPU_BICARA_1MAY"
--    ,('Seberang Perai Utara','BICARA','08-MAY-17') as "SPU_BICARA_8MAY"
--    ,('Seberang Perai Utara','MENGAKU SALAH','01-MAY-17') as "SPU_SALAH_1MAY"
--    ,('Seberang Perai Utara','MENGAKU SALAH','08-MAY-17') as "SPU_SALAH_8MAY"
--    ,('Seberang Perai Utara','TANGGUH','01-MAY-17') as "SPU_TANGGUH_1MAY"
--    ,('Seberang Perai Utara','TANGGUH','08-MAY-17') as "SPU_TANGGUH_8MAY"
--    )
--  sum(kes) for ("trial_date") in ('01-MAY-17','08-MAY-17')
--  ,sum(kes_mahkamah) for ("trial_date") in ('01-MAY-17','08-MAY-17')
--  ,sum(semua_kes) for ("trial_date") in ('01-MAY-17','08-MAY-17')
--  ,sum(semua_kes) for ("trial_date") in ('01-MAY-17','08-MAY-17')
--)
--pivot (sum(kes_mahkamah) for ("trial_date") in ('01-MAY-17','08-MAY-17'))
--select distinct * from data1
order by district,dept,"trial_date"
;

select * from "department_case";
select * from "court_case";
select * from "case_record";
select * from "case_status";

select distinct next_day(to_date('01-01-2017','mm-dd-yyyy')-1+level,'Monday') All_MON from dual 
connect by level <= floor(to_date('12-31-2016','mm-dd-yyyy') - to_date('01-01-2016','mm-dd-yyyy'))
order by All_MON;

select distinct next_day(to_date('01-01-2017','mm-dd-yyyy')-1+level,'Monday') All_TUES from dual 
connect by level <= floor(to_date('12-31-2016','mm-dd-yyyy') - to_date('01-01-2016','mm-dd-yyyy'))
order by All_TUES;





with data1 as (
-- core data
select 
  d."name" dept
  ,ds."name" district
  ,trunc(dc."created_at") "date"
  ,trunc(cr."date") "trial_date"
--  ,trunc(cc."trial_date") "date"
--  ,trunc(dc."commited_date") "date2"
  ,cs."name" "trial_type"
  ,dc."file_no" "file_dept"
--  ,cc."department_file_id"
  ,cc."file_no"
  ,cs."type" "is_done"
from "department_case" dc
left join "department" d on d."id" = dc."department_id"
left join "court_case" cc on cc."id" = dc."id"
left join "case_record" cr on cr."case_id" = dc."id"
left join "case_status" cs on cr."case_status_id" = cs."id"
left join "district" ds on ds."id" = dc."district_id"
)
,  data2 as 
(
-- data processing
select 
  data1.*
--  ,count("trial_type") over (partition by "trial_type",district,dept,"trial_date") kes
--  ,count("file_no") over (partition by district,dept,"trial_date") kes_mahkamah
--  ,count("file_dept") over (partition by district,dept,"trial_date") semua_kes 
--  ,count("file_no") over (partition by district,dept,"trial_date") kes_diserah
from data1
)
,  data3 as  ( select distinct "trial_date"from data1 )
select * from data1
pivot (
  count("trial_type") for "trial_date" in ('01-MAY-17','08-MAY-17')
--  count("trial_type") for dept in ('Seberang Perai Tengah','Seberang Perai Utara')
)
--select distinct * from data1
--order by district,dept,"trial_date"
;





with data1 as (
-- core data
select 
  d."name" "dept"
  ,ds."name" "district"
  ,extract(year from cc."created_at") "year"
  ,extract(month from cc."created_at") "month"
  ,trunc(cc."created_at") "date"
  ,cc."file_no"
  ,cs."type" "is_done"
from "department_case" dc
left join "department" d on d."id" = dc."department_id"
left join "court_case" cc on cc."id" = dc."id"
left join "case_record" cr on cr."case_id" = dc."id"
left join "case_status" cs on cr."case_status_id" = cs."id"
left join "district" ds on ds."id" = dc."district_id"
where cc."file_no" is not null
)
, data2 as (
select distinct
  "district"
  ,"dept"
  ,"year"
  ,"month"
--  ,count(*) over (partition by "year") total_case_yearly --final
--  ,count(*) over (partition by "month","year") total_case_monthly -- bottom
  ,count(*) over (partition by "dept","year") total_case_by_dept --right
  ,count(*) over (partition by "month","dept","year") total_case_dept_monthly --right
from data1
)

select * from (
  select 
    *
  from data2
  pivot
  (
    sum(total_case_dept_monthly) for "year" in (2016,2017)
--    ,sum(total_case_dept_monthly) for "month" in (1 as Jan,2 as feb,6 as jun,12 as dec)
  )
)
--pivot
--(
--  sum(total_case_year) for "month" in (1 as Jan,2 as feb,6 as jun,12 as dec)
----  ,sum(total_case_dept_monthly) for "month" in (1 as Jan,2 as feb,6 as jun,12 as dec)
--)

;


with data1 as (
select 
  d."name" "dept"
  ,ds."name" "district"
  ,extract(year from cc."created_at") "year"
  ,extract(month from cc."created_at") "month"
  ,trunc(cc."created_at") "date"
  ,cc."file_no"
  ,cs."type" "is_done"
from "department_case" dc
left join "department" d on d."id" = dc."department_id"
left join "court_case" cc on cc."id" = dc."id"
left join "case_record" cr on cr."case_id" = dc."id"
left join "case_status" cs on cr."case_status_id" = cs."id"
left join "district" ds on ds."id" = dc."district_id"
where cc."file_no" is not null
)
select 
--  *
  count(case when "month" = 6 then 1 end)   "jun"
  ,count(case when "year" = 2017 then 1 end)   "2017"
  ,count(case when "dept" = 'DIREKTORAT PENGUATKUASAAN' then 1 end)   "DIREKTORAT PENGUATKUASAAN"
from data1
;

SELECT (case when EXTRACT(day from to_date("date",'DD-Mon-YY')) =  1 then "date" else NEXT_DAY('01-MAY-17','Monday') end) "date" from ;
SELECT NEXT_DAY('01-MAY-17','Monday') "Next_Day" from DUAL;

select distinct to_char(to_date("date",'DD-Mon-YY'),'Dy') as day from "monthly_report";

with data1 as (
select * from "monthly_report"
where EXTRACT(YEAR FROM "date") = '2017'
and EXTRACT(MONTH FROM "date") = 5
and "district" = 'Seberang Perai Tengah'
and "trial_type" is not null
order by "date","district","dept"
)
, data2 as (
select * from "monthly_report"
where EXTRACT(YEAR FROM "date") = '2017'
and EXTRACT(MONTH FROM "date") = 5
and "district" = 'Seberang Perai Tengah'
and "trial_type" is null
order by "date","district","dept"
)
select
  *
  t1."district"
  ,t1."date"
  ,t1."dept"
  ,t1."trial_type"
  ,t1."is_done"
  ,t1."jenis_kes"
  ,t1."kes_mahkamah"
  ,t1."semua_kes"
  ,t1."kes_diserah"
  ,t1."kes_x_diserah"
from data1 t1 full join data2 t2 on t1."date" = t2."date"
;


--------------------------------------------------------------------
------------------------------------View----------------------------------
--------------------------------------------------------------------

with data1 as (
-- core data
select 
  dc."id" "id"
  ,cc."id" "cid"
  ,d."name" "dept"
  ,ds."name" "district"
  ,coalesce(extract(year from cr."date"),extract(year from dc."created_at")) as "year"
  ,coalesce(extract(month from cr."date"),extract(month from dc."created_at")) as "month"
  ,(case when cs."name" is null then
  (case when to_char(to_date(trunc(dc."created_at"),'DD-Mon-YY'),'Dy') =  'Mon' then trunc(dc."created_at") else NEXT_DAY(trunc(dc."created_at"),'Monday') end) 
  else
  (case when to_char(to_date(trunc(cr."date"),'DD-Mon-YY'),'Dy') =  'Mon' then trunc(cr."date") else NEXT_DAY(trunc(cr."date"),'Monday') end)
  end) "date"
--  ,trunc(cr."date") "trial_date"
--  ,trunc(cc."trial_date") "date"
--  ,trunc(dc."commited_date") "date2"
  ,cs."name" "trial_type"
  ,dc."file_no" "file_dept"
--  ,cc."department_file_id"
  ,cc."file_no"
  ,cs."type" "is_done"
from "department_case" dc
left join "department" d on d."id" = dc."department_id"
left join "court_case" cc on cc."id" = dc."id"
left join "case_record" cr on cr."case_id" = dc."id"
left join "case_status" cs on cr."case_status_id" = cs."id"
left join "district" ds on ds."id" = dc."district_id"
)
,  data2 as 
(
-- data processing
select 
  data1.*
  ,count("trial_type") over (partition by "trial_type","district","dept","date") jenis_kes
  ,count("file_no") over (partition by "district","dept","date") kes_mahkamah
  ,count("file_dept") over (partition by "district","dept","date") semua_kes 
  ,count("file_no") over (partition by "district","dept","date") kes_diserah

  ,count("trial_type") over (partition by "district","dept","trial_type","month","year") jumlah_jenis_kes
  ,count("file_no") over (partition by "district","dept","month","year") jumlah_kes_mahkamah
  ,count("file_dept") over (partition by "district","dept","month","year") jumlah_semua_kes 
  ,count("file_no") over (partition by "district","dept","month","year") jumlah_kes_diserah
from data1
)
,  data3 as ( 
select  --distinct
--   data2.*,
   data2."id",
   data2."cid",
  data2."district" "district"
  ,"year","month","date"
--  ,data2."trial_date"
  ,data2."dept" "dept"
  ,data2."trial_type"
  ,data2."is_done"
  ,data2.jenis_kes "jenis_kes"
  ,data2.kes_mahkamah "kes_mahkamah"
  ,data2.semua_kes "semua_kes"
  ,data2.kes_diserah "kes_diserah"
  ,(semua_kes - kes_diserah) "kes_x_diserah"
  ,data2.jumlah_jenis_kes "jumlah_jenis_kes"
  ,data2.jumlah_kes_mahkamah "jumlah_kes_mahkamah"
  ,data2.jumlah_semua_kes "jumlah_semua_kes"
  ,data2.jumlah_kes_diserah "jumlah_kes_diserah"
  ,(jumlah_semua_kes - jumlah_kes_diserah) "jumlah_kes_x_diserah"
  ,(kes_diserah- "is_done") "kes_dibawa_kedepan"
  ,(jumlah_kes_diserah- sum("is_done") over (partition by "district","dept","month","year")) "jumlah_kes_dibawa_kedepan"
from data2
)
,  data4 as ( 
-- data manipulation
select  distinct
  data3.*
from data3
)
,data5 as (
select data4.* from data4
where "trial_type" is null
--and EXTRACT(YEAR FROM "date") = '2017'
--and EXTRACT(MONTH FROM "date") = 5
--and "district" = 'Seberang Perai Tengah'
order by "date","district","dept"
)
,data6 as (
select data4.* from data4
where "trial_type" is not null
--and EXTRACT(YEAR FROM "date") = '2017'
--and EXTRACT(MONTH FROM "date") = 5
--and "district" = 'Seberang Perai Tengah'
order by "date","district","dept"
)
--select * from data1;
select 
  *
--  coalesce(t1."district",t2."district") "district"
--  ,coalesce(t1."year",t2."year") "year"
--  ,coalesce(t1."month",t2."month") "month"
--  ,coalesce(t1."date",t2."date") "date"
from data5 t1 full join data6 t2 on t1."date" = t2."date"
;


--------------------------------------------------------------------
------------------------------------View----------------------------------
--------------------------------------------------------------------

with data1 as (
-- core data
select 
  d."name" "dept"
  ,ds."name" "district"
  ,coalesce(extract(year from cr."date"),extract(year from dc."created_at")) as "year"
  ,coalesce(extract(month from cr."date"),extract(month from dc."created_at")) as "month"
  ,(case when cs."name" is null then
  (case when to_char(to_date(trunc(dc."created_at"),'DD-Mon-YY'),'Dy') =  'Mon' then trunc(dc."created_at") else NEXT_DAY(trunc(dc."created_at"),'Monday') end) 
  else
  (case when to_char(to_date(trunc(cr."date"),'DD-Mon-YY'),'Dy') =  'Mon' then trunc(cr."date") else NEXT_DAY(trunc(cr."date"),'Monday') end)
  end) "date"
--  ,trunc(cr."date") "trial_date"
--  ,trunc(cc."trial_date") "date"
--  ,trunc(dc."commited_date") "date2"
  ,cs."name" "trial_type"
  ,dc."file_no" "file_dept"
--  ,cc."department_file_id"
  ,cc."file_no"
  ,cs."type" "is_done"
from "department_case" dc
left join "department" d on d."id" = dc."department_id"
left join "court_case" cc on cc."id" = dc."id"
left join "case_record" cr on cr."case_id" = dc."id"
left join "case_status" cs on cr."case_status_id" = cs."id"
left join "district" ds on ds."id" = dc."district_id"
)
,  listOfDate as 
(
-- data processing
select distinct
--  data1.*
  data1."dept",data1."district",data1."year",data1."month",data1."date"
--  ,sum(case when "trial_type" is not null then 1 end) over (partition by "trial_type","district","dept","date") jenis_kes
--  ,count("trial_type") over (partition by "trial_type","district","dept","date") jenis_kes
--  ,count("file_no") over (partition by "district","dept","date") kes_mahkamah
--  ,count("file_dept") over (partition by "district","dept","date") semua_kes 
--  ,count("file_no") over (partition by "district","dept","date") kes_diserah

--  ,count("trial_type") over (partition by "district","dept","trial_type","month","year") jumlah_jenis_kes
--  ,count("file_no") over (partition by "district","dept","month","year") jumlah_kes_mahkamah
--  ,count("file_dept") over (partition by "district","dept","month","year") jumlah_semua_kes 
--  ,count("file_no") over (partition by "district","dept","month","year") jumlah_kes_diserah
from data1
)
, data2 as (
select 
  data1.*
  ,count("trial_type") over (partition by "trial_type","district","dept","date") jenis_kes
  ,count("file_no") over (partition by "district","dept","date") kes_mahkamah
  ,count("file_dept") over (partition by "district","dept","date") semua_kes 
  ,count("file_no") over (partition by "district","dept","date") kes_diserah

  ,count("trial_type") over (partition by "district","dept","trial_type","month","year") jumlah_jenis_kes
  ,count("file_no") over (partition by "district","dept","month","year") jumlah_kes_mahkamah
  ,count("file_dept") over (partition by "district","dept","month","year") jumlah_semua_kes 
  ,count("file_no") over (partition by "district","dept","month","year") jumlah_kes_diserah
from data1
)
, data3 as (
select 
  data2.*
  ,(semua_kes - kes_diserah) kes_x_diserah
  ,(jumlah_semua_kes - jumlah_kes_diserah) jumlah_kes_x_diserah
  ,(kes_diserah- "is_done") kes_dibawa_kedepan
  ,(jumlah_kes_diserah- sum("is_done") over (partition by "district","dept","month","year")) jumlah_kes_dibawa_kedepan
from data2
)
, dataCase as (
select distinct 
  data3."dept",data3."district",data3."year",data3."date",data3."trial_type",data3."is_done"
  ,data3.jenis_kes "jenis_kes"
  ,data3.kes_mahkamah "kes_mahkamah"
  ,data3.semua_kes "semua_kes"
  ,data3.kes_diserah "kes_diserah"
  ,data3.kes_x_diserah "kes_x_diserah"
  ,data3.jumlah_jenis_kes "jumlah_jenis_kes"
  ,data3.jumlah_kes_mahkamah "jumlah_kes_mahkamah"
  ,data3.jumlah_semua_kes "jumlah_semua_kes"
  ,data3.jumlah_kes_diserah "jumlah_kes_diserah"
  ,jumlah_kes_x_diserah "jumlah_kes_x_diserah"
  ,kes_dibawa_kedepan "kes_dibawa_kedepan"
  ,jumlah_kes_dibawa_kedepan "jumlah_kes_dibawa_kedepan"
from listOfDate
left join data3 
on listOfDate."dept" = data3."dept"
and listOfDate."district" = data3."district"
and listOfDate."year" = data3."year"
and listOfDate."date" = data3."date"
where data3."trial_type" is not null
)
select * from dataCase
order by "date"

;




























with data1 as (
				-- core data
				select
				  d."name" dept
				  ,ds."name" district
				  ,coalesce(extract(year from cr."date"),extract(year from dc."created_at")) as "year"
				  ,coalesce(extract(month from cr."date"),extract(month from dc."created_at")) as "month"
				  ,trunc(dc."created_at") "date"
				  ,trunc(cr."date") "trial_date"
				--  ,trunc(cc."trial_date") "date"
				--  ,trunc(dc."commited_date") "date2"
				  ,cs."name" "trial_type"
				  ,dc."file_no" "file_dept"
				--  ,cc."department_file_id"
				  ,cc."file_no"
				  ,cs."type" "is_done"
				from "department_case" dc
				left join "department" d on d."id" = dc."department_id"
				left join "court_case" cc on cc."id" = dc."id"
				left join "case_record" cr on cr."case_id" = dc."id"
				left join "case_status" cs on cr."case_status_id" = cs."id"
				left join "district" ds on ds."id" = dc."district_id"
				)
				,  data2 as
				(
				-- data processing
				select
				  data1.*
				  ,count("trial_type") over (partition by "trial_type",district,dept,"trial_date") jenis_kes
				  ,count("file_no") over (partition by district,dept,"trial_date") kes_mahkamah
				  ,count("file_dept") over (partition by district,dept,"trial_date") semua_kes
				  ,count("file_no") over (partition by district,dept,"trial_date") kes_diserah

				  ,count("trial_type") over (partition by district,dept,"trial_type","month","year") jumlah_jenis_kes
				  ,count("file_no") over (partition by district,dept,"month","year") jumlah_kes_mahkamah
				  ,count("file_dept") over (partition by district,dept,"month","year") jumlah_semua_kes
				  ,count("file_no") over (partition by district,dept,"month","year") jumlah_kes_diserah
				from data1
				)
				,  data3 as (
				select  --distinct
				  -- data2.*,
				  data2.district "district"
				  ,COALESCE(data2."trial_date",data2."date") "date"
				  ,data2."trial_date"
				  ,data2.dept "dept"
				  ,data2."trial_type"
				  ,data2."is_done"
				  ,data2.jenis_kes "jenis_kes"
				  ,data2.kes_mahkamah "kes_mahkamah"
				  ,data2.semua_kes "semua_kes"
				  ,data2.kes_diserah "kes_diserah"
				  ,(semua_kes - kes_diserah) "kes_x_diserah"
				  ,data2.jumlah_jenis_kes "jumlah_jenis_kes"
				  ,data2.jumlah_kes_mahkamah "jumlah_kes_mahkamah"
				  ,data2.jumlah_semua_kes "jumlah_semua_kes"
				  ,data2.jumlah_kes_diserah "jumlah_kes_diserah"
				  ,(jumlah_semua_kes - jumlah_kes_diserah) "jumlah_kes_x_diserah"
				  ,(kes_diserah- "is_done") "kes_dibawa_kedepan"
				  ,(jumlah_kes_diserah- sum("is_done") over ()) "jumlah_kes_dibawa_kedepan"
				from data2
				)
				,  data4 as (
				-- data manipulation
				select  distinct
				  data3.*
				from data3
				)
				select rownum "id",data4."district",data4."date",data4."trial_date",data4."dept",data4."trial_type",data4."is_done",data4."jenis_kes",data4."kes_mahkamah",data4."semua_kes",data4."kes_diserah",data4."kes_x_diserah",data4."jumlah_jenis_kes",data4."jumlah_kes_mahkamah",data4."jumlah_semua_kes",data4."jumlah_kes_diserah",data4."jumlah_kes_x_diserah",data4."kes_dibawa_kedepan",data4."jumlah_kes_dibawa_kedepan" from data4
				order by "district","dept","trial_date"