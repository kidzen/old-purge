
select *
--  ,sum()over(partition by "year") 
from (
  select 
    d."name" "dept"
    ,o."name" "offence"
    ,ds."shortcode" "district_code"
    ,extract(year from cc."created_at") "year"
    ,extract(month from cc."created_at") "month"
--    ,extract(year from cc."created_at") "year1"
--    ,extract(month from cc."created_at") "month1"
  from "department_case" dc
  left join "department" d on d."id" = dc."department_id"
  left join "court_case" cc on cc."id" = dc."id"
  --left join "case_record" cr on cr."case_id" = dc."id"
  --left join "case_status" cs on cr."case_status_id" = cs."id"
  left join "district" ds on dc."district_id" = ds."id"
  left join "offence" o on dc."offence_id" = o."id"
  where cc."id" is not null
)
pivot (
  count(*) for "month" in
  (1 jan,2 feb,5 may,6 jun)
)

--where "year1" = '2016'
where "year" = '2017'
order by "dept","offence","district_code"
;

select extract(year from "created_at") from "court_case";








;
--select distinct * from (
select 
    "dept"
    ,"offence"
    ,(case when "district_code" is null then 'all District' else to_char("district_code") end) "district_code"
    ,(case when "year" is null then 'all year' else to_char("year") end) "year"
    ,(case when "month" is null then 'TOTAL' else to_char("month") end) "month"
    ,sum("cnt1") "total"
from (
  select 
    d."name" "dept"
    ,o."name" "offence"
    ,ds."shortcode" "district_code"
    ,extract(year from cc."created_at") "year"
    ,extract(month from cc."created_at") "month"
    ,1 "cnt1"
  from "department_case" dc
  left join "department" d on d."id" = dc."department_id"
  left join "court_case" cc on cc."id" = dc."id"
  left join "district" ds on dc."district_id" = ds."id"
  left join "offence" o on dc."offence_id" = o."id"
  where cc."id" is not null
)
GROUP BY rollup(    
    "dept"
    ,"offence"
    ,"district_code"
    ,"year"
    ,"month"
)
;

  select * from "yearly_report"
  pivot
  (
    sum("total") for "month" in ('4' april,'5' may, '6' jun, 'all_month' total_all_month)
--    ,
--    sum("total") for "district_code" in ('SPT (BM)' SPT, 'SPU (BW)' SPU, 'SPS (JAWI)' SPS,'all district' total_per_month)
  )
;

with yearly_month_report as (
  select 
    (case when "dept" is null then 'all dept' else to_char("dept") end) "dept"
    ,(case when "offence" is null then 'all offence' else to_char("offence") end) "offence"
    ,(case when "district_code" is null then 'all district' else to_char("district_code") end) "district_code"
    ,(case when "year" is null then 'all year' else to_char("year") end) "year"
    ,(case when "month" is null then 'all_month' else to_char("month") end) "month"
    ,sum("cnt1") "total"
  from (
    select 
      d."name" "dept"
      ,o."name" "offence"
      ,ds."shortcode" "district_code"
      ,extract(year from cc."created_at") "year"
      ,extract(month from cc."created_at") "month"
      ,1 "cnt1"
    from "department_case" dc
    left join "department" d on d."id" = dc."department_id"
    left join "court_case" cc on cc."id" = dc."id"
    left join "district" ds on dc."district_id" = ds."id"
    left join "offence" o on dc."offence_id" = o."id"
    where cc."id" is not null
  )
  GROUP BY rollup("dept","offence","district_code","year","month")
)
, yearly_district_report as (
  select 
    (case when "dept" is null then 'all dept' else to_char("dept") end) "dept"
    ,(case when "offence" is null then 'all offence' else to_char("offence") end) "offence"
    ,(case when "year" is null then 'all year' else to_char("year") end) "year"
    ,(case when "month" is null then 'all_month' else to_char("month") end) "month"
    ,(case when "district_code" is null then 'all district' else to_char("district_code") end) "district_code"
    ,sum("cnt1") "total"
  from (
    select 
      d."name" "dept"
      ,o."name" "offence"
      ,ds."shortcode" "district_code"
      ,extract(year from cc."created_at") "year"
      ,extract(month from cc."created_at") "month"
      ,1 "cnt1"
    from "department_case" dc
    left join "department" d on d."id" = dc."department_id"
    left join "court_case" cc on cc."id" = dc."id"
    left join "district" ds on dc."district_id" = ds."id"
    left join "offence" o on dc."offence_id" = o."id"
    where cc."id" is not null
  )
  GROUP BY rollup("dept","offence","year","month","district_code")
)
, monthly as  (
  select * from yearly_month_report
  pivot
  (
    sum("total") for "month" in ('4' april,'5' may, '6' jun, 'all_month' total)
  )
  where "district_code" !='all district'
  and "year" != 'all year'
), by_district as (
  select * from yearly_district_report
  pivot
  (
    sum("total") for "district_code" in ('SPT (BM)' SPT, 'SPU (BW)' SPU, 'SPS (JAWI)' SPS,'all district' total_per_month)
  )
--  order by "year","district_code"
  order by "year","dept","offence","month"
--  where "district_code" ='all District'
--  and "year" = 'all year'
)
--select * from by_district
select * from yearly_district_report
--select * from yearly_monthly_report
;



   GROUP BY rollup(KZ.KZ_NAME, RL.REGION)








CREATE TABLE  "KZ_REG" 
   (     "ID" NUMBER, 
     "AMOUNT" NUMBER, 
     "KZ_NAME_ID" NUMBER, 
     "REGION_ID" NUMBER, 
      CONSTRAINT "KZ_REG_PK" PRIMARY KEY ("ID") ENABLE
   )
/
CREATE TABLE  "KZ_NAME_LOOKUP" 
   (     "KZ_NAME_ID" NUMBER NOT NULL ENABLE, 
     "KZ_NAME" VARCHAR2(45) NOT NULL ENABLE, 
      PRIMARY KEY ("KZ_NAME_ID") ENABLE
   )
/
CREATE TABLE  "REGION_LOOKUP" 
   (     "REGION_ID" NUMBER NOT NULL ENABLE, 
     "REGION" VARCHAR2(5) NOT NULL ENABLE, 
      PRIMARY KEY ("REGION_ID") ENABLE
   )
/
INSERT INTO REGION_LOOKUP (REGION_ID, REGION ) VALUES(1,'EMEA');
INSERT INTO REGION_LOOKUP (REGION_ID, REGION ) VALUES(2,'LAD');
INSERT INTO REGION_LOOKUP (REGION_ID, REGION ) VALUES(3,'APAC');
INSERT INTO REGION_LOOKUP (REGION_ID, REGION ) VALUES(4,'NAS');
INSERT INTO REGION_LOOKUP (REGION_ID, REGION ) VALUES(5,'JAPAN');
/
INSERT INTO KZ_NAME_LOOKUP (KZ_NAME_ID, KZ_NAME ) VALUES(1,'KZ_1');
INSERT INTO KZ_NAME_LOOKUP (KZ_NAME_ID, KZ_NAME ) VALUES(2,'KZ_2');
INSERT INTO KZ_NAME_LOOKUP (KZ_NAME_ID, KZ_NAME ) VALUES(3,'KZ_3');
INSERT INTO KZ_NAME_LOOKUP (KZ_NAME_ID, KZ_NAME ) VALUES(4,'KZ_4');
INSERT INTO KZ_NAME_LOOKUP (KZ_NAME_ID, KZ_NAME ) VALUES(5,'KZ_5');
INSERT INTO KZ_NAME_LOOKUP (KZ_NAME_ID, KZ_NAME ) VALUES(6,'KZ_6');
INSERT INTO KZ_NAME_LOOKUP (KZ_NAME_ID, KZ_NAME ) VALUES(7,'KZ_7');
INSERT INTO KZ_NAME_LOOKUP (KZ_NAME_ID, KZ_NAME ) VALUES(8,'KZ_8');
INSERT INTO KZ_NAME_LOOKUP (KZ_NAME_ID, KZ_NAME ) VALUES(9,'KZ_9');
INSERT INTO KZ_NAME_LOOKUP (KZ_NAME_ID, KZ_NAME ) VALUES(10,'KZ_10');
/
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(41,5,2,3);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(42,7,2,1);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(43,2,2,2);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(44,13,2,4);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(45,0,2,5);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(46,6,4,3);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(47,7,4,1);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(48,2,4,2);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(49,13,4,4);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(50,0,4,5);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(51,20,6,3);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(52,31,6,1);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(53,9,6,2);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(54,19,6,4);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(55,0,6,5);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(56,24,1,3);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(57,39,1,1);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(58,8,1,2);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(59,26,1,4);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(60,0,1,5);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(61,24,3,3);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(62,37,3,1);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(63,8,3,2);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(64,29,3,4);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(65,1,3,5);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(66,26,7,3);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(67,41,7,1);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(68,10,7,2);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(69,23,7,4);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(70,0,7,5);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(71,26,5,3);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(72,40,5,1);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(73,10,5,2);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(74,31,5,4);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(75,0,5,5);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(76,29,8,3);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(77,42,8,1);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(78,10,8,2);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(79,35,8,4);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(80,0,8,5);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(81,29,9,3);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(82,49,9,1);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(83,17,9,2);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(84,43,9,4);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(85,2,9,5);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(86,47,10,3);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(87,67,10,1);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(88,25,10,2);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(89,36,10,4);
INSERT INTO KZ_REG (ID, AMOUNT, KZ_NAME_ID, REGION_ID ) VALUES(90,1,10,5);
/



SELECT
--  *
      KZ.KZ_NAME AS kn,
      nvl(RL.REGION, 'TOTAL')  AS re,
      sum(KR.AMOUNT)  AS am
   FROM
      REGION_LOOKUP RL ,
      KZ_NAME_LOOKUP KZ,
      KZ_REG KR
   WHERE
      rl.region_id   = kr.region_id
   AND kz.kz_name_id = kr.kz_name_id
   GROUP BY rollup(KZ.KZ_NAME, RL.REGION)
   
   
   
   
;




select * from (
  select distinct
    nvl(to_char("year"),'all year') "year"
    ,nvl(to_char("dept"),'all dept') "dept"
    ,nvl(to_char("offence"),'all offence') "offence"
    ,nvl(to_char("district_code"),'all district') "district_code"
    ,nvl(to_char("month"),'all month') "month"
    ,nvl(sum("cnt1") ,0) cnt
  from (
    select 
      d."name" "dept"
      ,o."name" "offence"
      ,extract(year from cc."created_at") "year"
      ,ds."shortcode" "district_code"
      ,extract(month from cc."created_at") "month"
      ,1 "cnt1"
    from "department_case" dc
    left join "department" d on d."id" = dc."department_id"
    left join "court_case" cc on cc."id" = dc."id"
    left join "district" ds on dc."district_id" = ds."id"
    left join "offence" o on dc."offence_id" = o."id"
    where cc."id" is not null
  )
--  group by ROLLUP ("dept","offence","district_code","year","month")
--  group by  "dept","offence", ROLLUP ("district_code","year","month")
  group by  "year", CUBE ("dept","offence","district_code","month")
)
--PIVOT
--(SUM(cnt) FOR "month" IN ('5' may, '6' jun,'all month' total))
--where "year" <> 'all year'
order by "year","dept","offence","district_code","month"
--order by "district_code","year","month"

;

select * from (
  select distinct
    nvl(to_char("dept"),'all dept') "dept"
    ,nvl(to_char("offence"),'all offence') "offence"
    ,nvl(to_char("year"),'all year') "year"
    ,nvl(to_char("month"),'all month') "month"
    ,nvl(to_char("district_code"),'all district') "district_code"
    ,nvl(sum("cnt1") ,0) cnt
  from (
    select 
      d."name" "dept"
      ,o."name" "offence"
      ,ds."shortcode" "district_code"
      ,extract(year from cc."created_at") "year"
      ,extract(month from cc."created_at") "month"
      ,1 "cnt1"
    from "department_case" dc
    left join "department" d on d."id" = dc."department_id"
    left join "court_case" cc on cc."id" = dc."id"
    left join "district" ds on dc."district_id" = ds."id"
    left join "offence" o on dc."offence_id" = o."id"
    where cc."id" is not null
  )
  group by ROLLUP ("dept","offence","year","month","district_code")
)
--PIVOT
--(SUM(cnt) FOR "month" IN ('5' may, '6' jun,'all month' total))
where "year" <> 'all year'
order by "dept","offence","year","month","district_code"

;

select * from "yearly_report"
--where "district_code" = 'SPU (BW)'
where "year" = 2017 
;

select * from "department";
select * from "district";
select * from "offence";
1,2
;
select * from (
  select 
    extract(year from cc."created_at") year
    ,extract(month from cc."created_at") month
    ,dc."file_no",cc."file_no" cfile
    ,d."name" dept
    ,ds."shortcode" dist
    ,o."name" offence
  from "court_case" cc
  left join "department_case" dc on dc."id" = cc."id"
  left join "department" d on dc."department_id" = d."id"
  left join "district" ds on dc."district_id" = ds."id"
  left join "offence" o on dc."offence_id" = o."id"
)
where year = 2017 
--and "department_id" = 1
--and "offence_id" = 2
--and "district_id" = 3
and dept = 1
and offence = 2
and dist = 3