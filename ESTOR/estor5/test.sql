select 
  ii.id item_id 
  ,oi.id
  ,o.id
  ,t.id
  ,i.id
from inventory_items ii
left join ORDER_ITEMS oi on oi.id = ii.CHECKOUT_TRANSACTION_ID
left join orders o on o.id = oi.order_id
left join inventories i on i.id = oi.INVENTORY_ID
left join transactions t on t.id = o.TRANSACTION_ID
where ii.CHECKOUT_TRANSACTION_ID is not null
;

--list check in
create table lci as 
select 
  i.id
from inventory_items ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
left join inventories i on i.id = ic.INVENTORY_ID
left join transactions t on t.id = ic.TRANSACTION_ID
where ii.CHECKOUT_TRANSACTION_ID is null
group by i.id
;

--list check out
create table lco as 
select 
--  listagg(i.id,',') within group (order by i.id)
  i.id
from inventory_items ii
left join ORDER_ITEMS oi on oi.id = ii.CHECKOUT_TRANSACTION_ID
left join orders o on o.id = oi.order_id
left join inventories i on i.id = oi.INVENTORY_ID
left join transactions t on t.id = o.TRANSACTION_ID
where ii.CHECKOUT_TRANSACTION_ID is not null
group by i.id;


create table list as 
select * from lco union select * from lci;


select 
  listagg(id,',') within group (order by id) from 
(select 
  *
from list
group by id
)
;
--list cannot be deleted
21,41,42,43,45,46,62,63,65,66,69,71,72,74,75,76,77,78,79,80,81,82,83,84,85,86,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,122,123,124,125,126,127,128,130,132,133,134,135,136,137,138,141,142,143,144,145,146,147,148,149,150,151,152,154,155,156,157,158,159,160,161,162,163,164,165,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,224,226,227,228,229,230,231,232,233,234,235,237,238,239,240,241,244,245,247,248,255,256,258,259,262,263,264,282,283,285,286,287,288,289,290,291,292,293,294,295,315,316,317,318,319,320,321,323,324,325,328,329,330,331,332,333,334,335,336

select 
  id, category_id
from inventories 
where id in 
(21,41,42,43,45,46,62,63,65,66,69,71,72,74,75,76,77,78,79,80,81,82,83,84,85,86,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,122,123,124,125,126,127,128,130,132,133,134,135,136,137,138,141,142,143,144,145,146,147,148,149,150,151,152,154,155,156,157,158,159,160,161,162,163,164,165,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,224,226,227,228,229,230,231,232,233,234,235,237,238,239,240,241,244,245,247,248,255,256,258,259,262,263,264,282,283,285,286,287,288,289,290,291,292,293,294,295,315,316,317,318,319,320,321,323,324,325,328,329,330,331,332,333,334,335,336)
;


select 
  listagg(category_id,',') within group (order by category_id) from
(select 
--  id,
  category_id
from inventories 
where id in 
(21,41,42,43,45,46,62,63,65,66,69,71,72,74,75,76,77,78,79,80,81,82,83,84,85,86,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,122,123,124,125,126,127,128,130,132,133,134,135,136,137,138,141,142,143,144,145,146,147,148,149,150,151,152,154,155,156,157,158,159,160,161,162,163,164,165,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,224,226,227,228,229,230,231,232,233,234,235,237,238,239,240,241,244,245,247,248,255,256,258,259,262,263,264,282,283,285,286,287,288,289,290,291,292,293,294,295,315,316,317,318,319,320,321,323,324,325,328,329,330,331,332,333,334,335,336)
group by category_id
)
;


--category id that cannot be deleted
1,2,3,6,7,8,9,10,11,12,13,14,15,17

--inventory id that cannot be deleted
21,41,42,43,45,46,62,63,65,66,69,71,72,74,75,76,77,78,79,80,81,82,83,84,85,86,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,122,123,124,125,126,127,128,130,132,133,134,135,136,137,138,141,142,143,144,145,146,147,148,149,150,151,152,154,155,156,157,158,159,160,161,162,163,164,165,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,224,226,227,228,229,230,231,232,233,234,235,237,238,239,240,241,244,245,247,248,255,256,258,259,262,263,264,282,283,285,286,287,288,289,290,291,292,293,294,295,315,316,317,318,319,320,321,323,324,325,328,329,330,331,332,333,334,335,336



select 
  listagg(category_id,',') within group (order by category_id)
from 
  
  (select category_id from
    (select 
      id,code_no,description,quantity
      ,category_id
      ,count(*) over (partition by code_no) cnt
    from inventories 
    )
  where cnt != 1
  group by category_id
  )
--order by cnt desc
;

select 
  id 
from categories 
;

select * from categories where id in (10,15,21,23,24);

--delete from categories where id in (10,15,21,23,24);

select * from categories where name like 'ENJIN';

select quantity from inventories where id = ;


select * from inventories where category_id in (10,15);
select * from inventories_checkin where inventory_id in (10,15);


--drop view temp;
--drop table list


select count(*) from categories;
select count(*) from inventories;
select count(*) from transactions where type = 1;
select count(*) from inventories_checkin;
select count(*) from transactions where type = 2;
select count(*) from orders;
select count(*) from order_items;
select count(*) from inventory_items;

select * from categories order by name;



conflict 
2,3

select * from inventories where category_id = 3;

update inventories set category_id = 3 where category_id = 2;




select * from INVENTORIES_CHECKIN where inventory_id in (21,45,151);









select 
  listagg(transaction_id,',') within group (order by transaction_id)
from 
  (
    select 
      transaction_id 
    from inventories_checkin
    union 
    select 
      transaction_id 
    from orders
  )
;


select 
  count(*)
--  listagg(id,',') within group (order by id)
from transactions where id not in
(3,14,18,19,20,21,22,24,25,26,27,30,31,32,34,35,36,37,38,41,46,49,51,53,54,55,56,57,58,59,60,61,62,63,64,65,66,68,69,70,72,73,80,81,82,86,87,88,89,90,91,92,93,94,95,96,97,98,100,101,105,106,109,110,112,114,116,117,118,119,120,121,122,123,126,127,128,129,133,134,135,136,137,139,140,141,142,143,144,145,146,147,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,250,251,252,253,254,255,256,257,258,261,262,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,280,281,282,283,284,285,286,289,290)
;


--delete from transactions where id in (1,2,4,5,6,7,8,9,10,11,12,13,15,16,17,23,28,29,33,39,40,42,43,44,45,47,48,50,52,67,71,74,75,76,77,78,79,83,84,85,99,102,103,104,107,108,111,113,115,124,125,130,131,132,138,148,197,249,259,260,263,279,287,288,291,292,293)
--;


select * from (
  select 
    order_no,count(*) over (partition by order_no) cnt
  from orders
)where cnt != 1;


select listagg(card_no,',') within group (order by card_no) from (
  select 
    card_no,code_no,count(*) over (partition by card_no) cnt
  from inventories
)where cnt != 1 and cnt != 500;


select * from (
  select 
    sku,count(*) over (partition by sku) cnt
  from inventory_items
)where cnt != 1;





select sum(items_quantity) from inventories_checkin;    9455
select count(*) from inventory_items;     9348

create table temp2 as 
select * from (
  select 
    checkin_transaction_id checkin_id,
    count(*) over (partition by checkin_transaction_id) cnt
  from inventory_items
)
group by checkin_id,cnt;


select * from temp;
select * from temp2;

select * from 
  (
  select 
    t.*,ic.id,ic.ITEMS_QUANTITY
  from temp2 t
  left join INVENTORIES_CHECKIN ic on ic.id = t.checkin_id
  )
where cnt != items_quantity
;


select listagg(id,',') within group (order by id) from (
select ii.id id,ic.id id2 from inventory_items ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.checkin_transaction_id
where ic.id is null
)
;

delete from inventory_items where id in (
9485,9486,18786,18787,18788,18789,18790,18791,18792,18793,18794,18795,18796,18797,18798,18799,18800,18801,18802,18803,18804,18805,18806,18807,18808,18809,18810,18811,18812,18813,18814,18815,18816,18817,18818,18819,18820,18821,18822,18823,18824,18825,18826,18827,18828,18829,18830,18836,18837,18838,18839,18840,18841,18842,18843,18844,18845,18846,18847,18848,18849
);



select * from  (
  select 
    id,count(*) over (partition by sku) cnt
  from inventory_items
)
order by cnt desc
;

create table temp5 as 
select * from (
  select 
    checkout_transaction_id checkout_id,
    count(*) over (partition by checkout_transaction_id) cnt
  from inventory_items
)
group by checkout_id,cnt;


--create table temp4 as 
  select 
    t.CHECKOUT_ID id,oi.id
  from temp5 t
  left join ORDER_ITEMS oi on oi.id = t.checkout_id 
  where oi.id is null
  ;



select * from 
  (
  select 
    t.*,ic.id,ic.ITEMS_QUANTITY
  from temp3 t
  left join ORDER_ITEMS oi on oi.id = t.checkout_id
  )
where cnt != items_quantity
;


select listagg(id,',') within group (order by id) from (
select ii.id id,ic.id id2 from inventory_items ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.checkin_transaction_id
where ic.id is null
)
;


select listagg(id,',') within group (order by id) from temp4;
update inventory_items set CHECKOUT_TRANSACTION_ID = null
where CHECKOUT_TRANSACTION_ID in 
(2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,27,28,29,30,31,33,34,35,36,37,38,39,40,44,47,48,49,50,51,52,53,54,55,56,57,58,59,60,62,71,87,88,89,90,91,92,93,94,95);



create view TRANSACTION_OUT_SISKEN as 
select  
    id
    ,order_no
    ,check_date
    ,status
    ,order_items_id
    ,category_name
    ,card_no
    ,code_no
    ,inventory_description
  , LISTAGG(sku, ', ') WITHIN GROUP (ORDER BY sku) items
  ,quantity,total_price
from
  (
  select 
    o.arahan_kerja_id id
    ,o.order_no
    ,to_char(o.approved_at,'dd-MON-yy') check_date
    ,o.approved status
    ,oi.id order_items_id
    ,c.name category_name
    ,i.card_no
    ,i.code_no
    ,i.description inventory_description
    ,ii.sku
    ,count(*) over (partition by o.order_no) quantity
    ,sum(ii.unit_price) over (partition by o.order_no) total_price
  from inventory_items ii
  left join order_items oi on oi.id = ii.checkout_transaction_id
  left join orders o on o.id = oi.order_id
  left join transactions t on t.id = o.transaction_id
  left join inventories i on i.id = oi.inventory_id
  left join categories c on c.id = i.category_id
  where ii.checkout_transaction_id is not null 
--  and o.arahan_kerja_id is not null
  order by o.order_no desc
  )
group by 
    id
    ,order_no
    ,check_date
    ,status
    ,order_items_id
    ,category_name
    ,card_no
    ,code_no
    ,inventory_description
--    ,sku
    , quantity
    , total_price
order by order_no desc
;
