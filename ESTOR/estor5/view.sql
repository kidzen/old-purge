--vehicle report
create view vehicle_report as 
select 
--  rownum ID
  vl.REG_NO
--  ,vl.REG_NO
--  ,o.ORDER_NO order_no
--  ,oi.UNIT_PRICE
  ,sum(NVL(oi.UNIT_PRICE, '0')) sum_usage
--  ,sum(NVL(oi.UNIT_PRICE, '0')) over (GROUP BY vl.REG_NO) sum
--  ,sum(NVL(oi.UNIT_PRICE, '0')) over (group by vl.REG_NO) sum1
from VEHICLE_LIST vl
left join orders o on o.VEHICLE_ID = vl.id
left join ORDER_ITEMS oi on oi.order_id = o.id
GROUP BY vl.REG_NO;

select 
  rownum ID
  ,t.* 
from
(
select 
--  ii.sku
  to_char(o.APPROVED_AT,'dd-MON-yy') check_out_date
  ,o.arahan_kerja_id
  ,o.ORDER_NO
  ,vl.reg_no
  ,oi.INVENTORY_ID
  ,oi.APP_QUANTITY
  ,ii.UNIT_PRICE
  ,sum(NVL(oi.UNIT_PRICE, '0')) over (partition by reg_no, oi.INVENTORY_ID) total_price
  ,sum(NVL(oi.APP_QUANTITY, '0')) over (partition by reg_no, oi.INVENTORY_ID) quantity
from inventory_items ii
left join ORDER_ITEMS oi on oi.id = ii.CHECKOUT_TRANSACTION_ID
left join orders o on o.ID = oi.ORDER_ID
left join VEHICLE_LIST vl on vl.id = o.VEHICLE_ID
where ii.CHECKOUT_TRANSACTION_ID is not null
order by o.APPROVED_AT desc) t
--group by t.*
--order by check_out_date
;


--vendor report
create view vendors_report as 
select 
  v.NAME
  ,SUM (ic.ITEMS_TOTAL_PRICE) total
--  ,vl.REG_NO
--  ,o.ORDER_NO order_no
--  ,oi.UNIT_PRICE
--  ,sum(NVL(oi.UNIT_PRICE, '0')) sum_usage
--  ,sum(NVL(oi.UNIT_PRICE, '0')) over (GROUP BY vl.REG_NO) sum
--  ,sum(NVL(oi.UNIT_PRICE, '0')) over (group by vl.REG_NO) sum1
from VENDORS v
left join inventories_checkin ic on ic.VENDOR_ID = v.id
GROUP BY v.NAME