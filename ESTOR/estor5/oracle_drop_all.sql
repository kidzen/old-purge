select * from transactions;

select 'drop table ' || table_name || ' cascade constraints;' as sql from user_tables; 
select 'drop view ' || view_name || ' ;' as sql from user_views; 

drop view LAPORAN_PENYELENGARAAN_SISKEN ;
drop view APPROVAL ;
drop view REPORT_IN_QUARTER ;
drop view REPORT_OUT_QUARTER ;
drop view LIST_YEAR ;
drop table VENDORS cascade constraints;
drop table VEHICLE_LIST cascade constraints;
drop table USAGE_LIST cascade constraints;
drop table TRANSACTIONS cascade constraints;
drop table ROLES cascade constraints;
drop table PEOPLE cascade constraints;
drop table PACKAGE cascade constraints;
drop table ORDER_ITEMS cascade constraints;
drop table ORDERS cascade constraints;
drop table INVENTORY_ITEMS cascade constraints;
drop table INVENTORIES_CHECKIN cascade constraints;
drop table INVENTORIES cascade constraints;
drop table CATEGORIES cascade constraints;
drop table AUTH_RULE cascade constraints;
drop table AUTH_ITEM cascade constraints;
drop table AIRCOND_LIST cascade constraints;
drop table ACTIVITY_LOGS cascade constraints;


select * from user_views; 
