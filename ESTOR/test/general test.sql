--------------------------------------------------------------------------------------------
--check estor production db
--------------------------------------------------------------------------------------------

select * from INVENTORIES where created_by = 21 or updated_by = 21;
select * from INVENTORIES_CHECKIN where created_by = 21 or updated_by = 21 or approved_by = 21 or check_by = 21;--
select * from INVENTORY_ITEMS where created_by = 21 or updated_by = 21;--
select * from ORDERS where created_by = 21 or updated_by = 21 or approved_by = 21;
select * from ORDER_ITEMS where created_by = 21 or updated_by = 21;
select * from TRANSACTIONS where created_by = 21 or updated_by = 21 or check_by = 21;--
select * from VEHICLE_LIST where created_by = 21 or updated_by = 21;--
select * from VENDORS where created_by = 21 or updated_by = 21;
select * from ROLES where created_by = 21 or updated_by = 21;--
select * from PACKAGE where created_by = 21 or updated_by = 21;--
select * from USAGE_LIST where created_by = 21 or updated_by = 21;--
select * from CATEGORIES where created_by = 21 or updated_by = 21;--
select * from VENDORS where created_by = 21 or updated_by = 21;--

--------------------------------------------------------------------------------------------
--update estor production db
--------------------------------------------------------------------------------------------

--update user yusri id
UPDATE INVENTORIES_CHECKIN SET created_by = 62 where created_by = 21;
UPDATE INVENTORIES_CHECKIN SET updated_by = 62 where updated_by = 21;
UPDATE INVENTORIES_CHECKIN SET approved_by = 62 where approved_by = 21;
UPDATE INVENTORIES_CHECKIN SET check_by = 62 where check_by = 21;

UPDATE INVENTORY_ITEMS SET created_by = 62 where created_by = 21;
UPDATE INVENTORY_ITEMS SET updated_by = 62 where updated_by = 21;

UPDATE ORDERS SET created_by = 62 where created_by = 21;
UPDATE ORDERS SET updated_by = 62 where updated_by = 21;
UPDATE ORDERS SET approved_by = 62 where approved_by = 21;

UPDATE ORDER_ITEMS SET created_by = 62 where created_by = 21;
UPDATE ORDER_ITEMS SET updated_by = 62 where updated_by = 21;

UPDATE TRANSACTIONS SET created_by = 62 where created_by = 21;
UPDATE TRANSACTIONS SET updated_by = 62 where updated_by = 21;
UPDATE TRANSACTIONS SET check_by = 62 where check_by = 21;

UPDATE VEHICLE_LIST SET created_by = 62 where created_by = 21;
UPDATE VEHICLE_LIST SET updated_by = 62 where updated_by = 21;

UPDATE VENDORS SET created_by = 62 where created_by = 21;
UPDATE VENDORS SET updated_by = 62 where updated_by = 21;

UPDATE INVENTORIES SET created_by = 62 where created_by = 21;
UPDATE INVENTORIES SET updated_by = 62 where updated_by = 21;

--start update order_no on table orders
--drop table order_temp ;
--order temp for nov
create table order_temp1 as select * from (
select id,order_no,concat(substr,rownum) substr from (
  select id,order_no,SUBSTR(order_no,6,8) substr
  from orders
  where SUBSTR(order_no,6,8) = '2016/11-'
  order by created_at
  )
  )
;
--order temp for sep
create table order_temp2 as select * from (
select id,order_no,concat(substr,rownum) substr from (
  select id,order_no,SUBSTR(order_no,6,8) substr
  from orders
  where SUBSTR(order_no,6,8) = '2016/09-'
  order by created_at
  )
  )
;
create table temp1 as select * from (
  select * from order_temp1 union
  select * from order_temp2
);
--update order no for nov
update orders o set o.order_no = (select substr from temp1 ot where ot.ID = o.id);
--drop both temp table
DROP TABLE order_temp1;
DROP TABLE order_temp2;
DROP TABLE temp1;

--start update inventories quantity

create table temp1 as select * from (
  select distinct * from (
    select 
      i.id iv_id,code_no
      ,count(*) over (partition by i.id) cnt
      ,i.quantity
    from INVENTORY_ITEMS ii
    left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
    left join INVENTORIES i on i.id = ic.INVENTORY_ID
    where ii.checkout_transaction_id is null and ii.deleted = 0 
    --group by i.id,i.quantity
    )
  where cnt != quantity
);
select * from temp1;


update inventories i set i.quantity = (select cnt from temp1 t where t.IV_ID = i.id)
where id in (select iv_id from temp1);

drop TABLE temp1;

--start 
--update missing/faulty data
update order_items set app_quantity = 1 where id = 173;
update order_items set app_quantity = 2 where id = 175;
update order_items set app_quantity = 1,unit_price = 18 where id = 171;
update order_items set app_quantity = 2,unit_price = 1.6 where id = 203;
update order_items set app_quantity = 72,unit_price = 72 where id = 256;
update order_items set app_quantity = 489,unit_price = 490.6 where id = 257;
--end
update orders set arahan_kerja_id = null where arahan_kerja_id is not null and deleted = 1;
update orders set arahan_kerja_id = null where arahan_kerja_id is not null and approved = 8;
--------------------------------------------------------------------------------------------
--general test on estor production db
--------------------------------------------------------------------------------------------
select 'drop table '||table_name||' cascade constraints;' from user_tables;
select 'drop view '||table_name||';' from user_tables;

select * from all_users;
select view_name from all_views where owner like 'ESTOR';
select table_name from all_tables where owner like 'ESTOR';
select table_name from user_tables;
select username, password, last_login from dba_users;
select owner, table_name, num_rows from dba_tables where owner = 'ESTOR';
SELECT * FROM USER_SYS_PRIVS;
select cons.CONSTRAINT_NAME, cons.CONSTRAINT_TYPE, cons.TABLE_NAME, cons.STATUS, cons.INDEX_NAME
from all_constraints cons
where cons.owner = 'ESTOR'
and cons.INDEX_NAME like '%PK'
;

select order_no,SUBSTR(order_no,6,8)
from orders;


select concat(substr,rownum) from (
  select SUBSTR(order_no,6,8) substr
  from orders
  where SUBSTR(order_no,6,8) = '2016/11-'
  order by created_at
  )
;

update orders set order_temp = (
              select concat(substr,rownum) from (
                select order_no,SUBSTR(order_no,6,8) substr
                from orders
                where SUBSTR(order_no,6,8) = '2016/11-'
                order by created_at
                ) t
              where t.order_no = o.order_no
            )
where SUBSTR(order_no,6,8) = '2016/11-';
              
              select concat(substr,rownum) from (
                select order_no,SUBSTR(order_no,6,8) substr
                from orders
                where SUBSTR(order_no,6,8) = '2016/11-'
                order by created_at
                ) t
              where t.order_no = 'MPSP/2016/11-38';
              
              
update orders set order_temp = SUBSTR(order_no,6,8);


--start
--drop table order_temp ;
--order temp for nov
create table order_temp1 as select * from (
select id,order_no,concat(substr,rownum) substr from (
  select id,order_no,SUBSTR(order_no,6,8) substr
  from orders
  where SUBSTR(order_no,6,8) = '2016/11-'
  order by created_at
  )
  )
;
--order temp for sep
create table order_temp2 as select * from (
select id,order_no,concat(substr,rownum) substr from (
  select id,order_no,SUBSTR(order_no,6,8) substr
  from orders
  where SUBSTR(order_no,6,8) = '2016/09-'
  order by created_at
  )
  )
;
create table temp1 as select * from (
  select * from order_temp1 union
  select * from order_temp2
);
--update order no for nov
update orders o set o.order_no = (select substr from temp1 ot where ot.ID = o.id);
--drop both temp table
DROP TABLE order_temp1;
DROP TABLE order_temp2;
DROP TABLE temp1;

select 
  order_no,order_temp
from orders 
order by created_at
;


--end
--------------------------------------------------------------------------------------------
--general test2 on estor production db
--------------------------------------------------------------------------------------------

create table temp1 as select * from (
  select distinct * from (
    select 
      i.id iv_id,code_no
      ,count(*) over (partition by i.id) cnt
      ,i.quantity
    from INVENTORY_ITEMS ii
    left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
    left join INVENTORIES i on i.id = ic.INVENTORY_ID
    where ii.checkout_transaction_id is null and ii.deleted = 0 
    --group by i.id,i.quantity
    )
  where cnt != quantity
);
select * from temp1;


update inventories i set i.quantity = (select cnt from temp1 t where t.IV_ID = i.id)
where id in (select iv_id from temp1);

drop TABLE temp1;

--end
--------------------------------------------------------------------------------------------
--general test3 on estor production db
--------------------------------------------------------------------------------------------
--create table temp1 as select * from (
select * from (
  select distinct
  --  *
    oi.id oi_id, --oi.order_id o_id,
    oi.RQ_QUANTITY,
    oi.APP_QUANTITY
    ,count(*) over (partition by ii.checkout_transaction_id) actual_quantity
    ,oi.unit_price oi_price
    ,ii.unit_price ii_price
    ,ii.CHECKOUT_TRANSACTION_ID
  from INVENTORY_ITEMS ii
  --left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
  --left join INVENTORIES i on i.id = ic.INVENTORY_ID
  --left join CATEGORIES c on c.id = i.CATEGORY_ID
  left join ORDER_ITEMS oi on oi.id = ii.checkout_transaction_id  
  left join ORDERS o on o.id = oi.order_ID
  where checkout_transaction_id is not null and  o.approved = 1
) 
where app_quantity != actual_quantity
;

select * from order_items where id = 171;
select distinct
  CHECKIN_TRANSACTION_ID,
  CHECKOUT_TRANSACTION_ID
--  by part
  ,count(*) over (partition by unit_price) quantity_by_part
  ,unit_price
  ,sum(unit_price) over (partition by CHECKIN_TRANSACTION_ID) price_by_part
--  grouped
  ,count(*) over (partition by CHECKOUT_TRANSACTION_ID) quantity_total
  ,sum(unit_price) over (partition by CHECKOUT_TRANSACTION_ID) total_price
from INVENTORY_ITEMS 
where DELETED = 0
;

select 
--  *
  count(*) 
from order_items 
where app_quantity is not null 
--where id = 65
order by id
;

--select count(*) from (
select distinct
--  CHECKIN_TRANSACTION_ID,
  CHECKOUT_TRANSACTION_ID
--  by part
--  ,count(*) over (partition by unit_price) quantity_by_part
--  ,unit_price
--  ,sum(unit_price) over (partition by CHECKIN_TRANSACTION_ID) price_by_part
--  grouped
  ,count(*) over (partition by CHECKOUT_TRANSACTION_ID) quantity_total
  ,sum(unit_price) over (partition by CHECKOUT_TRANSACTION_ID) total_price
from INVENTORY_ITEMS 
where DELETED = 0 and CHECKOUT_TRANSACTION_ID is not null
--group by CHECKOUT_TRANSACTION_ID
order by CHECKOUT_TRANSACTION_ID
--)
;

--drop table temp1;
create table temp1 as select * from (
select 
  checkout_transaction_id id
  ,QUANTITY_TOTAL
  ,TOTAL_PRICE
from TEMP_CHECKOUT_PRICE_QUANTITY
where CHECKOUT_TRANSACTION_ID is not null 
group by CHECKOUT_TRANSACTION_ID,QUANTITY_TOTAL,TOTAL_PRICE
order by CHECKOUT_TRANSACTION_ID
)
;

select 
  t.id,t.QUANTITY_TOTAL,t.TOTAL_PRICE
  ,oi.rq_quantity,oi.app_quantity,oi.unit_price
from temp1 t left join (
select * from order_items where APP_QUANTITY is not null order by id
) oi on oi.id = t.ID
where t.QUANTITY_TOTAL != oi.app_quantity
;
select app_quantity, unit_price from order_items where id = 257;
select count(*),sum(unit_price) from INVENTORY_ITEMS where CHECKOUT_TRANSACTION_ID = 257;

select * from temp1 where id in (173,175);
select * from order_items where APP_QUANTITY is not null order by id;
select * from order_items where id in (173,175);
select * from orders where id = 183;

update order_items set app_quantity = 1 where id = 173;
update order_items set app_quantity = 2 where id = 175;

update order_items set app_quantity = 1,unit_price = 18 where id = 171;
update order_items set app_quantity = 2,unit_price = 1.6 where id = 203;
update order_items set app_quantity = 72,unit_price = 72 where id = 256;
update order_items set app_quantity = 489,unit_price = 490.6 where id = 257;
end;

--end
--------------------------------------------------------------------------------------------
--general test4 on estor production db
--------------------------------------------------------------------------------------------


