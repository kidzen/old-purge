--delete duplicate code no---------------------------------------------------
SELECT * FROM INVENTORIES;

SELECT  
  *
FROM (SELECT ID,CODE_NO,COUNT(*) OVER (PARTITION BY CODE_NO) CNT FROM INVENTORIES ) T
where cnt = 2 ORDER BY CNT desc; -- count 2 is max

--get list of duplicate ids
SELECT  
  listagg(id,',') within group (order by id) AS str_of_str
--  wm_concat(id) 
FROM (SELECT ID,CODE_NO,COUNT(*) OVER (PARTITION BY CODE_NO) CNT FROM INVENTORIES ) T
where cnt = 2 ORDER BY CNT desc; -- count 2 is max

--list of duplicate ids
1,21,22,41,44,46,120,121,130,131,251,326,334,337
1,22,44,121,326 deleted

--check quantity and request out


--SELECT ID,CODE_NO,COUNT(*) OVER (PARTITION BY CODE_NO) CNT FROM INVENTORIES 
select * from inventories_checkin where inventory_id in 
(1,21,22,41,44,46,120,121,130,131,251,326,334,337);

--remove deleted inventories and related data---------------------------------------------------------------------
select id from inventories where deleted = 1;

--remove from checkout------------------------------------
select 
  oi.id order_items_id
  ,o.id order_id
  ,t.id transact_id
from order_items oi
left join orders o on o.id = oi.order_id
left join transactions t on t.id = o.transaction_id
where oi.INVENTORY_ID in (1,22,44,121,326);

--list of deleted
--order_items (95 - 2 ): 32,1
--orders (95 - 1 ): 1
--transact (293 - 1): 2

select listagg(id,',') within group (order by id) list_id from INVENTORY_ITEMS where CHECKOUT_TRANSACTION_ID in (1,32);
--items out : 1

--remove from checkin
select 
  ic.id inventories_checkin_id
  ,t.id transact_id
from inventories_checkin ic
left join transactions t on t.id = ic.transaction_id
where ic.INVENTORY_ID in (1,22,44,121,326);

--list of deleted 
--inventories_checkin (198 - 1 ): 189,1
--transact (293 - 2 ): 1,279

select count(*) from INVENTORY_ITEMS where CHECKin_TRANSACTION_ID in (189,1);
select listagg(id,',') within group (order by id) list_id from INVENTORY_ITEMS where CHECKin_TRANSACTION_ID in (189,1);
--items in (9516-61) : 1,2,9163,9164,9165,9166,9167,9168,9169,9170,9171,9172,9173,9174,9175,9176,9177,9178,9179,9180,9181,9182,9183,9184,9185,9186,9187,9188,9189,9190,9191,9192,9193,9194,9195,9196,9197,9198,9199,9200,9201,9202,9203,9204,9205,9206,9207,9208,9209,9210,9211,9212,9213,9214,9215,9216,9217,9218,9219,9220,9221

DELETE FROM inventory_items t WHERE t.id in (1,2,9163,9164,9165,9166,9167,9168,9169,9170,9171,9172,9173,9174,9175,9176,9177,9178,9179,9180,9181,9182,9183,9184,9185,9186,9187,9188,9189,9190,9191,9192,9193,9194,9195,9196,9197,9198,9199,9200,9201,9202,9203,9204,9205,9206,9207,9208,9209,9210,9211,9212,9213,9214,9215,9216,9217,9218,9219,9220,9221);
DELETE FROM inventories_checkin t WHERE t.id in (189,1);
DELETE FROM inventories t WHERE t.id in (1,279);
DELETE FROM TRANSACTIONS t WHERE t.id in (2,1,279);
DELETE FROM orders t WHERE t.id in (1);
DELETE FROM order_items t WHERE t.id in (32,1);

-->>>>>>>>>>>>>>>>>>>>>>>later need to delete deleted inventories *done>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
DELETE FROM inventories t WHERE t.id in (1,22,44,121,326);

--check deleted from checkin
select * 
from inventory_items ii
left join inventories_checkin ic on ic.id = ii.CHECKIN_TRANSACTION_ID
where ic.inventory_id in (1,279)
;


--check deleted from checkout
select * 
from inventory_items ii
left join ORDER_ITEMS oi on oi.id = ii.CHECKOUT_TRANSACTION_ID
where oi.inventory_id in (1,279)
;

--check others deleted data
select * from inventories_checkin where deleted = 1;
select * from inventory_items where deleted = 1;
select * from orders where deleted = 1;
select * from order_items where deleted = 1;
select * from transactions where deleted = 1;

--result = none

--proceed to duplicates inventories
--get list of duplicate ids
SELECT  
  listagg(id,',') within group (order by id) AS str_of_str
--  wm_concat(id) 
FROM (SELECT ID,CODE_NO,COUNT(*) OVER (PARTITION BY CODE_NO) CNT FROM INVENTORIES ) T
where cnt = 2 ORDER BY CNT desc; -- count 2 is max

--list of duplicate ids
130,131,251,337

select 
  i.id, i.code_no, i.quantity, i.description
from inventories i
where i.id in (130,131,251,337);

--check history checkin duplicates item
select * from INVENTORIES_CHECKIN where inventory_id in (130,131,251,337);
--check history checkout duplicates item
select * from ORDER_ITEMS where inventory_id in (130,131,251,337);

--checkin history for inventory id 130 :has 1 
--inventories_checkin id = 54
--transactions id = 97

--get items checkin for transaction id 97 or checkin id 54
select 
--  *
  listagg(id,',') within group (order by id) items_list_id 
from INVENTORY_ITEMS where CHECKIN_TRANSACTION_ID = 54;

--items list id
7215,7216,7217,7218,7219,7220

--delete record of duplicates items
--delete from transactions where id = 97;
--delete from INVENTORIES_CHECKIN where id = 54;
--delete from inventory_items where id in (7215,7216,7217,7218,7219,7220);
--delete from inventories where id =130;

--proceed to duplicates inventories
--get list of duplicate ids
SELECT  
  listagg(id,',') within group (order by id) AS str_of_str
--  wm_concat(id) 
FROM (SELECT ID,CODE_NO,COUNT(*) OVER (PARTITION BY CODE_NO) CNT FROM INVENTORIES ) T
where cnt = 2  ORDER BY CNT desc; -- count 2 is max

--list of duplicate ids
251,337

select 
  i.id, i.code_no, i.quantity, i.description
from inventories i
where i.id in (251,337);

--check history checkin duplicates item
select * from INVENTORIES_CHECKIN where inventory_id in (251,337);
--check history checkout duplicates item
select * from ORDER_ITEMS where inventory_id in (251,337);
--result : none exist, only inventories record
--delete 1 of the duplicate inventories record with larger id sequence
--delete from inventories where id = 337;

------------------------------------------------------------------------------------------------------------------------
--------------------------------DONE DELETED DUPLICATE INVENTORIES AND RELATED DATA------------------------------
------------------------------------------------------------------------------------------------------------------------

--check for sync of transactions
select count(*) from inventories;
select count(*) from transactions where type=1;
select count(*) from inventories_checkin; 
select sum(items_quantity) from inventories_checkin; 
select count(*) from inventory_items;

select count(*) from transactions where type=2;
select count(*) from orders;        --94
select count(*) from order_items;   --93
select sum(RQ_QUANTITY) from order_items;  -- ask for 150
select sum(APP_QUANTITY) from order_items;  --approve 149
select count(*) from inventory_items where CHECKOUT_TRANSACTION_ID is not null;
select * from order_items where APP_QUANTITY != RQ_QUANTITY;


--result : orders count 94 and order_items count 93 : need to sync

---------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Start Integrity Test--------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------
select count(*) from orders; 
select count(*) from order_items;

select 
  o.id order_id
  , oi.id items_id
from orders o 
left join order_items oi on oi.order_id = o.id
--where oi.id is null
;
select 
  o.id order_id
  , oi.id items_id
from order_items oi
left join orders o on oi.order_id = o.id
;

select * from order_items where id = 16;
select * from orders where id = 101;
select * from estor3.orders where id = 101;

select 
  o.id order_id
  , oi.id items_id
  , oi.order_id items_order_id
from order_items oi
left join orders o on oi.created_at = o.created_at;


select * from order_items;

select 
  o.id order_id
  , oi.id items_id
  , oi.order_id items_order_id
from orders o 
left join order_items oi on oi.created_at = o.created_at;

select 
  o.id order_id
  , oi.id items_id
  , oi.order_id items_order_id
from order_items oi 
left join orders o on oi.created_at = o.created_at;

select * from order_items where id = 33;
select * from orders where id = 33;
--problem found!!
--require update to order_items order_id
---------------------------------------------------------------------------------------------------------------------------
-----------------------------------------End Integrity Test--------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

--update to order_items order_id
--update order_items set order_id = id;

--check order_items after update
select id,order_id from order_items order by id;

--find order id that is not link to order_items
select 
  listagg(o.id,',') within group (order by o.id) list_order_id
from orders o 
left join order_items oi on oi.order_id = o.id
where oi.id is null;
--list of order id based on updated order_id
32


--find orders id that is not link to order_items
select 
  listagg(oi.id,',') within group (order by oi.id) list_order_items_id
from order_items oi 
left join orders o on oi.order_id = o.id
where o.id is null
;

--list of order id based on updated order_id
null

--check unsync data
select * from orders where id = 32;
--transact id : 74
--order id : 32

--remove unsync data
delete from orders where id = 32;
delete from transactions where id = 74;

---------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Start Synchronizing Test--------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

--check for sync of transactions
select count(*) from inventories;   --252
select count(*) from transactions where type=1;   --195
select count(*) from inventories_checkin;   --195
select sum(items_quantity) from inventories_checkin;    --9449   
select count(*) from inventory_items;       --9449

select count(*) from transactions where type=2; --93
select count(*) from orders;        --93
select count(*) from order_items;   --93
select sum(RQ_QUANTITY) from order_items;  -- ask for 150
select sum(APP_QUANTITY) from order_items;  --approve 149
select count(*) from inventory_items where CHECKOUT_TRANSACTION_ID is not null;   -approve 149
select * from order_items where APP_QUANTITY != RQ_QUANTITY;


--result : Synchronizing succes!
---------------------------------------------------------------------------------------------------------------------------
-----------------------------------------End Synchronizing Test---------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Start Repair Migration Data----------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

--check for sequence order
--update inventory_items id
select rownum,ii.* from inventory_items ii order by created_at;
--update inventory_items set id = rownum;

------------------------------------backup all data and schema------------------------------------------------

--create temp table for all checkout with noid as rownum
--create table temp_inventory_items as select rownum noid,ii.* from inventory_items ii order by created_at;
--create table temp_order_items as select rownum noid,oi.* from order_items oi order by created_at;
--create table temp_orders as select rownum noid,o.* from orders o order by created_at;
--create table temp_transactions as select rownum noid,t.* from transactions t order by created_at;
create table temp_inventories as select rownum noid,t.* from inventories t order by created_at;
create table temp_inventories_checkin as select rownum noid,t.* from inventories_checkin t order by created_at;

--update noid for temp table with rownum
update TEMP_INVENTORY_ITEMS set noid = rownum;
update TEMP_ORDER_ITEMS set noid = rownum;
update TEMP_ORDERS set noid = rownum;
update TEMP_TRANSACTIONS set noid = rownum;

--update temp_inventory_items
select 
  tii.id iid,toi.id oid,tii.CHECKOUT_TRANSACTION_ID
  ,tii.noid tiid,toi.noid toid,tii.TEMP_CHECKOUT_ID 
from TEMP_INVENTORY_ITEMS tii
left join TEMP_ORDER_ITEMS toi on toi.id = tii.CHECKOUT_TRANSACTION_ID;

update TEMP_INVENTORY_ITEMS tii set TEMP_CHECKOUT_ID = 
(select noid from TEMP_ORDER_ITEMS toi where toi.id = tii.CHECKOUT_TRANSACTION_ID);

--update temp_order_items
select 
  tii.id iid,toi.id oid,tii.ORDER_ID
  ,tii.noid tiid,toi.noid toid,tii.TEMP_ORDER_ID 
from TEMP_order_items tii
left join TEMP_ORDERS toi on toi.id = tii.ORDER_ID;

update TEMP_ORDER_ITEMS tii set TEMP_ORDER_ID = 
(select noid from TEMP_ORDERS toi where toi.id = tii.ORDER_ID);

--update temp_orders
select 
  tii.id iid,toi.id oid,tii.TRANSACTION_ID
  ,tii.noid tiid,toi.noid toid,tii.TEMP_TRANSACTION_ID 
from TEMP_ORDERS tii
left join TEMP_TRANSACTIONS toi on toi.id = tii.TRANSACTION_ID;

update TEMP_ORDERS tii set TEMP_TRANSACTION_ID = 
(select noid from TEMP_TRANSACTIONS toi where toi.id = tii.TRANSACTION_ID);

--update temp_checkin
select 
  tii.id iid,toi.id oid,tii.TRANSACTION_ID
  ,tii.noid tiid,toi.noid toid,tii.TEMP_TRANSACTION_ID 
from TEMP_INVENTORIES_CHECKIN tii
left join TEMP_TRANSACTIONS toi on toi.id = tii.TRANSACTION_ID;

update TEMP_INVENTORIES_CHECKIN tii set TEMP_TRANSACTION_ID = 
(select noid from TEMP_TRANSACTIONS toi where toi.id = tii.TRANSACTION_ID);

--update temp_inventory_items
select 
  tii.id iid,toi.id oid,tii.CHECKIN_TRANSACTION_ID
  ,tii.noid tiid,toi.noid toid,tii.TEMP_CHECKIN_ID
from TEMP_INVENTORY_ITEMS tii
left join TEMP_INVENTORIES_CHECKIN toi on toi.id = tii.CHECKIN_TRANSACTION_ID;

update TEMP_INVENTORY_ITEMS tii set TEMP_CHECKIN_ID = 
(select noid from TEMP_INVENTORIES_CHECKIN toi where toi.id = tii.CHECKIN_TRANSACTION_ID);

select 
  tii.id iid,toi.id oid,tii.CHECKOUT_TRANSACTION_ID
  ,tii.noid tiid,toi.noid toid,tii.TEMP_CHECKOUT_ID
from TEMP_INVENTORY_ITEMS tii
left join TEMP_ORDER_ITEMS toi on toi.id = tii.CHECKOUT_TRANSACTION_ID;

update TEMP_INVENTORY_ITEMS tii set TEMP_CHECKOUT_ID = 
(select noid from TEMP_ORDER_ITEMS toi where toi.id = tii.CHECKOUT_TRANSACTION_ID);



select 
  tii.id iid,tii.CHECKOUT_TRANSACTION_ID
  ,tii.noid tiid,tii.TEMP_CHECKOUT_ID
  ,tii.CHECKIN_TRANSACTION_ID
  ,tii.TEMP_CHECKIN_ID
from TEMP_INVENTORY_ITEMS tii;

--update temp into real column of all table
--update TEMP_INVENTORIES_CHECKIN set ID = NOID;
--update TEMP_INVENTORIES_CHECKIN set TRANSACTION_ID = TEMP_TRANSACTION_ID;
--update TEMP_TRANSACTIONS set ID = NOID;
--update TEMP_INVENTORY_ITEMS set CHECKIN_TRANSACTION_ID = TEMP_CHECKIN_ID;
--update TEMP_INVENTORY_ITEMS set ID = NOID;
--update TEMP_INVENTORY_ITEMS set CHECKOUT_TRANSACTION_ID = TEMP_CHECKOUT_ID;
--update TEMP_ORDERS set ID = NOID;
--update TEMP_ORDERS set TRANSACTION_ID = TEMP_TRANSACTION_ID;
--update TEMP_ORDER_ITEMS set ID = NOID;
--update TEMP_ORDER_ITEMS set ORDER_ID = TEMP_ORDER_ID;


-------------------------------------------------------------------------------------------------------------------------
--------------------------------------    DONE    -----------------------------------------------
-------------------------------------------------------------------------------------------------------------------------


select * from inventories order by created_at;
select * from TRANSACTIONS order by created_at;
select * from inventories_checkin order by created_at;
select * from inventory_items order by created_at,sku;
select * from orders order by created_at,order_no;
select * from order_items order by created_at;

create table TEMP_INVENTORIES_CHECKIN as SELECT rownum noid,t.* FROM INVENTORIES_CHECKIN t order by created_at,TRANSACTION_ID;
select * from TEMP_INVENTORIES_CHECKIN order by created_at,TRANSACTION_ID;
select * from transactions order by created_at;
select * from inventory_items order by created_at,sku;

update INVENTORY_ITEMS o set CHECKIN_TRANSACTION_ID = 
(select noid from TEMP_INVENTORIES_CHECKIN t where t.id = o.CHECKIN_TRANSACTION_ID);
update INVENTORIES_CHECKIN t1 set ID =  (select noid from TEMP_INVENTORIES_CHECKIN t2 where t1.id = t2.id);

drop table TEMP_INVENTORIES_CHECKIN ;
---------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Start Integrity Test--------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------
select count(*) from INVENTORIES_CHECKIN; 
select count(*) from order_items;

select 
  ii.id items_id
  , ic.id checkin_id
  , t.id trans_id
  ,ii.CREATED_AT
  , ic.CREATED_AT
  , t.CREATED_AT
from inventory_items ii 
left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
left join transactions t on t.id = iC.TRANSACTION_ID
--where oi.id is null
;
select 
  o.id order_id
  , oi.id items_id
from order_items oi
left join orders o on oi.order_id = o.id
;

select * from order_items where id = 16;
select * from orders where id = 101;
select * from estor3.orders where id = 101;

select 
  o.id order_id
  , oi.id items_id
  , oi.order_id items_order_id
from order_items oi
left join orders o on oi.created_at = o.created_at;


select * from order_items;

select 
  o.id order_id
  , oi.id items_id
  , oi.order_id items_order_id
from orders o 
left join order_items oi on oi.created_at = o.created_at;

select 
  o.id order_id
  , oi.id items_id
  , oi.order_id items_order_id
from order_items oi 
left join orders o on oi.created_at = o.created_at;

select * from order_items where id = 33;
select * from orders where id = 33;
--problem found!!
--require update to order_items order_id
---------------------------------------------------------------------------------------------------------------------------
-----------------------------------------End Integrity Test--------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

