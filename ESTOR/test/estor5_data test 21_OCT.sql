select count(*) from inventory_items
where checkout_transaction_id > 70
;

select count(*) from order_items
where id > 70
;

select count(*) from orders
where id > 70
;

select count(*) from TRANSACTIONS
where id in 
(select transaction_id from orders
where id > 70)
;

select transaction_id from orders
where id > 70
;

--start deletion
update inventory_items set 
checkout_transaction_id = null,
updated_at = created_at
where checkout_transaction_id > 86
;
delete from order_items where id > 86;
delete from transactions where id in (select transaction_id from orders where id > 86);
delete from orders where id > 86;
update inventories i set quantity = (
select count(*) from inventory_items ii 
left join inventories_checkin ic on ii.checkin_transaction_id = ic.id
where ic.inventory_id = i.id and ii.deleted = 0 and ii.CHECKOUT_TRANSACTION_ID is null
)
;

rollback;
commit;

select * from order_items;

update inventories i set quantity = (
select count(*) from inventory_items ii 
left join inventories_checkin ic on ii.checkin_transaction_id = ic.id
where ic.inventory_id = i.id and ii.deleted = 0 and ii.CHECKOUT_TRANSACTION_ID is null
)
;
select * from inventories;

select * from inventory_items where deleted = 1;