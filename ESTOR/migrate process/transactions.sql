--CHECK IN TRANSACTION
select
  ii.id
  ,c.NAME category_name
  ,i.DESCRIPTION inventory_name
  ,i.QUANTITY store_quantity
  ,ii.UNIT_PRICE
  ,t.TYPE
  ,t.CHECK_DATE transaction_date
  ,ic.ITEMS_QUANTITY transaction_total_quantity
  ,ic.ITEMS_TOTAL_PRICE transaction_total_price
  ,v.NAME vendor
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
  left join TRANSACTIONS t on t.id = ic.TRANSACTION_ID
  left join VENDORS v on v.id = ic.VENDOR_ID
  left join INVENTORIES i on i.id = ic.INVENTORY_ID
left join CATEGORIES c on c.id = i.CATEGORY_ID
;

--CHECK OUT TRANSACTION
select
  oi.id
  ,t.TYPE
  ,o.ORDER_NO
  ,t.CHECK_DATE transaction_date
  ,c.NAME items_category_name
  ,i.DESCRIPTION items_inventory_name
  ,oi.RQ_QUANTITY
  ,oi.APP_QUANTITY
--  ,o.APPROVED
  ,oi.CURRENT_BALANCE
  ,oi.UNIT_PRICE total_price
  ,o.ORDER_DATE
  ,o.REQUIRED_DATE
  ,vl.reg_no usage
from ORDER_ITEMS oi
left join INVENTORIES i on i.id = oi.INVENTORY_ID
left join CATEGORIES c on c.id = i.CATEGORY_ID
left join ORDERS o on o.id = oi.ORDER_ID
  left join vehicle_list vl on vl.id = o.vehicle_id
  left join TRANSACTIONS t on t.id = o.TRANSACTION_ID
;


--CHECK OUT ITEMS
select 
  ii.id
--  ,oi.id
  ,t.TYPE
  ,o.ORDER_NO
  ,t.CHECK_DATE transaction_date
  ,c.NAME items_category_name
  ,i.DESCRIPTION items_inventory_name
  ,oi.RQ_QUANTITY
  ,oi.APP_QUANTITY
--  ,o.APPROVED
  ,oi.CURRENT_BALANCE
  ,oi.UNIT_PRICE total_price
  ,o.ORDER_DATE
  ,o.REQUIRED_DATE
from INVENTORY_ITEMS ii
left join INVENTORIES i on i.id = ii.INVENTORY_ID
left join CATEGORIES c on c.id = i.CATEGORY_ID
left join ORDERS o on o.id = ii.CHECKOUT_TRANSACTION_ID
  left join vehicle_list vl on vl.id = o.vehicle_id
  left join TRANSACTIONS t on t.id = o.TRANSACTION_ID
left join ORDER_ITEMS oi on oi.ORDER_ID = o.id  
;

select * from inventories;



--CHECK OUT TRANSACTION
select
  oi.id
  ,t.TYPE
  ,o.ORDER_NO
  ,t.CHECK_DATE transaction_date
  ,c.NAME items_category_name
  ,i.DESCRIPTION items_inventory_name
  ,oi.RQ_QUANTITY
  ,oi.APP_QUANTITY
--  ,o.APPROVED
  ,oi.CURRENT_BALANCE
  ,oi.UNIT_PRICE total_price
  ,o.ORDER_DATE
  ,o.REQUIRED_DATE
  ,vl.reg_no usage
from ORDER_ITEMS oi
left join INVENTORIES i on i.id = oi.INVENTORY_ID
left join CATEGORIES c on c.id = i.CATEGORY_ID
left join ORDERS o on o.id = oi.ORDER_ID
  left join vehicle_list vl on vl.id = o.vehicle_id
  left join TRANSACTIONS t on t.id = o.TRANSACTION_ID
;

select 
  ii.id id
  ,i.DESCRIPTION
  ,ii.SKU
  ,oi.RQ_QUANTITY quantity_rq
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
left join INVENTORIES i on i.id = ic.INVENTORY_ID
left join ORDER_ITEMS oi on oi.INVENTORY_ID = ic.INVENTORY_ID
order by ii.id
;

select * from ORDER_ITEMS;