
create table tout2 as
select 
  2 type
  , 0000 icid
  ,o.id oid
  ,to_char(o.created_at, 'dd-MON-yy') check_date
  , o.created_by check_by
  , o.created_at 
  , o.created_at updated_at
  , o.created_by
  , o.created_by updated_by
  , o.DELETED
  , o.DELETED_AT
from orders o 
;

create table tin2 as
select 
  1 type
  , ic.id icid
  ,00 oid
  ,to_char(ic.created_at, 'dd-MON-yy') check_date
  , ic.created_by check_by
  , ic.created_at 
  , ic.created_at updated_at
  , ic.created_by
  , ic.created_by updated_by
  , ic.DELETED
  , ic.DELETED_AT
--left join orders o on  o.TRANSACTION_ID = t.id
from inventories_checkin ic;

create table tin3 as
select t.* from
(select * from tin2 order by created_at) t;

create table tout3 as
select t.* from
(select * from tout2 order by created_at) t;


select rownum,t.* from tout3 t order by created_at;

select count(*) from tout3;
select count(*) from tin3;

create table trans as select * from tout3;
truncate table trans;
select COUNT(*) from trans;

create table trans2 as
select rownum ID,t.* from 
(select * from trans order by created_at) t;

--update trans2 set icid = null where icid = 0;
select * from trans2;
select count(*) from trans2 
where type = 1
;

select
  count(*)over(),
  t.id,t.icid
  ,ic.id
from trans2 t
left join inventories_checkin ic on ic.id = t.icid
where t.type = 1
;
select
  count(*)over(),
  t.id,t.oid
  ,o.id
from trans2 t
left join orders o on o.id = t.oid
where t.type = 2
;

update inventories_checkin ic set ic.transaction_id = 
(select id from trans2 t where t.icid = ic.id)
;
update orders o set o.transaction_id = 
(select id from trans2 t where t.oid = o.id)
;

select count(*)over(partition by type),trans2.* from trans2 order by id;

create table trans3 as select * from trans2;
truncate table transactions;

select * from transactions order by created_at;
select * from transactions ;


select * from orders;
select * from INVENTORIES_CHECKIN order by created_at;
--select * from INVENTORIES order by created_at;
--select * from inventory_items order by created_at;



select 
  t.id
  ,t.created_at 
--  ,ic.created_at
  ,count(*) over ()
from transactions t 
left join inventories_checkin ic on t.created_at = ic.created_at
where t.type = 1
order by t.id
;

select 
  o.created_at ,t.created_at,count(*) over ()
from transactions t 
left join orders o on t.created_at = o.created_at
where t.type = 2
;
select count(*) over (),t.* from transactions t where type =2;
select count(*) over (),t.* from transactions t where type =1;

