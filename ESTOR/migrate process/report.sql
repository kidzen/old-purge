select * from transactions;
--------------------------------------------------------
--  CREATE VIEW
--------------------------------------------------------
--Report in quarterly
--drop view REPORT_IN_QUARTER ;
--create view REPORT_IN_QUARTER as
WITH DATA1 AS (
select 
  ii.sku
  ,ii.unit_price
  ,ic.inventory_id
  ,t.check_date
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
from inventory_items ii
left join inventories_checkin ic on ic.id = ii.checkin_transaction_id
left join transactions t on t.id = ic.transaction_id
where ii.deleted = 0
)

SELECT 
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER
;

--Report out quarterly
--drop view REPORT_OUT_QUARTER;
--create view REPORT_OUT_QUARTER as
WITH DATA1 AS (
select 
  ii.sku
  ,ii.unit_price
  ,oi.inventory_id
  ,t.check_date
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
from inventory_items ii
left join order_items oi on oi.id = ii.checkout_transaction_id
left join orders o on o.id = oi.order_id
left join transactions t on t.id = o.transaction_id
where ii.deleted = 0 and ii.checkout_transaction_id is not null
)

SELECT 
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER
;

--view for list year
--  CREATE OR REPLACE EDITIONABLE VIEW "LIST_YEAR" ("YEAR", "QUARTER") AS 
  select year,quarter from REPORT_IN_QUARTER union 
select year,quarter from REPORT_OUT_QUARTER;


--create report all
--  CREATE OR REPLACE EDITIONABLE VIEW "REPORT_ALL" ("YEAR", "QUARTER", "COUNT_IN", "PRICE_IN", "COUNT_OUT", "PRICE_OUT", "COUNT_CURRENT", "PRICE_CURRENT") AS 
  select
    list_year.YEAR,
    list_year.QUARTER,
    NVL(REPORT_IN_QUARTER.COUNT, '0') count_in,
    NVL(REPORT_IN_QUARTER.TOTAL_PRICE, '0') price_in,
    NVL(REPORT_out_QUARTER.COUNT, '0') count_out,
    NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0') price_out,

      sum(NVL(REPORT_IN_QUARTER.COUNT, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(REPORT_OUT_QUARTER.COUNT, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING)
    count_current,
      sum(NVL(REPORT_in_QUARTER.TOTAL_PRICE, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING)
    price_current

from list_year
left join REPORT_IN_QUARTER 
on (REPORT_IN_QUARTER.YEAR = list_year.YEAR and list_year.QUARTER = REPORT_IN_QUARTER.QUARTER)
left join REPORT_OUT_QUARTER 
on (list_year.YEAR = REPORT_OUT_QUARTER.YEAR and list_year.QUARTER = REPORT_OUT_QUARTER.QUARTER)
order by list_year.YEAR, list_year.QUARTER;


--current report
--creat
  WITH DATA1 AS (select 
  ii.SKU
  ,ii.UNIT_PRICE
  ,ic.INVENTORY_ID
  ,ic.CHECK_DATE
  ,extract(year from ic.CHECK_DATE) year
  ,to_char(ic.CHECK_DATE,'Q') quarter 
from INVENTORY_ITEMS  ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
--WHERE CHECKIN_TRANSACTION_ID IS NOT NULL 
--and CHECKOUT_TRANSACTION_ID IS NULL 
--or CHECKOUT_DATE IS NULL 
--or to_char(,'q') < 4)
)
SELECT 
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;

--------------------------------------------------------
--  DDL for View REPORT_ALL2
--------------------------------------------------------

--  CREATE OR REPLACE EDITIONABLE VIEW "REPORT_ALL2" ("YEAR", "QUARTER", "COUNT_CURRENT", "PRICE_CURRENT", "COUNT_IN", "PRICE_IN", "COUNT_OUT", "PRICE_OUT") AS 
  select
    REPORT_IN_QUARTER.YEAR,
    REPORT_IN_QUARTER.QUARTER,
    NVL(REPORT_CURRENT.COUNT, '0') count_CURRENT,
    NVL(REPORT_CURRENT.TOTAL_PRICE, '0') price_CURRENT,
    NVL(REPORT_IN_QUARTER.COUNT, '0') count_in,
    NVL(REPORT_IN_QUARTER.TOTAL_PRICE, '0') price_in,
    NVL(REPORT_out_QUARTER.COUNT, '0') count_out,
    NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0') price_out
from REPORT_IN_QUARTER 
left join REPORT_OUT_QUARTER 
on (REPORT_IN_QUARTER.YEAR = REPORT_OUT_QUARTER.YEAR and REPORT_IN_QUARTER.QUARTER = REPORT_OUT_QUARTER.QUARTER)
left join REPORT_CURRENT
on (REPORT_IN_QUARTER.YEAR = REPORT_CURRENT.YEAR and REPORT_IN_QUARTER.QUARTER = REPORT_CURRENT.QUARTER)
order by REPORT_IN_QUARTER.YEAR, REPORT_IN_QUARTER.QUARTER;
/

--transactions alias KAD PETAK
--in
select 
--  *
  t.id transaction_no
  ,ic.id refference_no
  ,t.check_date
  ,t.type
  ,ii.sku
  ,ii.unit_price
  ,ic.inventory_id
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'MON') month 
  ,to_char(t.check_date,'Q') quarter 

from inventory_items ii
left join inventories_checkin ic on ic.id = ii.checkin_transaction_id
left join transactions t on t.id = ic.transaction_id
order by t.created_at
;
--out
select 
--  *
  t.id transaction_no
  ,o.order_no refference_no
  ,t.check_date
  ,t.type
  ,oi.inventory_id
  ,oi.APP_QUANTITY
  ,ii.unit_price
  ,extract(year from o.APPROVED_AT) year
  ,to_char(o.APPROVED_AT,'MON') month 
  ,to_char(o.APPROVED_AT,'Q') quarter 

from inventory_items ii
left join order_items oi on oi.id = ii.checkout_transaction_id
left join orders o on o.id = oi.order_id
left join transactions t on t.id = o.transaction_id
where ii.CHECKOUT_TRANSACTION_ID is not null
order by t.created_at
;

--in
select 
--  *
  rownum id
--  ,t.id transaction_no
  ,t.check_date
  ,ic.id refference_no
  ,ic.ITEMS_QUANTITY
  ,sum(ic.ITEMS_TOTAL_PRICE/ic.ITEMS_QUANTITY) over (partition by ic.id) unit_price 
--  ,t.type
  ,ic.inventory_id
  ,ic.ITEMS_TOTAL_PRICE
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'MON') month 
  ,to_char(t.check_date,'Q') quarter 

from inventories_checkin ic 
left join transactions t on t.id = ic.transaction_id
where 1=1 
  and ic.inventory_id = 21
order by t.created_at
;
