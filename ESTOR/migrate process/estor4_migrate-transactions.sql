select * from transactions;
--drop table tin;
create table tin as
select
  id icid
  ,1 type
  ,to_char(created_at, 'dd-MON-yy') check_date
  ,created_by check_by
  ,created_at 
  ,created_at updated_at
  ,created_by
  ,created_by updated_by
  ,deleted
  ,deleted_at
from inventories_checkin;

--drop table tout;
--create table tout as
select
  id oid
  ,2 type
  ,to_char(created_at, 'dd-MON-yy') check_date
  ,created_by check_by
  ,created_at 
  ,created_at updated_at
  ,created_by
  ,created_by updated_by
  ,deleted
  ,deleted_at
from orders;

--create table temp as select * from transactions;


truncate table transactions;
truncate table temp;

insert into temp
(
  icid
  ,type
  ,check_date
  ,check_by
  ,created_at 
  ,updated_at
  ,created_by
  ,updated_by
  ,deleted
  ,deleted_at
)
select 
  icid
  ,type
  ,check_date
  ,check_by
  ,created_at 
  ,updated_at
  ,created_by
  ,updated_by
  ,deleted
  ,deleted_at
from tin order by created_at;

insert into temp
(
  oid
  ,type
  ,check_date
  ,check_by
  ,created_at 
  ,updated_at
  ,created_by
  ,updated_by
  ,deleted
  ,deleted_at
)
select 
  oid
  ,type
  ,check_date
  ,check_by
  ,created_at 
  ,updated_at
  ,created_by
  ,updated_by
  ,deleted
  ,deleted_at
from tout order by created_at;


select sum(198+93) from dual;
select count(*) from temp;

--select temp without order
select * from temp;
--select temp with order
select rownum id, t.* from 
(select * from temp order by created_at) t;
--create tempt2 with id
drop table temp2;
create table temp2 as
select rownum id, t.* from 
(select * from temp order by created_at) t
;
--select temp with order
select * from temp2 order by created_at asc;
--truncate temp2
--truncate table temp2;

select * from inventories_checkin order by created_at;

select * from orders order by created_at;

select * from estor.stocks order by created_date;
select s.id,si.id from estor.stock_items si left join estor.stocks s on si.stock_id = s.id;
select * from estor.stock_items where id = 1;

create table dummy_o1 as 
select 
  "ID"
  ,"INVENTORY_ID"
  ,"STOCK_ID" ORDER_ID
  ,"RQ_QUANTITY"
  ,"APP_QUANTITY"
  ,"CURRENT_BALANCE"
--  ,"UNIT_PRICE"
  ,"TOTAL_PRICE" UNIT_PRICE
--  ,"SKU"
--  ,"DESCRIPTION"
  ,"CREATED_DATE" CREATED_AT
  ,"UPDATED_DATE" UPDATED_AT
  ,"CREATED_BY"
  ,"UPDATED_BY"
  ,"DELETED"
  ,"DELETED_DATE"
from ESTOR.STOCK_ITEMS
;

select * from dummy_o1 ;
