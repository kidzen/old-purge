--transaction in
--create view TRANSACTION_IN as 
WITH DATA1 AS (
select 
  t.id
  ,t.type
  ,ii.sku
  ,ii.unit_price
  ,ic.inventory_id
  ,t.check_date
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
  ,COUNT(*) OVER (PARTITION BY t.ID) COUNT
  ,SUM(ii.UNIT_PRICE) OVER (PARTITION BY t.ID) TOTAL_PRICE
from inventory_items ii
left join inventories_checkin ic on ic.id = ii.checkin_transaction_id
left join transactions t on t.id = ic.transaction_id
where ii.deleted = 0
)

SELECT 
  ID,
  TYPE,  
  CHECK_DATE,  
  INVENTORY_ID,
--  rownum as id,
--  QUARTER,
  COUNT,
  TOTAL_PRICE
FROM DATA1
group by id,count,total_price, type, check_date,inventory_id
ORDER BY id;


--transaction out
--create view TRANSACTION_OUT as
WITH DATA1 AS (
select 
  t.id
  ,t.type
  ,ii.sku
  ,ii.unit_price
  ,oi.inventory_id
  ,t.check_date
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
  ,COUNT(*) OVER (PARTITION BY t.ID) COUNT
  ,SUM(ii.UNIT_PRICE) OVER (PARTITION BY t.ID) TOTAL_PRICE
from inventory_items ii
left join order_items oi on oi.id = ii.checkout_transaction_id
left join orders o on o.id = oi.order_id
left join transactions t on t.id = o.transaction_id
where ii.deleted = 0 and ii.checkout_transaction_id is not null
)

SELECT 
--  *
  ID,
  TYPE,  
  CHECK_DATE,  
  INVENTORY_ID,
--  rownum as id,
--  QUARTER,
  COUNT,
  TOTAL_PRICE
FROM DATA1
group by id,count,total_price, type, check_date,inventory_id
--group by 
ORDER BY id;




--transaction all
--CREATE VIEW TRANSACTIONS_ALL AS 
select
--    rownum as id,
    IDS.ID,
    IDS.TYPE,
    IDS.CHECK_DATE,
    IDS.INVENTORY_ID,
    NVL(TRANSACTION_IN.COUNT, '0') count_in,
    NVL(TRANSACTION_IN.TOTAL_PRICE, '0') price_in,
    NVL(TRANSACTION_OUT.COUNT, '0') count_out,
    NVL(TRANSACTION_OUT.TOTAL_PRICE, '0') price_out,

      sum(NVL(TRANSACTION_IN.COUNT, '0')) over (ORDER BY IDS.ID ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(TRANSACTION_OUT.COUNT, '0')) over (ORDER BY IDS.ID ROWS UNBOUNDED PRECEDING)
    count_current,
      sum(NVL(TRANSACTION_IN.TOTAL_PRICE, '0')) over (ORDER BY IDS.ID ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(TRANSACTION_OUT.TOTAL_PRICE, '0')) over (ORDER BY IDS.ID ROWS UNBOUNDED PRECEDING)
    price_current

from (select * from TRANSACTION_OUT union 
select * from TRANSACTION_IN
) IDS
left join TRANSACTION_IN
on (TRANSACTION_IN.ID = IDS.ID)
left join TRANSACTION_OUT 
on (IDS.ID = TRANSACTION_OUT.ID)
order by IDS.ID;


SELECT * FROM TRANSACTIONS_ALL;