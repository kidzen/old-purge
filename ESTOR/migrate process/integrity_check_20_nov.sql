select id_inventories,quantity,available_per_order from (
select
  rownum as id
  ,count(*) over(partition by oi.id,i.id) available_per_order
  ,i.quantity
  ,ii.id id_inventory_items
  ,i.id id_inventories
  ,c.id id_categories
  ,ic.id id_inventories_checkin
  ,oi.id id_order_items
  ,o.id id_orders
  ,ii.sku
  ,ii.UNIT_PRICE
--  ,t.TYPE
  ,o.ORDER_NO
--  ,t.CHECK_DATE transaction_date
  ,c.NAME items_category_name
  ,i.DESCRIPTION items_inventory_name
  ,oi.RQ_QUANTITY
  ,oi.APP_QUANTITY
  ,o.APPROVED
  ,oi.CURRENT_BALANCE
  ,oi.UNIT_PRICE total_price
  ,o.ORDER_DATE
  ,o.REQUIRED_DATE
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
left join INVENTORIES i on i.id = ic.INVENTORY_ID
left join CATEGORIES c on c.id = i.CATEGORY_ID
left join ORDER_ITEMS oi on oi.inventory_id = i.id  
left join ORDERS o on o.id = oi.order_ID
--  left join vehicle_list vl on vl.id = o.vehicle_id
--  left join TRANSACTIONS t on t.id = o.TRANSACTION_ID
where ii.CHECKOUT_TRANSACTION_ID is null
) where quantity != available_per_order;


select distinct * from (
select 
  i.id,code_no
  ,count(*) over (partition by i.id) cnt
  ,i.quantity
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
left join INVENTORIES i on i.id = ic.INVENTORY_ID
where ii.checkout_transaction_id is null and ii.deleted = 0 
--group by i.id,i.quantity
)
where cnt != quantity;



-----------------------------------------------------------------------------------------------
--inventory quantity integrity check
-----------------------------------------------------------------------------------------------
select distinct * from (
  select 
    iv_id,ic_id
    ,iv_quantity
    ,count(*) over (partition by iv_id) actual_quantity
--    ,ic_total_price
  --  ,sum(unit_price) over (partition by iv_id) actual_total_price
  from (
    select 
      i.id iv_id
      ,ic.id ic_id
      ,i.quantity iv_quantity
      ,ic.ITEMS_QUANTITY ic_quantity
      ,ii.unit_price
      ,ic.ITEMS_TOTAL_PRICE ic_total_price
    from INVENTORY_ITEMS ii
    left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
    left join INVENTORIES i on i.id = ic.INVENTORY_ID
    where ii.checkout_transaction_id is null 
    and ii.deleted = 0 and i.deleted = 0 and ic.deleted = 0
    --and ii.INVENTORY_ID = 149
  )
)
where iv_quantity != actual_quantity
;


-----------------------------------------------------------------------------------------------
--inventories price integrity test
-----------------------------------------------------------------------------------------------
select distinct * from (
  select 
    iv_id,ic_id
    ,ic_total_price
    ,sum(unit_price) over (partition by ic_id) actual_total_price
  from (
    select 
      i.id iv_id
      ,ic.id ic_id
      ,i.quantity iv_quantity
      ,ic.ITEMS_QUANTITY ic_quantity
      ,ii.unit_price
      ,ic.ITEMS_TOTAL_PRICE ic_total_price
    from INVENTORY_ITEMS ii
    left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
    left join INVENTORIES i on i.id = ic.INVENTORY_ID
    where ii.deleted = 0 and i.deleted = 0 and ic.deleted = 0
  )
)
where ic_total_price != actual_total_price
;


-----------------------------------------------------------------------------------------------
--check if there is deleted item checked out
-----------------------------------------------------------------------------------------------
select *  from INVENTORY_ITEMS ii
where ii.deleted = 1 and ii.CHECKOUT_TRANSACTION_ID is not null
;

-----------------------------------------------------------------------------------------------
--check if there is duplicate sku
-----------------------------------------------------------------------------------------------
select * from (
  select count(*) over (partition by sku) sku_count from (
    select sku  
    from INVENTORY_ITEMS ii
  )
)
where sku_count > 1
;


-----------------------------------------------------------------------------------------------
--checkout integrity test
-----------------------------------------------------------------------------------------------
select * from (
  select distinct
  --  *
    oi.id,
    oi.RQ_QUANTITY,
    oi.APP_QUANTITY
    ,count(*) over (partition by ii.checkout_transaction_id) actual_quantity
  from INVENTORY_ITEMS ii
  --left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
  --left join INVENTORIES i on i.id = ic.INVENTORY_ID
  --left join CATEGORIES c on c.id = i.CATEGORY_ID
  left join ORDER_ITEMS oi on oi.id = ii.checkout_transaction_id  
  left join ORDERS o on o.id = oi.order_ID
  where checkout_transaction_id is not null and  o.approved = 1
) 
where app_quantity != actual_quantity
;


select distinct o.id,oi.id,o.ORDER_NO, o.approved
from INVENTORY_ITEMS ii
--left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
--left join INVENTORIES i on i.id = ic.INVENTORY_ID
--left join CATEGORIES c on c.id = i.CATEGORY_ID
left join ORDER_ITEMS oi on oi.id = ii.checkout_transaction_id  
left join ORDERS o on o.id = oi.order_ID
where oi.id in (171, 203, 257, 256)
;



WITH USER_SQL AS (SELECT "ORDERS".ORDER_NO as orderNo ,"ORDER_ITEMS".* FROM "ORDER_ITEMS" LEFT JOIN "INVENTORIES" ON "ORDER_ITEMS"."INVENTORY_ID" = "INVENTORIES"."ID" LEFT JOIN "ORDERS" ON "ORDER_ITEMS"."ORDER_ID" = "ORDERS"."ID" LEFT JOIN "TRANSACTIONS" ON "ORDERS"."TRANSACTION_ID" = "TRANSACTIONS"."ID" LEFT JOIN "LAPORAN_PENYELENGARAAN_SISKEN" ON "ORDERS"."ARAHAN_KERJA_ID" = "LAPORAN_PENYELENGARAAN_SISKEN"."ID" LEFT JOIN "VEHICLE_LIST" ON "ORDERS"."VEHICLE_ID" = "VEHICLE_LIST"."ID" WHERE ("ORDER_ITEMS"."DELETED"=0) AND ("ORDERS"."ORDER_NO" LIKE '%MPSP/2016/11-59%') ORDER BY "ORDER_ITEMS"."ID" DESC),
    PAGINATION AS (SELECT USER_SQL.*, rownum as rowNumId FROM USER_SQL)
SELECT *
FROM PAGINATION
WHERE rownum <= 20


;

--check duplicate order_no
select * from (
  select 
    order_no
    ,count(*) over (partition by order_no) count
  from orders 
)
where count > 1
;

select count(*) from orders;


