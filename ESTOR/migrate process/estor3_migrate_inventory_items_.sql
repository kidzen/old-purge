select 
  inventory_id inventory_id
  ,count(*)  items_quantity
  ,unit_price items_total_price
--  ,CREATED_BY check_by
--  ,1 approved
--  ,CREATED_BY approved_by
  ,to_char(created_date, 'dd-MON-yy hh24.mi') approve_at
  ,to_char(created_date, 'dd-MON-yy hh24.mi') created_at
  ,to_char(created_date, 'dd-MON-yy hh24.mi') updated_at
--  ,substr(created_date,0,15) approve_at
--  ,substr(created_date,0,15) created_at
--  ,substr(created_date,0,15) updated_at
--  ,CREATED_BY created_by
--  ,CREATED_BY updated_by
--  ,substr(created_date,0,14)
from estor.estor_items
group by 
--  rid,
  inventory_id
  ,checkin_date
  ,CREATED_BY
  ,unit_price
  ,to_char(created_date, 'dd-MON-yy hh24.mi')
--  ,substr(created_date,0,15)
--  ,substr(created_date,27,26)
--order by substr(created_date,0,15) asc
--order by inventory_id asc
order by to_char(created_date, 'dd-MON-yy hh24.mi') asc
;

select * from inventory_items
where to_char(created_at, 'dd-MON-yy hh24.mi') = '02-SEP-16 09.02'
;

select
  inventory_id inventory_id
  ,count(*)  items_quantity
  ,unit_price items_total_price
  ,to_char(created_date, 'dd-MON-yy hh24.mi') approve_at
  ,to_char(created_date, 'dd-MON-yy hh24.mi') created_at
  ,to_char(created_date, 'dd-MON-yy hh24.mi') updated_at
from estor.estor_items
group by 
  inventory_id
  ,checkin_date
  ,CREATED_BY
  ,unit_price
  ,to_char(created_date, 'dd-MON-yy hh24.mi')
order by to_char(created_date, 'dd-MON-yy hh24.mi') asc
;


select * from dummy_items;
create table dummy_items2 as select * from ESTOR2.INVENTORY_ITEMS;
select * from dummy_items2;
create table dummy_items3 as select * from INVENTORY_ITEMS;
select * from dummy_items3;

select 
  ic.id
  ,ic.transaction_id
  ,ic.INVENTORY_ID
  ,ic.ITEMS_QUANTITY
  ,ic.ITEMS_TOTAL_PRICE
  ,to_char(ic.created_at, 'dd-MON-yy hh24.mi') created_at
from INVENTORIES_CHECKIN ic;

select 
--  *
  ii.id
  ,ic.id transacion_id
--  ,ii.INVENTORY_ID
--  ,ii.CHECKIN_TRANSACTION_ID
--  ,ii.UNIT_PRICE
  ,to_char(ii.created_at, 'dd-MON-yy hh24.mi') created_at
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN ic on to_char(ii.created_at, 'dd-MON-yy hh24.mi') = to_char(ic.created_at, 'dd-MON-yy hh24.mi')
;
drop TABLE items;
create table items as 
select 
  dm."ID"
  ,dm."INVENTORY_ID"
--  ,dm."CHECKIN_TRANSACTION_ID"
  ,ic."ID" CHECKIN_TRANSACTION_ID
  ,dm."CHECKOUT_TRANSACTION_ID"
  ,dm."SKU"
  ,dm."UNIT_PRICE"
  ,dm."CREATED_AT"
  ,dm."UPDATED_AT"
  ,dm."CREATED_BY"
  ,dm."UPDATED_BY"
  ,dm."DELETED"
  ,dm."DELETED_AT"
from DUMMY_ITEMS3 dm
left join INVENTORIES_CHECKIN ic on 
  to_char(dm.created_at, 'dd-MON-yy hh24.mi') = to_char(ic.created_at, 'dd-MON-yy hh24.mi')
  and dm."CREATED_BY" = ic."CREATED_BY"
  and dm."UNIT_PRICE" = ic."ITEMS_TOTAL_PRICE"
  and dm."INVENTORY_ID" = ic."INVENTORY_ID"
--where ic.ID = 52
;

select 
  CHECKIN_TRANSACTION_ID
  ,count(*) items_quantity
from items
group by CHECKIN_TRANSACTION_ID
order by CHECKIN_TRANSACTION_ID
;
select 
  id
  ,ITEMS_QUANTITY 
FROM INVENTORIES_CHECKIN
order by ID
;
drop table test2; 
create table test2 as
select 
  i.CHECKIN_TRANSACTION_ID
  ,count(*) items_quantity
from items i
group by i.CHECKIN_TRANSACTION_ID
order by i.CHECKIN_TRANSACTION_ID
; 

select 
  t.CHECKIN_TRANSACTION_ID
  ,ic.ID id2
  ,sum(t.CHECKIN_TRANSACTION_ID - ic.ID) over (PARTITION BY t.CHECKIN_TRANSACTION_ID) id_compare
  ,t.ITEMS_QUANTITY
  ,ic.ITEMS_QUANTITY as quantity2
  ,sum(t.ITEMS_QUANTITY - ic.ITEMS_QUANTITY) over (PARTITION BY t.CHECKIN_TRANSACTION_ID) quantity_compare
from test2 t
left join INVENTORIES_CHECKIN ic on ic.id = t.CHECKIN_TRANSACTION_ID
--order by quantity_compare desc
--order by id_compare desc
--where t.CHECKIN_TRANSACTION_ID != ic.ID
;

UPDATE DUMMY_ITEMS3 dm
   SET dm.CHECKIN_TRANSACTION_ID = (SELECT ic.ID FROM INVENTORIES_CHECKIN ic
                WHERE 1=1
                  and to_char(dm.created_at, 'dd-MON-yy hh24.mi') = to_char(ic.created_at, 'dd-MON-yy hh24.mi')
                  and dm."CREATED_BY" = ic."CREATED_BY"
                  and dm."UNIT_PRICE" = ic."ITEMS_TOTAL_PRICE"
                  and dm."INVENTORY_ID" = ic."INVENTORY_ID"
                )
-- WHERE SKU IN (SELECT SKU FROM SKU_Size_Map);
 ;
 
 select 
--  *
--  CHECKIN_TRANSACTION_ID  
  checkin_transaction_id
  ,count(*)  cnt
--  ,count(*) over () cnt
--  ,sum(count(*)) over (order by checkin_transaction_id ROWS UNBOUNDED PRECEDING ) up_curr
--  ,sum(count(*)) over (order by checkin_transaction_id ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) upcr_curr
--  ,sum(count(*)) over (order by checkin_transaction_id ROWS  BETWEEN 1 PRECEDING AND 0 FOLLOWING) c3p1f_after
--  ,sum(count(*)) over (order by checkin_transaction_id ROWS  BETWEEN 3 PRECEDING AND 1 PRECEDING ) c3p1p_before
--  ,sum(count(*)) over (order by checkin_transaction_id ROWS BETWEEN 1 FOLLOWING AND 3 FOLLOWING) c1f3f
from DUMMY_ITEMS3
--left join 
 group by CHECKIN_TRANSACTION_ID
 order by CHECKIN_TRANSACTION_ID 
 ;
 
 select 
  count(*) 
from DUMMY_ITEMS3 
where CHECKIN_TRANSACTION_ID is not null;

select * from items
order by CREATED_AT,id;
select * from DUMMY_ITEMS3
order by CREATED_AT,id;

UPDATE ESTOR3.INVENTORY_ITEMS dm
   SET dm.CHECKIN_TRANSACTION_ID = (SELECT ic.CHECKIN_TRANSACTION_ID FROM ESTOR3.DUMMY_ITEMS3 ic
                WHERE dm.id=ic.id
                )
-- WHERE SKU IN (SELECT SKU FROM SKU_Size_Map);
 ;
 select id,CHECKIN_TRANSACTION_ID from INVENTORY_ITEMS;
SELECT id,ic.CHECKIN_TRANSACTION_ID FROM DUMMY_ITEMS3 ic;






