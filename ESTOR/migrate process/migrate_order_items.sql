drop table oi1;
create table oi1 as 
select 
  "ID"
  ,"INVENTORY_ID"
  ,"STOCK_ID" ORDER_ID
  ,"STOCK_ID" ORDER_ID2
  ,"RQ_QUANTITY"
  ,"APP_QUANTITY"
  ,"CURRENT_BALANCE"
--  ,"UNIT_PRICE"
  ,"TOTAL_PRICE" UNIT_PRICE
--  ,"SKU"
--  ,"DESCRIPTION"
  ,"CREATED_DATE" CREATED_AT
  ,"UPDATED_DATE" UPDATED_AT
  ,"CREATED_BY"
  ,"UPDATED_BY"
  ,"DELETED"
  ,"DELETED_DATE" DELETED_AT
from ESTOR.STOCK_ITEMS
;

select count(*) from estor.stock_items;
select count(*) from oi1;

select * from oi1 order by created_by;

select
  count(*)
--  count(app_quantity) -- over () 
from oi1 
--left join inventories_items
;
select * from oi1 where app_quantity is null;
select * from o2 where id = 32;

select count(*) from inventory_items ii where ii.checkout_transaction_id is not null;

create table oi2 as
select rownum id2, t.* from 
(select * from oi1 order by created_at) t;

select * from oi2;

select 
  ii.id,ii.CHECKOUT_TRANSACTION_ID
  ,oi2.id,oi2.id2
from inventory_items ii
left join oi2 on oi2.id2 = ii.CHECKOUT_TRANSACTION_ID
where ii.CHECKOUT_TRANSACTION_ID is not null
;

--update inventory_items ii set ii.CHECKOUT_TRANSACTION_ID = (select id2 from oi2 where oi2.id = ii.CHECKOUT_TRANSACTION_ID);
update inventory_items ii set ii.updated_at = (select created_at from oi2 where oi2.id2 = ii.CHECKOUT_TRANSACTION_ID)
where CHECKOUT_TRANSACTION_ID is not null;

select id,id2,created_at from oi2 where id2 =1;
select id,CHECKOUT_TRANSACTION_ID,created_at,updated_at  from inventory_items where CHECKOUT_TRANSACTION_ID = 1;
select id,CHECKOUT_ID,created_at,updated_at  from estor.estor_items where CHECKOUT_ID = '1' ;
select * from estor.estor_items where CHECKOUT_ID is not null order by CHECKOUT_ID;
select * from oi2 where id = 1;

select * from inventory_items order by id;
select * from estor.estor_items order by id;
select 
--  ei.created_date, ii.created_at 
  ei.updated_date, ii.updated_at 
  ,ei.id,ii.id
from estor.estor_items ei 
left join inventory_items ii on ei.sku = ii.sku
--where ii.created_at is not null
--order by created_date
;

--update inventory_items ii
--set created_at = (select created_date from estor.estor_items ei where ei.sku = ii.sku)
--;
--
--update inventory_items ii
--set updated_at = (select updated_date from estor.estor_items ei where ei.sku = ii.sku)
--;
--
select * from inventory_items ii;
select * from oi2;

select ii.id,ii.created_at,oi2.CREATED_AT from inventory_items ii left join oi2 on oi2.id2 = ii.CHECKOUT_TRANSACTION_ID
where oi2.id is not null;


select 
  oi2.id2 newid ,oi2.id oldid, ii.CHECKOUT_TRANSACTION_ID
from oi2 
left join inventory_items ii on oi2.id = ii.CHECKOUT_TRANSACTION_ID
order by id2;

select * from inventory_items ii where ii.checkout_transaction_id is not null order by ii.CHECKOUT_TRANSACTION_ID;


--update inventory_items  ii set ii.CHECKOUT_TRANSACTION_ID = (select oi2.id2 from oi2 where oi2.id = ii.CHECKOUT_TRANSACTION_ID);

select * from oi2 order by created_at;
select * from inventory_items order by CHECKOUT_TRANSACTION_ID;
select * from order_items order by created_at;

--update order_items oi set id = (select id2 from oi2 where oi2.id = oi.id);


create table orders_bkp as select * from orders;
truncate table orders;
select count(*) from estor.stocks;
select count(*) from orders;
select count(*) from o2;
select * from o2;
select * from orders order by created_at;
select * from order_items where order_id = 1;
select o2.order_no,o.order_no from o2 left join orders o  on o.order_no = o2.order_no
;

select count(*) from order_items;
select count(*) from estor.stock_items;

