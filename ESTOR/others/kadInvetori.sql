select 
  to_char(updated_date, 'DD-MON-YY') 
from stock_items
;

select 
  ei.id
  , to_char(si.updated_date, 'DD-MON-YY') checkdate
  , ei.checkin_date checkin_date
  , ei.checkout_date checkout_date
--  , count(checkin_date) over (partition by to_char(si.updated_date, 'DD-MON-YY'))
from stock_items si
left join stocks s ON s.id = si.stock_id
left join ESTOR_INVENTORIES ev ON ev.id = si.INVENTORY_ID
left join ESTOR_ITEMS ei ON ei.CHECKOUT_ID = s.id
order by id
;




select 
--  DISTINCT(checkdate)
  checkdate
  , checkin_date
  , checkout_date
  , count(checkin_date) over (partition by checkdate) checkin_count
  , count(checkout_date) over (partition by checkdate) checkout_count
from (
  select 
    ei.id,
    to_char(si.updated_date, 'DD-MON-YY') checkdate,
    ei.checkin_date,
    ei.checkout_date
  --  count(checkin_date) over (partition by checkin_date)
  from stock_items si
  left join stocks s ON s.id = si.stock_id
  left join ESTOR_INVENTORIES ev ON ev.id = si.INVENTORY_ID
  left join ESTOR_ITEMS ei ON ei.CHECKOUT_ID = s.id
  order by id
  )
order by checkdate
  ;
  
select   
  DISTINCT(CHECKIN_DATE)
--  checkin_date  
  , count(CHECKIN_DATE) over (partition by CHECKIN_DATE) checkin_count
from ESTOR_ITEMS 
;

select   
  DISTINCT(CHECKOUT_DATE)
--  checkin_date  
  , count(CHECKOUT_DATE) over (partition by CHECKOUT_DATE) checkout_count
from ESTOR_ITEMS 
;










------////////////////////////////////////
select 
  inventory_id
  , checkdate
--  , sum( - NVL(checkin_count, '0')) over (partition by checkdate) before_count
  , NVL(checkin_count, '0') checkin_count
  , NVL(unit_price, '0') unit_price
  , NVL(sum_checkin_prorate, '0') sum_checkin_prorate
  , NVL(checkout_count, '0') checkout_count
  , NVL(sum_checkout_prorate, '0') sum_checkout_prorate
  , sum(NVL(sum_checkin_prorate, '0') - NVL(sum_checkout_prorate, '0') ) over (partition by INVENTORY_ID, checkdate) current_balance
--  , checkout_id
  , ordered_by
from (
    select 
      list_checkdate.INVENTORY_ID
      , list_checkdate.checkdate
    --  , checkin_date
      , checkin_count
      , sum(checkin_count) OVER (PARTITION BY list_checkdate.inventory_id ORDER BY checkdate ROWS UNBOUNDED PRECEDING) sum_checkin_prorate
      , list_checkin.UNIT_PRICE
    --  , si.CURRENT_BALANCE
    --  , sum (ev.quantity + checkin_count) over (partition by checkdate) current_balance
    --  , checkout_date
      , checkout_id
      , checkout_count
      , sum(checkout_count) OVER (PARTITION BY list_checkdate.inventory_id ORDER BY checkdate ROWS UNBOUNDED PRECEDING) sum_checkout_prorate
    --  , sum(checkout_count) OVER (ORDER BY s.CHECKOUT_DATE ROWS UNBOUNDED PRECEDING) sum_checkin_prorate
    --  , sum (si.CURRENT_BALANCE - checkout_count) over (partition by checkdate) balance
      , s.ordered_by
    from (
--      select 
--        distinct(to_char(updated_date, 'DD-MON-YY')) checkdate
--      from estor_items
        select 
--          distinct(to_char(checkin_date, 'DD-MON-YY')) checkdate
            DISTINCT( inventory_id)
            , to_char(checkin_date, 'DD-MON-YY') checkdate
        from estor_items
        where checkin_date is not null
        union
        select 
          DISTINCT( inventory_id)
          ,to_char(checkout_date, 'DD-MON-YY') checkdate
        from estor_items 
        where checkout_date is not null
    ) list_checkdate
    left join (
        select   
        DISTINCT(CHECKIN_DATE) checkin_date
        , ESTOR_ITEMS.INVENTORY_ID
        , ESTOR_ITEMS.UNIT_PRICE
        , count(CHECKIN_DATE) over (partition by ESTOR_ITEMS.INVENTORY_ID, CHECKIN_DATE) checkin_count
      from ESTOR_ITEMS 
    ) list_checkin 
      on 1=1
        and list_checkin.checkin_date = list_checkdate.checkdate 
        and list_checkin.inventory_id = list_checkdate.inventory_id
    left join (
      select   
--      DISTINCT(CHECKOUT_DATE) checkout_date
--      , ESTOR_ITEMS.INVENTORY_ID
        DISTINCT(INVENTORY_ID)
        , CHECKOUT_DATE 
        , checkout_id
        , count(CHECKOUT_DATE) over (partition by CHECKOUT_DATE) checkout_count
      from ESTOR_ITEMS   
      ) list_checkout 
        on 1=1 
          and list_checkout.checkout_date = list_checkdate.checkdate 
          and list_checkout.inventory_id = list_checkdate.inventory_id
    left join STOCK_ITEMS si on si.id = list_checkout.checkout_id
    left join STOCKS s on s.id = si.stock_id
--    left join ESTOR_INVENTORIES ev on ev.id = list_checout.INVENTORY_ID
--    order by checkdate
--    order by inventory_id
)
;

-------/////////////////////////////////////////////////////


select 
  * 
from 
  estor_inventories
--left join 
;


select 
  checkdate
--  , ev.id
--  , sum(13+1) over (order by checkdate rows unbounded PRECEDING) test
  , sum(CURRENT_BALANCE - CHECKIN_COUNT + CHECKOUT_COUNT ) over (partition by checkdate) before_balance
  , checkin_count
  , SUM_CHECKIN_PRORATE
  , CHECKOUT_COUNT
  , SUM_CHECKOUT_PRORATE
  , CURRENT_BALANCE
  , ordered_by
from 
  INVENTORy_CARD
;

select 
  CHECKIN_DATE,
  checkin_count_prorate,
   sum(checkin_count_prorate) OVER (ORDER BY CHECKIN_DATE ROWS UNBOUNDED PRECEDING) sum_checkin_prorate
from (
  select 
    distinct(CHECKIN_DATE) ,
    count(CHECKIN_DATE) over (partition by checkin_date) checkin_count_prorate
  from estor_items
  where checkin_date is not null
  order by checkin_date
)
;
select 
  CHECKOUT_DATE,
  checkout_count_prorate,
   sum(checkout_count_prorate) OVER (ORDER BY CHECKOUT_DATE ROWS UNBOUNDED PRECEDING) sum_checkout_prorate
from (
  select 
    distinct(CHECKOUT_DATE),
    count(CHECKOUT_DATE) over (partition by CHECKOUT_date) checkout_count_prorate
  from estor_items
  where checkout_date is not null
  order by CHECKOUT_date
)
;