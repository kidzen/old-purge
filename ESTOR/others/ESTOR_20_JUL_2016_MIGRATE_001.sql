--------------------------------------------------------
--  File created - Wednesday-July-20-2016   
--------------------------------------------------------
DROP TABLE "ACTIVITY_LOGS" cascade constraints;
DROP TABLE "AIRCOND_LIST" cascade constraints;
DROP TABLE "AUTH_ITEM" cascade constraints;
DROP TABLE "AUTH_RULE" cascade constraints;
DROP TABLE "CATEGORIES" cascade constraints;
DROP TABLE "ESTOR_CACHE" cascade constraints;
DROP TABLE "ESTOR_INVENTORIES" cascade constraints;
DROP TABLE "ESTOR_ITEMS" cascade constraints;
DROP TABLE "ESTOR_ROLES" cascade constraints;
DROP TABLE "ESTOR_SESSION" cascade constraints;
DROP TABLE "PACKAGE" cascade constraints;
DROP TABLE "PEOPLE" cascade constraints;
DROP TABLE "STOCKS" cascade constraints;
DROP TABLE "STOCK_ITEMS" cascade constraints;
DROP TABLE "USAGE_LIST" cascade constraints;
DROP TABLE "VENDORS" cascade constraints;
DROP SEQUENCE "ACTIVITY_LOGS_SEQ";
DROP SEQUENCE "AIRCOND_LIST_SEQ";
DROP SEQUENCE "CATEGORIES_SEQ";
DROP SEQUENCE "CATEGORIES_SEQ1";
DROP SEQUENCE "ESTOR_INVENTORIES_SEQ";
DROP SEQUENCE "ESTOR_INVENTORIES_SEQ1";
DROP SEQUENCE "ESTOR_INVENTORIES_SEQ2";
DROP SEQUENCE "ESTOR_ITEMS_SEQ";
DROP SEQUENCE "ESTOR_ITEMS_SEQ1";
DROP SEQUENCE "ESTOR_ITEMS_SEQ2";
DROP SEQUENCE "ESTOR_ROLES_SEQ";
DROP SEQUENCE "ESTOR_SESSION_SEQ";
DROP SEQUENCE "PACKAGE_SEQ";
DROP SEQUENCE "PEOPLE_SEQ";
DROP SEQUENCE "STOCKS_SEQ";
DROP SEQUENCE "STOCK_ITEMS_SEQ";
DROP SEQUENCE "USAGE_LIST_SEQ";
DROP SEQUENCE "USAGE_LIST_SEQ1";
DROP VIEW "LIST_YEAR";
DROP VIEW "REPORT_ALL";
DROP VIEW "REPORT_ALL2";
DROP VIEW "REPORT_CURRENT";
DROP VIEW "REPORT_IN_QUARTER";
DROP VIEW "REPORT_OUT_QUARTER";
DROP VIEW "REPORT_PREVIOUS_Q1";
--------------------------------------------------------
--  DDL for Sequence ACTIVITY_LOGS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ACTIVITY_LOGS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence AIRCOND_LIST_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "AIRCOND_LIST_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence CATEGORIES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "CATEGORIES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence ESTOR_INVENTORIES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR_INVENTORIES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 81 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence ESTOR_ITEMS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR_ITEMS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence ESTOR_ROLES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR_ROLES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 41 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence ESTOR_SESSION_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR_SESSION_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence PACKAGE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "PACKAGE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence PEOPLE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "PEOPLE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence STOCKS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "STOCKS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 221 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence STOCK_ITEMS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "STOCK_ITEMS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 201 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence USAGE_LIST_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "USAGE_LIST_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Table ACTIVITY_LOGS
--------------------------------------------------------

  CREATE TABLE "ACTIVITY_LOGS" 
   (	"ID" NUMBER(11,0), 
	"PERSON_ID" NUMBER(11,0), 
	"ACTION" VARCHAR2(255), 
	"MODEL" VARCHAR2(255), 
	"MODEL_ID" NUMBER(11,0), 
	"TITLE" VARCHAR2(255), 
	"DESCRIPTION" VARCHAR2(255), 
	"CREATED" TIMESTAMP (0), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0)
   )
--------------------------------------------------------
--  DDL for Table AIRCOND_LIST
--------------------------------------------------------

  CREATE TABLE "AIRCOND_LIST" 
   (	"ID" NUMBER(11,0), 
	"LOCATION" VARCHAR2(50), 
	"FLOOR" VARCHAR2(5), 
	"ADDRESS" VARCHAR2(20), 
	"TYPE" VARCHAR2(20), 
	"BRAND" VARCHAR2(20), 
	"HP" FLOAT(126), 
	"SIRI_NO" VARCHAR2(20), 
	"KEW_PA" VARCHAR2(20), 
	"MODEL" VARCHAR2(20)
   )
--------------------------------------------------------
--  DDL for Table AUTH_ITEM
--------------------------------------------------------

  CREATE TABLE "AUTH_ITEM" 
   (	"NAME" VARCHAR2(64), 
	"TYPE" NUMBER, 
	"DESCRIPTION" VARCHAR2(64), 
	"RULE_NAME" VARCHAR2(64), 
	"DATA" VARCHAR2(64), 
	"CREATED_AT" NUMBER, 
	"UPDATED_AT" NUMBER
   )
--------------------------------------------------------
--  DDL for Table AUTH_RULE
--------------------------------------------------------

  CREATE TABLE "AUTH_RULE" 
   (	"NAME" VARCHAR2(64), 
	"DATA" VARCHAR2(64), 
	"CREATED_AT" NUMBER, 
	"UPDATED_AT" NUMBER
   )
--------------------------------------------------------
--  DDL for Table CATEGORIES
--------------------------------------------------------

  CREATE TABLE "CATEGORIES" 
   (	"ID" NUMBER(11,0), 
	"NAME" VARCHAR2(255), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_BY" VARCHAR2(255)
   )
--------------------------------------------------------
--  DDL for Table ESTOR_CACHE
--------------------------------------------------------

  CREATE TABLE "ESTOR_CACHE" 
   (	"id" CHAR(128), 
	"expire" NUMBER(*,0), 
	"data" BLOB
   )
--------------------------------------------------------
--  DDL for Table ESTOR_INVENTORIES
--------------------------------------------------------

  CREATE TABLE "ESTOR_INVENTORIES" 
   (	"ID" NUMBER(11,0), 
	"CARD_NO" VARCHAR2(255), 
	"CODE_NO" VARCHAR2(255), 
	"DESCRIPTION" VARCHAR2(255), 
	"QUANTITY" NUMBER DEFAULT 0, 
	"CREATED_DATE" TIMESTAMP (0), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_DATE" TIMESTAMP (0), 
	"UPDATED_BY" VARCHAR2(255), 
	"PURCHASED_DATE" DATE, 
	"APPROVED_BY" VARCHAR2(255), 
	"APPROVED_DATE" TIMESTAMP (0), 
	"APPROVED" NUMBER(1,0) DEFAULT 1, 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"CATEGORY_ID" NUMBER(11,0), 
	"MIN_STOCK" NUMBER(11,0) DEFAULT 0, 
	"LOCATION" VARCHAR2(255), 
	 PRIMARY KEY ("ID") ENABLE
   ) ORGANIZATION INDEX NOCOMPRESS
--------------------------------------------------------
--  DDL for Table ESTOR_ITEMS
--------------------------------------------------------

  CREATE TABLE "ESTOR_ITEMS" 
   (	"ID" NUMBER(11,0), 
	"SKU" VARCHAR2(50), 
	"UNIT_PRICE" NUMBER, 
	"CHECKIN_DATE" DATE, 
	"CHECKOUT_DATE" DATE, 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" NUMBER(11,0), 
	"UPDATED_BY" NUMBER(11,0), 
	"INVENTORY_ID" NUMBER(11,0), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (6), 
	"CHECKOUT_ID" NUMBER(11,0)
   )
--------------------------------------------------------
--  DDL for Table ESTOR_ROLES
--------------------------------------------------------

  CREATE TABLE "ESTOR_ROLES" 
   (	"ID" NUMBER(11,0), 
	"NAME" VARCHAR2(255), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_BY" VARCHAR2(255), 
	 PRIMARY KEY ("ID") ENABLE
   ) ORGANIZATION INDEX NOCOMPRESS
--------------------------------------------------------
--  DDL for Table ESTOR_SESSION
--------------------------------------------------------

  CREATE TABLE "ESTOR_SESSION" 
   (	"id" CHAR(40), 
	"expire" NUMBER(*,0), 
	"data" BLOB
   )
--------------------------------------------------------
--  DDL for Table PACKAGE
--------------------------------------------------------

  CREATE TABLE "PACKAGE" 
   (	"ID" NUMBER(11,0), 
	"DETAIL" VARCHAR2(255), 
	"DELIVERY" VARCHAR2(255), 
	"PACKAGE_BY" VARCHAR2(255), 
	"PACKAGE_DATE" DATE, 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (6), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" NUMBER, 
	"UPDATED_BY" NUMBER
   )
--------------------------------------------------------
--  DDL for Table PEOPLE
--------------------------------------------------------

  CREATE TABLE "PEOPLE" 
   (	"ID" NUMBER(11,0), 
	"USERNAME" VARCHAR2(255), 
	"EMAIL" VARCHAR2(255), 
	"PASSWORD" VARCHAR2(255), 
	"ROLE_ID" NUMBER(11,0), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"PASSWORD_RESET_TOKEN" VARCHAR2(255), 
	"AUTH_KEY" VARCHAR2(255)
   )
--------------------------------------------------------
--  DDL for Table STOCKS
--------------------------------------------------------

  CREATE TABLE "STOCKS" 
   (	"ID" NUMBER(11,0), 
	"ORDER_DATE" DATE, 
	"ORDERED_BY" VARCHAR2(255), 
	"TYPE" NUMBER, 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"APPROVED_BY" VARCHAR2(255), 
	"APPROVED_DATE" TIMESTAMP (0), 
	"APPROVED" NUMBER(1,0) DEFAULT 1, 
	"USAGE" VARCHAR2(20 CHAR), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_BY" VARCHAR2(255), 
	"REQUIRED_DATE" DATE, 
	"CHECKOUT_DATE" DATE, 
	"CHECKOUT_BY" VARCHAR2(255), 
	"ORDER_NO" VARCHAR2(20), 
	"PACKAGE_ID" NUMBER(11,0)
   )
--------------------------------------------------------
--  DDL for Table STOCK_ITEMS
--------------------------------------------------------

  CREATE TABLE "STOCK_ITEMS" 
   (	"ID" NUMBER(11,0), 
	"INVENTORY_ID" NUMBER(11,0), 
	"RQ_QUANTITY" NUMBER, 
	"APP_QUANTITY" NUMBER, 
	"CURRENT_BALANCE" NUMBER, 
	"UNIT_PRICE" NUMBER(12,2), 
	"STOCK_ID" NUMBER(11,0), 
	"SKU" VARCHAR2(255), 
	"DESCRIPTION" VARCHAR2(255), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_BY" VARCHAR2(255), 
	"TOTAL_PRICE" NUMBER
   )
--------------------------------------------------------
--  DDL for Table USAGE_LIST
--------------------------------------------------------

  CREATE TABLE "USAGE_LIST" 
   (	"ID" NUMBER(11,0), 
	"NAME" VARCHAR2(20), 
	"DELETED" NUMBER(1,0), 
	"DELETED_DATE" TIMESTAMP (6), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(20), 
	"UPDATED_BY" VARCHAR2(20)
   )
--------------------------------------------------------
--  DDL for Table VENDORS
--------------------------------------------------------

  CREATE TABLE "VENDORS" 
   (	"ID" NUMBER(11,0), 
	"NAME" VARCHAR2(255), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"ADDRESS" VARCHAR2(255), 
	"CONTACT_NO" VARCHAR2(50), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_BY" VARCHAR2(255)
   )
--------------------------------------------------------
--  DDL for View LIST_YEAR
--------------------------------------------------------

  CREATE OR REPLACE VIEW "LIST_YEAR" ("YEAR", "QUARTER") AS 
  select year,quarter from REPORT_IN_QUARTER union
select year,quarter from REPORT_OUT_QUARTER
--------------------------------------------------------
--  DDL for View REPORT_ALL
--------------------------------------------------------

  CREATE OR REPLACE VIEW "REPORT_ALL" ("YEAR", "QUARTER", "COUNT_IN", "PRICE_IN", "COUNT_OUT", "PRICE_OUT", "COUNT_CURRENT", "PRICE_CURRENT") AS 
  select
    list_year.YEAR,
    list_year.QUARTER,
    NVL(REPORT_IN_QUARTER.COUNT, '0') count_in,
    NVL(REPORT_IN_QUARTER.TOTAL_PRICE, '0') price_in,
    NVL(REPORT_out_QUARTER.COUNT, '0') count_out,
    NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0') price_out,

      sum(NVL(REPORT_IN_QUARTER.COUNT, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(REPORT_OUT_QUARTER.COUNT, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING)
    count_current,
      sum(NVL(REPORT_in_QUARTER.TOTAL_PRICE, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING)
    price_current

from list_year
left join REPORT_IN_QUARTER 
on (REPORT_IN_QUARTER.YEAR = list_year.YEAR and list_year.QUARTER = REPORT_IN_QUARTER.QUARTER)
left join REPORT_OUT_QUARTER 
on (list_year.YEAR = REPORT_OUT_QUARTER.YEAR and list_year.QUARTER = REPORT_OUT_QUARTER.QUARTER)
order by list_year.YEAR, list_year.QUARTER
--------------------------------------------------------
--  DDL for View REPORT_ALL2
--------------------------------------------------------

  CREATE OR REPLACE VIEW "REPORT_ALL2" ("YEAR", "QUARTER", "COUNT_CURRENT", "PRICE_CURRENT", "COUNT_IN", "PRICE_IN", "COUNT_OUT", "PRICE_OUT") AS 
  select
    REPORT_IN_QUARTER.YEAR,
    REPORT_IN_QUARTER.QUARTER,
    NVL(REPORT_CURRENT.COUNT, '0') count_CURRENT,
    NVL(REPORT_CURRENT.TOTAL_PRICE, '0') price_CURRENT,
    NVL(REPORT_IN_QUARTER.COUNT, '0') count_in,
    NVL(REPORT_IN_QUARTER.TOTAL_PRICE, '0') price_in,
    NVL(REPORT_out_QUARTER.COUNT, '0') count_out,
    NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0') price_out
from REPORT_IN_QUARTER 
left join REPORT_OUT_QUARTER 
on (REPORT_IN_QUARTER.YEAR = REPORT_OUT_QUARTER.YEAR and REPORT_IN_QUARTER.QUARTER = REPORT_OUT_QUARTER.QUARTER)
left join REPORT_CURRENT
on (REPORT_IN_QUARTER.YEAR = REPORT_CURRENT.YEAR and REPORT_IN_QUARTER.QUARTER = REPORT_CURRENT.QUARTER)
order by REPORT_IN_QUARTER.YEAR, REPORT_IN_QUARTER.QUARTER
--------------------------------------------------------
--  DDL for View REPORT_CURRENT
--------------------------------------------------------

  CREATE OR REPLACE VIEW "REPORT_CURRENT" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (select 
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKIN_DATE, extract(year from CHECKIN_DATE) year,
  to_char(CHECKIN_DATE,'Q') quarter 
from ESTOR_ITEMS
WHERE CHECKIN_DATE IS NOT NULL and CHECKOUT_DATE IS NULL or CHECKOUT_DATE IS NULL or to_char(CHECKOUT_DATE,'q') < 4)

SELECT 
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER
--------------------------------------------------------
--  DDL for View REPORT_IN_QUARTER
--------------------------------------------------------

  CREATE OR REPLACE VIEW "REPORT_IN_QUARTER" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (select 
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKIN_DATE, extract(year from CHECKIN_DATE) year,
  to_char(CHECKIN_DATE,'Q') quarter 
from ESTOR_ITEMS
WHERE CHECKIN_DATE IS NOT NULL and deleted = 0)

SELECT 
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER
--------------------------------------------------------
--  DDL for View REPORT_OUT_QUARTER
--------------------------------------------------------

  CREATE OR REPLACE VIEW "REPORT_OUT_QUARTER" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (select 
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKOUT_DATE, extract(year from CHECKOUT_DATE) year,
  to_char(CHECKOUT_DATE,'Q') quarter 
from ESTOR_ITEMS
WHERE CHECKOUT_DATE IS NOT NULL and deleted = 0)

SELECT 
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER
--------------------------------------------------------
--  DDL for View REPORT_PREVIOUS_Q1
--------------------------------------------------------

  CREATE OR REPLACE VIEW "REPORT_PREVIOUS_Q1" ("QUARTER", "QUANTITY", "TOTAL_PRICE") AS 
  SELECT 
  '1' QUARTER,
  COUNT ("ESTOR_ITEMS".ID) QUANTITY, 
  SUM ("ESTOR_ITEMS".UNIT_PRICE) TOTAL_PRICE 
FROM "ESTOR_ITEMS" 
WHERE "ESTOR_ITEMS".CHECKOUT_DATE IS NULL
AND ESTOR_ITEMS.DELETED = 0
--------------------------------------------------------
--  DDL for Index SYS_C0010455
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0010455" ON "STOCKS" ("ID")
--------------------------------------------------------
--  DDL for Index PEOPLE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PEOPLE_PK" ON "PEOPLE" ("ID")
--------------------------------------------------------
--  DDL for Index ACTIVITY_LOGS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ACTIVITY_LOGS_PK" ON "ACTIVITY_LOGS" ("ID")
--------------------------------------------------------
--  DDL for Index SYS_C0010453
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0010453" ON "VENDORS" ("ID")
--------------------------------------------------------
--  DDL for Index USAGE_LIST_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "USAGE_LIST_PK" ON "USAGE_LIST" ("ID")
--------------------------------------------------------
--  DDL for Index AUTH_ITEM_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AUTH_ITEM_PK" ON "AUTH_ITEM" ("NAME")
--------------------------------------------------------
--  DDL for Index ESTOR_SESSION_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR_SESSION_PK" ON "ESTOR_SESSION" ("id")
--------------------------------------------------------
--  DDL for Index ESTOR_ITEMS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR_ITEMS_PK" ON "ESTOR_ITEMS" ("ID")
--------------------------------------------------------
--  DDL for Index CATEGORIES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CATEGORIES_PK" ON "CATEGORIES" ("ID")
--------------------------------------------------------
--  DDL for Index SYS_C0012441
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0012441" ON "STOCK_ITEMS" ("ID")
--------------------------------------------------------
--  DDL for Index AIRCOND_LIST_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AIRCOND_LIST_PK" ON "AIRCOND_LIST" ("ID")
--------------------------------------------------------
--  DDL for Index PACKAGE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PACKAGE_PK" ON "PACKAGE" ("ID")
--------------------------------------------------------
--  DDL for Index AUTH_RULE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AUTH_RULE_PK" ON "AUTH_RULE" ("NAME")
--------------------------------------------------------
--  DDL for Index ESTOR_CACHE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR_CACHE_PK" ON "ESTOR_CACHE" ("id")
--------------------------------------------------------
--  Constraints for Table USAGE_LIST
--------------------------------------------------------

  ALTER TABLE "USAGE_LIST" MODIFY ("ID" NOT NULL ENABLE)
 
  ALTER TABLE "USAGE_LIST" ADD CONSTRAINT "USAGE_LIST_PK" PRIMARY KEY ("ID") ENABLE
--------------------------------------------------------
--  Constraints for Table ESTOR_SESSION
--------------------------------------------------------

  ALTER TABLE "ESTOR_SESSION" ADD CONSTRAINT "ESTOR_SESSION_PK" PRIMARY KEY ("id") ENABLE
 
  ALTER TABLE "ESTOR_SESSION" MODIFY ("id" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table VENDORS
--------------------------------------------------------

  ALTER TABLE "VENDORS" MODIFY ("ID" NOT NULL ENABLE)
 
  ALTER TABLE "VENDORS" ADD CONSTRAINT "VENDORS_PK" PRIMARY KEY ("ID") ENABLE
--------------------------------------------------------
--  Constraints for Table ESTOR_ROLES
--------------------------------------------------------

  ALTER TABLE "ESTOR_ROLES" ADD PRIMARY KEY ("ID") ENABLE
--------------------------------------------------------
--  Constraints for Table STOCKS
--------------------------------------------------------

  ALTER TABLE "STOCKS" ADD CONSTRAINT "STOCKS_PK" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "STOCKS" MODIFY ("ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table ESTOR_ITEMS
--------------------------------------------------------

  ALTER TABLE "ESTOR_ITEMS" ADD CONSTRAINT "ESTOR_ITEMS_PK" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "ESTOR_ITEMS" MODIFY ("ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table AIRCOND_LIST
--------------------------------------------------------

  ALTER TABLE "AIRCOND_LIST" ADD CONSTRAINT "AIRCOND_LIST_PK" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "AIRCOND_LIST" MODIFY ("ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table STOCK_ITEMS
--------------------------------------------------------

  ALTER TABLE "STOCK_ITEMS" ADD CONSTRAINT "STOCK_ITEMS_PK" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "STOCK_ITEMS" MODIFY ("ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table PEOPLE
--------------------------------------------------------

  ALTER TABLE "PEOPLE" ADD CONSTRAINT "PEOPLE_PK" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "PEOPLE" MODIFY ("ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table ESTOR_INVENTORIES
--------------------------------------------------------

  ALTER TABLE "ESTOR_INVENTORIES" ADD PRIMARY KEY ("ID") ENABLE
--------------------------------------------------------
--  Constraints for Table CATEGORIES
--------------------------------------------------------

  ALTER TABLE "CATEGORIES" ADD CONSTRAINT "CATEGORIES_PK" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "CATEGORIES" MODIFY ("ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table PACKAGE
--------------------------------------------------------

  ALTER TABLE "PACKAGE" ADD CONSTRAINT "PACKAGE_PK" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "PACKAGE" MODIFY ("ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table ACTIVITY_LOGS
--------------------------------------------------------

  ALTER TABLE "ACTIVITY_LOGS" ADD CONSTRAINT "ACTIVITY_LOGS_PK" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "ACTIVITY_LOGS" MODIFY ("ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table ESTOR_CACHE
--------------------------------------------------------

  ALTER TABLE "ESTOR_CACHE" ADD CONSTRAINT "ESTOR_CACHE_PK" PRIMARY KEY ("id") ENABLE
 
  ALTER TABLE "ESTOR_CACHE" MODIFY ("id" NOT NULL ENABLE)
--------------------------------------------------------
--  Ref Constraints for Table ESTOR_INVENTORIES
--------------------------------------------------------

  ALTER TABLE "ESTOR_INVENTORIES" ADD CONSTRAINT "ESTOR_INVENTORIES_FK1" FOREIGN KEY ("CATEGORY_ID")
	  REFERENCES "CATEGORIES" ("ID") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table ESTOR_ITEMS
--------------------------------------------------------

  ALTER TABLE "ESTOR_ITEMS" ADD CONSTRAINT "ESTOR_ITEMS_FK1" FOREIGN KEY ("INVENTORY_ID")
	  REFERENCES "ESTOR_INVENTORIES" ("ID") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table PEOPLE
--------------------------------------------------------

  ALTER TABLE "PEOPLE" ADD CONSTRAINT "PEOPLE_FK1" FOREIGN KEY ("ROLE_ID")
	  REFERENCES "ESTOR_ROLES" ("ID") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table STOCK_ITEMS
--------------------------------------------------------

  ALTER TABLE "STOCK_ITEMS" ADD CONSTRAINT "STOCK_ITEMS_FK1" FOREIGN KEY ("INVENTORY_ID")
	  REFERENCES "ESTOR_INVENTORIES" ("ID") ENABLE
 
  ALTER TABLE "STOCK_ITEMS" ADD CONSTRAINT "STOCK_ITEMS_FK2" FOREIGN KEY ("STOCK_ID")
	  REFERENCES "STOCKS" ("ID") ENABLE
--------------------------------------------------------
--  DDL for Trigger ACTIVITY_LOGS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ACTIVITY_LOGS_TRG" 
BEFORE INSERT ON ACTIVITY_LOGS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    NULL;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ACTIVITY_LOGS_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger AIRCOND_LIST_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "AIRCOND_LIST_TRG" 
BEFORE INSERT ON AIRCOND_LIST 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT AIRCOND_LIST_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "AIRCOND_LIST_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger CATEGORIES_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "CATEGORIES_TRG" 
BEFORE INSERT ON CATEGORIES 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT CATEGORIES_SEQ1.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "CATEGORIES_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger ESTOR_INVENTORIES_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR_INVENTORIES_TRG" 
BEFORE INSERT ON ESTOR_INVENTORIES 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    NULL;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR_INVENTORIES_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger ESTOR_ITEMS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR_ITEMS_TRG" 
BEFORE INSERT ON ESTOR_ITEMS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    NULL;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR_ITEMS_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger ESTOR_ROLES_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR_ROLES_TRG" 
BEFORE INSERT ON ESTOR_ROLES 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT ESTOR_ROLES_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR_ROLES_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger ESTOR_SESSION_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR_SESSION_TRG" 
BEFORE INSERT ON ESTOR_SESSION 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    NULL;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR_SESSION_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger PACKAGE_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PACKAGE_TRG" 
BEFORE INSERT ON PACKAGE 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT PACKAGE_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "PACKAGE_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger PEOPLE_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PEOPLE_TRG" 
BEFORE INSERT ON PEOPLE 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT PEOPLE_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "PEOPLE_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger STOCKS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "STOCKS_TRG" 
BEFORE INSERT ON STOCKS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT STOCKS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "STOCKS_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger STOCK_ITEMS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "STOCK_ITEMS_TRG" 
BEFORE INSERT ON STOCK_ITEMS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT STOCK_ITEMS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "STOCK_ITEMS_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger USAGE_LIST_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "USAGE_LIST_TRG" 
BEFORE INSERT ON USAGE_LIST 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT USAGE_LIST_SEQ1.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "USAGE_LIST_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger VENDORS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "VENDORS_TRG" 
BEFORE INSERT ON VENDORS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT VENDORS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "VENDORS_TRG" ENABLE
