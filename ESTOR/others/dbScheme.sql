
  CREATE TABLE "ESTOR"."ACTIVITY_LOGS" 
   (	"ID" NUMBER(11,0), 
	"PERSON_ID" NUMBER(11,0), 
	"ACTION" VARCHAR2(255), 
	"MODEL" VARCHAR2(255), 
	"MODEL_ID" NUMBER(11,0), 
	"TITLE" VARCHAR2(255), 
	"DESCRIPTION" VARCHAR2(255), 
	"CREATED" TIMESTAMP (0), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0)
   ) ;
--------------------------------------------------------
--  DDL for Table AUTH_ITEM
--------------------------------------------------------

  CREATE TABLE "ESTOR"."AUTH_ITEM" 
   (	"NAME" VARCHAR2(64), 
	"TYPE" NUMBER, 
	"DESCRIPTION" VARCHAR2(64), 
	"RULE_NAME" VARCHAR2(64), 
	"DATA" VARCHAR2(64), 
	"CREATED_AT" NUMBER, 
	"UPDATED_AT" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table AUTH_RULE
--------------------------------------------------------

  CREATE TABLE "ESTOR"."AUTH_RULE" 
   (	"NAME" VARCHAR2(64), 
	"DATA" VARCHAR2(64), 
	"CREATED_AT" NUMBER, 
	"UPDATED_AT" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table CATEGORIES
--------------------------------------------------------

  CREATE TABLE "ESTOR"."CATEGORIES" 
   (	"ID" NUMBER(11,0), 
	"NAME" VARCHAR2(255), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_BY" VARCHAR2(255)
   ) ;
--------------------------------------------------------
--  DDL for Table ESTOR_INVENTORIES
--------------------------------------------------------

  CREATE TABLE "ESTOR"."ESTOR_INVENTORIES" 
   (	"ID" NUMBER(11,0), 
	"CARD_NO" VARCHAR2(255), 
	"CODE_NO" VARCHAR2(255), 
	"DESCRIPTION" VARCHAR2(255), 
	"QUANTITY" NUMBER DEFAULT 0, 
	"CREATED_DATE" TIMESTAMP (0), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_DATE" TIMESTAMP (0), 
	"UPDATED_BY" VARCHAR2(255), 
	"PURCHASED_DATE" DATE, 
	"APPROVED_BY" VARCHAR2(255), 
	"APPROVED_DATE" TIMESTAMP (0), 
	"APPROVED" NUMBER(1,0) DEFAULT 1, 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"CATEGORY_ID" NUMBER(11,0), 
	"MIN_STOCK" NUMBER(11,0), 
	"LOCATION" VARCHAR2(255), 
	 PRIMARY KEY ("ID") ENABLE
   ) ORGANIZATION INDEX NOCOMPRESS ;
--------------------------------------------------------
--  DDL for Table ESTOR_ITEMS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."ESTOR_ITEMS" 
   (	"ID" NUMBER(11,0), 
	"SKU" NUMBER, 
	"UNIT_PRICE" NUMBER, 
	"CHECKIN_DATE" DATE, 
	"CHECKOUT_DATE" DATE, 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" NUMBER(11,0), 
	"UPDATED_BY" NUMBER(11,0), 
	"INVENTORY_ID" NUMBER(11,0), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (6)
   ) ;
--------------------------------------------------------
--  DDL for Table ESTOR_ROLES
--------------------------------------------------------

  CREATE TABLE "ESTOR"."ESTOR_ROLES" 
   (	"ID" NUMBER(11,0), 
	"NAME" VARCHAR2(255), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_BY" VARCHAR2(255), 
	 PRIMARY KEY ("ID") ENABLE
   ) ORGANIZATION INDEX NOCOMPRESS ;
--------------------------------------------------------
--  DDL for Table PACKAGE
--------------------------------------------------------

  CREATE TABLE "ESTOR"."PACKAGE" 
   (	"ID" NUMBER(11,0), 
	"DETAIL" VARCHAR2(255), 
	"DELIVERY" VARCHAR2(255), 
	"PACKAGE_BY" VARCHAR2(255), 
	"PACKAGE_DATE" DATE, 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (6), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" NUMBER, 
	"UPDATED_BY" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table PEOPLE
--------------------------------------------------------

  CREATE TABLE "ESTOR"."PEOPLE" 
   (	"ID" NUMBER(11,0), 
	"USERNAME" VARCHAR2(255), 
	"EMAIL" VARCHAR2(255), 
	"PASSWORD" VARCHAR2(255), 
	"ROLE_ID" NUMBER(11,0), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"PASSWORD_RESET_TOKEN" VARCHAR2(255), 
	"AUTH_KEY" VARCHAR2(255)
   ) ;
--------------------------------------------------------
--  DDL for Table STOCKS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."STOCKS" 
   (	"ID" NUMBER(11,0), 
	"ORDER_DATE" DATE, 
	"ORDERED_BY" VARCHAR2(255), 
	"TYPE" NUMBER, 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"APPROVED_BY" VARCHAR2(255), 
	"APPROVED_DATE" TIMESTAMP (0), 
	"APPROVED" NUMBER(1,0) DEFAULT 1, 
	"USAGE" VARCHAR2(20 CHAR), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_BY" VARCHAR2(255), 
	"REQUIRED_DATE" DATE, 
	"CHECKOUT_DATE" DATE, 
	"CHECKOUT_BY" VARCHAR2(255), 
	"ORDER_NO" VARCHAR2(20), 
	"PACKAGE_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table STOCK_ITEMS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."STOCK_ITEMS" 
   (	"ID" NUMBER(11,0), 
	"INVENTORY_ID" NUMBER(11,0), 
	"RQ_QUANTITY" NUMBER, 
	"APP_QUANTITY" NUMBER, 
	"CURRENT_BALANCE" NUMBER, 
	"UNIT_PRICE" NUMBER(12,2), 
	"STOCK_ID" NUMBER(11,0), 
	"SKU" VARCHAR2(255), 
	"DESCRIPTION" VARCHAR2(255), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_BY" VARCHAR2(255), 
	"TOTAL_PRICE" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table USAGE_LIST
--------------------------------------------------------

  CREATE TABLE "ESTOR"."USAGE_LIST" 
   (	"ID" NUMBER(11,0), 
	"NAME" VARCHAR2(20), 
	"DELETED" NUMBER(1,0), 
	"DELETED_DATE" TIMESTAMP (6), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(20), 
	"UPDATED_BY" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table VENDORS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."VENDORS" 
   (	"ID" NUMBER(11,0), 
	"NAME" VARCHAR2(255), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"ADDRESS" VARCHAR2(255), 
	"CONTACT_NO" VARCHAR2(50), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_BY" VARCHAR2(255)
   ) ;