select
    list_year.year,
    list_year.QUARTER,
    NVL(REPORT_IN_QUARTER.COUNT, '0') count_in,
    NVL(REPORT_IN_QUARTER.TOTAL_PRICE, '0') price_in,
    NVL(REPORT_out_QUARTER.COUNT, '0') count_out,
    NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0') price_out,

      sum(NVL(REPORT_IN_QUARTER.COUNT, '0')) over (ORDER BY list_year.year ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(REPORT_OUT_QUARTER.COUNT, '0')) over (ORDER BY list_year.year ROWS UNBOUNDED PRECEDING)
    count_current,
      sum(NVL(REPORT_in_QUARTER.TOTAL_PRICE, '0')) over (ORDER BY list_year.year ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0')) over (ORDER BY list_year.year ROWS UNBOUNDED PRECEDING)
    price_current

from (
  select year year,quarter from REPORT_IN_QUARTER union
  select year year,quarter from REPORT_OUT_QUARTER
) list_year
left join (
  SELECT 
    DISTINCT YEAR,
    QUARTER,
    COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
    SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
  FROM (
    select 
      ESTOR_ITEMS.SKU,
      ESTOR_ITEMS.UNIT_PRICE,
      ESTOR_ITEMS.INVENTORY_ID,
      CHECKIN_DATE, extract(year from CHECKIN_DATE) year,
      to_char(CHECKIN_DATE,'Q') quarter 
    from ESTOR_ITEMS
    WHERE CHECKIN_DATE IS NOT NULL and deleted = 0
  )
  ORDER BY YEAR,QUARTER
) REPORT_IN_QUARTER
on (REPORT_IN_QUARTER.YEAR = list_year.year and list_year.QUARTER = REPORT_IN_QUARTER.QUARTER)
left join (
  SELECT 
    DISTINCT YEAR,
    QUARTER,
    COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
    SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
  FROM (
    select 
      ESTOR_ITEMS.SKU,
      ESTOR_ITEMS.UNIT_PRICE,
      ESTOR_ITEMS.INVENTORY_ID,
      CHECKOUT_DATE, extract(year from CHECKOUT_DATE) year,
      to_char(CHECKOUT_DATE,'Q') quarter 
    from ESTOR_ITEMS
    WHERE CHECKOUT_DATE IS NOT NULL and deleted = 0
  )
  ORDER BY YEAR,QUARTER
) REPORT_OUT_QUARTER
on (list_year.year = REPORT_OUT_QUARTER.YEAR and list_year.QUARTER = REPORT_OUT_QUARTER.QUARTER)
order by list_year.year, list_year.QUARTER
;
 