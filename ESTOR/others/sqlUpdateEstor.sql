--------------------------------------------------------
--  DDL for View TRANSACTION_IN
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE VIEW "ESTOR"."TRANSACTION_IN" ("ID", "TYPE", "CHECK_DATE", "CHECK_BY", "REFFERENCE", "APPROVED_DATE", "APPROVED_BY", "INVENTORY_ID","UNIT_PRICE", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (
select 
  t.id
  ,t.type
  ,ii.sku
  ,ii.unit_price
  ,ic.inventory_id
  ,v.name vendor_name
  ,t.check_date
  ,t.check_by
  ,to_char(ic.approved_at,'dd-MON-yy') approved_date
  ,ic.approved_by
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
  ,COUNT(*) OVER (PARTITION BY t.ID) COUNT
  ,SUM(ii.UNIT_PRICE) OVER (PARTITION BY t.ID) TOTAL_PRICE
from inventory_items ii
left join inventories_checkin ic on ic.id = ii.checkin_transaction_id
left join transactions t on t.id = ic.transaction_id
left join vendors v on v.id = ic.vendor_id
where ii.deleted = 0
)

SELECT 
  ID,
  TYPE,  
  CHECK_DATE,  
  CHECK_BY,  
  VENDOR_NAME REFFERENCE,
  APPROVED_DATE,  
  APPROVED_BY,  
  INVENTORY_ID,
--  rownum as id,
--  QUARTER,
  COUNT,
  UNIT_PRICE,
  TOTAL_PRICE
FROM DATA1
group by id,count,unit_price,total_price, type, check_date, check_by, approved_date, approved_by, inventory_id, vendor_name
ORDER BY id
;
--------------------------------------------------------
--  DDL for View TRANSACTION_OUT
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE VIEW "ESTOR"."TRANSACTION_OUT" ("ID", "TYPE", "CHECK_DATE", "CHECK_BY", "REFFERENCE", "APPROVED_DATE", "APPROVED_BY", "INVENTORY_ID","UNIT_PRICE", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (
select 
  t.id
  ,t.type
  ,LISTAGG(ii.sku, ', ') WITHIN GROUP (ORDER BY ii.id) items
--  ,ii.sku
  ,ii.unit_price
  ,oi.inventory_id
  ,t.check_date
  ,t.check_by
  ,to_char(o.approved_at,'dd-MON-yy') approved_date
  ,o.order_no
  ,o.approved_by
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
  ,COUNT(*) OVER (PARTITION BY t.ID) COUNT
  ,SUM(ii.UNIT_PRICE) OVER (PARTITION BY t.ID) TOTAL_PRICE
from inventory_items ii
left join order_items oi on oi.id = ii.checkout_transaction_id
left join orders o on o.id = oi.order_id
left join transactions t on t.id = o.transaction_id
--where ii.deleted = 0 and ii.checkout_transaction_id is not null
  where ii.deleted = 0 and ii.checkout_transaction_id is not null and o.APPROVED = 1
  group by   
    o.id
    ,oi.ID
  --  ,ii.id
    ,ii.UNIT_PRICE
    ,oi.APP_QUANTITY
    ,t.id
  ,t.type
  ,oi.inventory_id
  ,t.check_date
  ,t.check_by
  ,o.order_no
  ,o.approved_by
  ,o.approved_at
  ,t.check_date
  
  order by o.id
)

SELECT 
--  *
  ID,
  TYPE,  
  CHECK_DATE,  
  CHECK_BY,  
  ORDER_NO REFFERENCE,  
  APPROVED_DATE,  
  APPROVED_BY,  
  INVENTORY_ID,
--  rownum as id,
--  QUARTER,
  UNIT_PRICE,
  COUNT,
  TOTAL_PRICE
FROM DATA1
group by id,count,unit_price,total_price, type, check_date, check_by, approved_date, approved_by, inventory_id,order_no
--group by 
ORDER BY id;
--------------------------------------------------------
--  DDL for View TRANSACTIONS_ALL
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE VIEW "ESTOR"."TRANSACTIONS_ALL" ("ID", "TYPE", "CHECK_DATE", "CHECK_BY", "REFFERENCE", "INVENTORY_ID", "UNIT_PRICE", "COUNT_IN", "PRICE_IN", "COUNT_OUT", "PRICE_OUT", "COUNT_CURRENT", "PRICE_CURRENT") AS 
  select 
  t."ID",t."TYPE",t."CHECK_DATE",t."CHECK_BY",t."REFFERENCE",t."INVENTORY_ID",t."UNIT_PRICE",t."COUNT_IN",t."PRICE_IN",t."COUNT_OUT",t."PRICE_OUT",
  sum(COUNT_IN - COUNT_OUT) over(PARTITION BY INVENTORY_ID ORDER BY ID ROWS UNBOUNDED PRECEDING) count_current,
  sum(PRICE_IN - PRICE_OUT) over(PARTITION BY INVENTORY_ID ORDER BY ID ROWS UNBOUNDED PRECEDING) price_current
from (
    select
    --    rownum as id,
        IDS.ID,
        IDS.TYPE,
        IDS.CHECK_DATE,
        IDS.CHECK_BY,
        IDS.REFFERENCE,
        IDS.INVENTORY_ID,
        IDS.UNIT_PRICE,
        case when IDS.TYPE = 1 then IDS.COUNT else 0 end count_in,
        case when IDS.TYPE = 1 then IDS.TOTAL_PRICE else 0 end price_in,
        case when IDS.TYPE = 2 then IDS.COUNT else 0 end count_out,
        case when IDS.TYPE = 2 then IDS.TOTAL_PRICE else 0 end price_out
    from (select * from TRANSACTION_OUT union 
    select * from TRANSACTION_IN
    ) IDS
    order by IDS.ID
) t
;

--------------------------------------------------------
--  DDL for View TRANSACTION_OUT_SISKEN
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE VIEW "ESTOR"."TRANSACTION_OUT_SISKEN" ("ID", "ARAHAN_KERJA_ID", "ORDER_NO", "CHECK_DATE", "STATUS", "ORDER_ITEMS_ID", "CATEGORY_NAME", "CARD_NO", "CODE_NO", "INVENTORY_DESCRIPTION", "ITEMS", "QUANTITY", "UNIT_PRICE", "TOTAL_PRICE") AS 
  select rownum as id,t."ARAHAN_KERJA_ID",t."ORDER_NO",t."CHECK_DATE",t."STATUS",t."ORDER_ITEMS_ID",t."CATEGORY_NAME",t."CARD_NO",t."CODE_NO",t."INVENTORY_DESCRIPTION",t."ITEMS",t."QUANTITY",t."UNIT_PRICE",t."TOTAL_PRICE" from
(
select  
    arahan_kerja_id
    ,order_no
    ,check_date
    ,status
    ,order_items_id
    ,category_name
    ,card_no
    ,code_no
    ,inventory_description
    , LISTAGG(sku, ', ') WITHIN GROUP (ORDER BY sku) items
    ,quantity
    ,unit_price
    ,total_price
from
  (
  select
    o.arahan_kerja_id ARAHAN_KERJA_id
    ,o.order_no
    ,to_char(o.approved_at,'dd-MON-yy') check_date
    ,o.approved status
    ,oi.id order_items_id
    ,c.name category_name
    ,i.card_no
    ,i.code_no
    ,i.description inventory_description
    ,ii.sku
    ,ii.unit_price
    ,count(*) over (partition by o.order_no) quantity
    ,sum(ii.unit_price) over (partition by o.order_no) total_price
  from inventory_items ii
  left join order_items oi on oi.id = ii.checkout_transaction_id
  left join orders o on o.id = oi.order_id
  left join transactions t on t.id = o.transaction_id
  left join inventories i on i.id = oi.inventory_id
  left join categories c on c.id = i.category_id
  where ii.checkout_transaction_id is not null 
  and o.arahan_kerja_id is not null
  order by o.order_no desc
  )
group by 
    arahan_kerja_id
    ,order_no
    ,check_date
    ,status
    ,order_items_id
    ,category_name
    ,card_no
    ,code_no
    ,inventory_description
--    ,sku
    , quantity
    , unit_price
    , total_price
order by order_no desc,order_items_id desc
) t;



ALTER TABLE categories MODIFY id
GENERATED BY DEFAULT ON NULL AS IDENTITY (START WITH LIMIT VALUE);
ALTER TABLE inventories MODIFY id
GENERATED BY DEFAULT ON NULL AS IDENTITY (START WITH LIMIT VALUE);
ALTER TABLE inventories_checkin MODIFY id
GENERATED BY DEFAULT ON NULL AS IDENTITY (START WITH LIMIT VALUE);
ALTER TABLE inventory_items MODIFY id
GENERATED BY DEFAULT ON NULL AS IDENTITY (START WITH LIMIT VALUE);
ALTER TABLE order_items MODIFY id
GENERATED BY DEFAULT ON NULL AS IDENTITY (START WITH LIMIT VALUE);
ALTER TABLE orders MODIFY id
GENERATED BY DEFAULT ON NULL AS IDENTITY (START WITH LIMIT VALUE);
ALTER TABLE people MODIFY id
GENERATED BY DEFAULT ON NULL AS IDENTITY (START WITH LIMIT VALUE);
ALTER TABLE "ROLES" MODIFY id
GENERATED BY DEFAULT ON NULL AS IDENTITY (START WITH LIMIT VALUE);
ALTER TABLE "TRANSACTIONS" MODIFY id
GENERATED BY DEFAULT ON NULL AS IDENTITY (START WITH LIMIT VALUE);
ALTER TABLE "VEHICLE_LIST" MODIFY id
GENERATED BY DEFAULT ON NULL AS IDENTITY (START WITH LIMIT VALUE);
ALTER TABLE "VENDORS" MODIFY id
GENERATED BY DEFAULT ON NULL AS IDENTITY (START WITH LIMIT VALUE);





--------------------------------------------------------
--  DDL for Index INV_CHK_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."INV_CHK_PK_DLTD_IDX" ON "ESTOR"."INVENTORIES_CHECKIN" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index VHCL_LST_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."VHCL_LST_PK_DLTD_IDX" ON "ESTOR"."VEHICLE_LIST" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index ACT_LG_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."ACT_LG_PK_DLTD_IDX" ON "ESTOR"."ACTIVITY_LOGS" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index VDR_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."VDR_PK_DLTD_IDX" ON "ESTOR"."VENDORS" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index PCKG_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."PCKG_PK_DLTD_IDX" ON "ESTOR"."PACKAGE" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index RLS_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."RLS_PK_DLTD_IDX" ON "ESTOR"."ROLES" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index ACND_LST_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."ACND_LST_PK_DLTD_IDX" ON "ESTOR"."AIRCOND_LIST" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index ODR_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."ODR_PK_DLTD_IDX" ON "ESTOR"."ORDERS" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index USG_LST_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."USG_LST_PK_DLTD_IDX" ON "ESTOR"."USAGE_LIST" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index CTG_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."CTG_PK_DLTD_IDX" ON "ESTOR"."CATEGORIES" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index ODR_ITM_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."ODR_ITM_PK_DLTD_IDX" ON "ESTOR"."ORDER_ITEMS" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index PPL_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."PPL_PK_DLTD_IDX" ON "ESTOR"."PEOPLE" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index INV_ITM_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."INV_ITM_PK_DLTD_IDX" ON "ESTOR"."INVENTORY_ITEMS" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index TRX_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."TRX_PK_DLTD_IDX" ON "ESTOR"."TRANSACTIONS" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index INV_PK_DLTD_IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."INV_PK_DLTD_IDX" ON "ESTOR"."INVENTORIES" ("ID","DELETED") 
  ;
--------------------------------------------------------
--  DDL for Index INV_CHK_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."INV_CHK_DLTD_IDX" ON "ESTOR"."INVENTORIES_CHECKIN" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index VHCL_LST_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."VHCL_LST_DLTD_IDX" ON "ESTOR"."VEHICLE_LIST" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index VDR_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."VDR_DLTD_IDX" ON "ESTOR"."VENDORS" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index PCKG_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."PCKG_DLTD_IDX" ON "ESTOR"."PACKAGE" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index RLS_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."RLS_DLTD_IDX" ON "ESTOR"."ROLES" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index ACND_LST_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."ACND_LST_DLTD_IDX" ON "ESTOR"."AIRCOND_LIST" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index ODR_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."ODR_DLTD_IDX" ON "ESTOR"."ORDERS" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index USG_LST_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."USG_LST_DLTD_IDX" ON "ESTOR"."USAGE_LIST" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index CTG_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."CTG_DLTD_IDX" ON "ESTOR"."CATEGORIES" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index ODR_ITM_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."ODR_ITM_DLTD_IDX" ON "ESTOR"."ORDER_ITEMS" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index PPL_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."PPL_DLTD_IDX" ON "ESTOR"."PEOPLE" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index INV_ITM_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."INV_ITM_DLTD_IDX" ON "ESTOR"."INVENTORY_ITEMS" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index TRX_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."TRX_DLTD_IDX" ON "ESTOR"."TRANSACTIONS" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index INV_DLTD_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."INV_DLTD_IDX" ON "ESTOR"."INVENTORIES" ("DELETED" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index INV_CHK_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."INV_CHK_CREATED_AT_IDX" ON "ESTOR"."INVENTORIES_CHECKIN" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index VHCL_LST_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."VHCL_LST_CREATED_AT_IDX" ON "ESTOR"."VEHICLE_LIST" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index ACT_LG_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."ACT_LG_CREATED_AT_IDX" ON "ESTOR"."ACTIVITY_LOGS" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index VDR_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."VDR_CREATED_AT_IDX" ON "ESTOR"."VENDORS" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index PCKG_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."PCKG_CREATED_AT_IDX" ON "ESTOR"."PACKAGE" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index RLS_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."RLS_CREATED_AT_IDX" ON "ESTOR"."ROLES" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index ACND_LST_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."ACND_LST_CREATED_AT_IDX" ON "ESTOR"."AIRCOND_LIST" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index ODR_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."ODR_CREATED_AT_IDX" ON "ESTOR"."ORDERS" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index USG_LST_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."USG_LST_CREATED_AT_IDX" ON "ESTOR"."USAGE_LIST" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index CTG_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."CTG_CREATED_AT_IDX" ON "ESTOR"."CATEGORIES" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index ODR_ITM_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."ODR_ITM_CREATED_AT_IDX" ON "ESTOR"."ORDER_ITEMS" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index PPL_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."PPL_CREATED_AT_IDX" ON "ESTOR"."PEOPLE" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index INV_ITM_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."INV_ITM_CREATED_AT_IDX" ON "ESTOR"."INVENTORY_ITEMS" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index TRX_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."TRX_CREATED_AT_IDX" ON "ESTOR"."TRANSACTIONS" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index INV_CREATED_AT_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."INV_CREATED_AT_IDX" ON "ESTOR"."INVENTORIES" ("CREATED_AT" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index INV_CHK_TRX_ID_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."INV_CHK_TRX_ID_IDX" ON "ESTOR"."INVENTORIES_CHECKIN" ("TRANSACTION_ID" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index INV_CHK_INV_ID_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."INV_CHK_INV_ID_IDX" ON "ESTOR"."INVENTORIES_CHECKIN" ("INVENTORY_ID" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index INV_CHK_VDR_ID_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."INV_CHK_VDR_ID_IDX" ON "ESTOR"."INVENTORIES_CHECKIN" ("VENDOR_ID" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index ODR_TRX_ID_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."ODR_TRX_ID_IDX" ON "ESTOR"."ORDERS" ("TRANSACTION_ID" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index ODR_ARHN_KRJA_ID_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."ODR_ARHN_KRJA_ID_IDX" ON "ESTOR"."ORDERS" ("ARAHAN_KERJA_ID" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index ODR_VHCL_ID_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."ODR_VHCL_ID_IDX" ON "ESTOR"."ORDERS" ("VEHICLE_ID" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index ODR_ITM_ODR_ID_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."ODR_ITM_ODR_ID_IDX" ON "ESTOR"."ORDER_ITEMS" ("ORDER_ID" ASC) 
  ;
--------------------------------------------------------
--  DDL for Index ODR_ITM_INV_ID_IDX
--------------------------------------------------------

  CREATE INDEX "ESTOR"."ODR_ITM_INV_ID_IDX" ON "ESTOR"."ORDER_ITEMS" ("INVENTORY_ID" ASC) 
  ;