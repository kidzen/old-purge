--TOTAL DATA
SELECT 
  "ESTOR_ITEMS".ID as ID, 
  "ESTOR_INVENTORIES".QUANTITY as QUANTITY, 
  "ESTOR_ITEMS".SKU as SKU, 
  "ESTOR_ITEMS".UNIT_PRICE as UNIT_PRICE, 
  "ESTOR_ITEMS".CHECKOUT_DATE as CHECKOUT_DATE,  
  "CATEGORIES".NAME as CATEGORY,
  "ESTOR_INVENTORIES".DESCRIPTION as DESCRIPTION,
  "ESTOR_INVENTORIES".APPROVED as APPROVED
FROM "ESTOR_ITEMS" 
LEFT JOIN "ESTOR_INVENTORIES" ON "ESTOR_ITEMS"."INVENTORY_ID" = "ESTOR_INVENTORIES"."ID" 
LEFT JOIN "CATEGORIES" ON "ESTOR_INVENTORIES"."CATEGORY_ID" = "CATEGORIES"."ID"
ORDER BY "ESTOR_INVENTORIES".DESCRIPTION;



--TOTAL ITEMS CURRENTLY IN STORE
SELECT 
  COUNT(*) TOTAL_ITEMS
--  "ESTOR_ITEMS".ID as ID, 
--  "ESTOR_INVENTORIES".DESCRIPTION as DESCRIPTION,
--  "ESTOR_INVENTORIES".QUANTITY as QUANTITY, 
--  "ESTOR_ITEMS".UNIT_PRICE as UNIT_PRICE, 
----  SUM("ESTOR_ITEMS".UNIT_PRICE) AS TOTAL_PRICE, 
--  "ESTOR_ITEMS".CHECKOUT_DATE as CHECKOUT_DATE,  
--  "CATEGORIES".NAME as CATEGORY,
--  "ESTOR_INVENTORIES".APPROVED as APPROVED
FROM "ESTOR_ITEMS" 
LEFT JOIN "ESTOR_INVENTORIES" ON "ESTOR_ITEMS"."INVENTORY_ID" = "ESTOR_INVENTORIES"."ID" 
LEFT JOIN "CATEGORIES" ON "ESTOR_INVENTORIES"."CATEGORY_ID" = "CATEGORIES"."ID"
WHERE "ESTOR_ITEMS".CHECKOUT_DATE IS NULL
AND ESTOR_ITEMS.DELETED = 0
AND ESTOR_INVENTORIES.DELETED = 0
AND ESTOR_INVENTORIES.APPROVED = 2
ORDER BY "ESTOR_INVENTORIES".DESCRIPTION;


            
--TOTAL ITEMS 41 CURRENTLY IN STORE
SELECT 
--  COUNT(*) TOTAL_ITEMS
  "ESTOR_ITEMS".ID as ID, 
  "ESTOR_INVENTORIES".DESCRIPTION as DESCRIPTION,
  "ESTOR_INVENTORIES".QUANTITY as QUANTITY, 
  "ESTOR_ITEMS".UNIT_PRICE as UNIT_PRICE, 
--  SUM("ESTOR_ITEMS".UNIT_PRICE) AS TOTAL_PRICE, 
  "ESTOR_ITEMS".CHECKOUT_DATE as CHECKOUT_DATE,  
  "CATEGORIES".NAME as CATEGORY,
  "ESTOR_INVENTORIES".APPROVED as APPROVED
FROM "ESTOR_ITEMS" 
LEFT JOIN "ESTOR_INVENTORIES" ON "ESTOR_ITEMS"."INVENTORY_ID" = "ESTOR_INVENTORIES"."ID" 
LEFT JOIN "CATEGORIES" ON "ESTOR_INVENTORIES"."CATEGORY_ID" = "CATEGORIES"."ID"
WHERE "ESTOR_ITEMS".CHECKOUT_DATE IS NULL
--AND ESTOR_ITEMS.INVENTORY_ID = 27
AND ESTOR_ITEMS.DELETED = 0
AND ESTOR_INVENTORIES.DELETED = 0
AND ESTOR_INVENTORIES.APPROVED = 2
ORDER BY "ESTOR_INVENTORIES".DESCRIPTION;



--TOTAL PRICE            41 28 27
SELECT 
  DISTINCT ESTOR_INVENTORIES.ID AS ID,
  ESTOR_INVENTORIES.DESCRIPTION AS DESCRIPTION,
--  ESTOR_INVENTORIES.QUANTITY AS QUANTITY,
  COUNT ("ESTOR_ITEMS".ID) OVER (PARTITION BY ESTOR_INVENTORIES.ID) QUANTITY, 
  SUM ("ESTOR_ITEMS".UNIT_PRICE) OVER (PARTITION BY ESTOR_INVENTORIES.ID) TOTAL_PRICE 
--  SUM("ESTOR_ITEMS".UNIT_PRICE) AS TOTAL_PRICE 
FROM "ESTOR_ITEMS" 
LEFT JOIN "ESTOR_INVENTORIES" ON "ESTOR_ITEMS"."INVENTORY_ID" = "ESTOR_INVENTORIES"."ID" 
LEFT JOIN "CATEGORIES" ON "ESTOR_INVENTORIES"."CATEGORY_ID" = "CATEGORIES"."ID"
WHERE ESTOR_ITEMS.INVENTORY_ID IN (27,41, 28)
AND "ESTOR_ITEMS".CHECKOUT_DATE IS NULL
AND ESTOR_ITEMS.DELETED = 0
AND ESTOR_INVENTORIES.DELETED = 0
AND ESTOR_INVENTORIES.APPROVED = 2
;

--TOTAL OUT            41 28 27
SELECT 
  DISTINCT ESTOR_INVENTORIES.ID AS ID,
  ESTOR_INVENTORIES.DESCRIPTION AS DESCRIPTION,
--  ESTOR_INVENTORIES.QUANTITY AS QUANTITY,
  COUNT ("ESTOR_ITEMS".ID) OVER (PARTITION BY ESTOR_INVENTORIES.ID) QUANTITY, 
  SUM ("ESTOR_ITEMS".UNIT_PRICE) OVER (PARTITION BY ESTOR_INVENTORIES.ID) TOTAL_PRICE 
--  SUM("ESTOR_ITEMS".UNIT_PRICE) AS TOTAL_PRICE 
FROM "ESTOR_ITEMS" 
LEFT JOIN "ESTOR_INVENTORIES" ON "ESTOR_ITEMS"."INVENTORY_ID" = "ESTOR_INVENTORIES"."ID" 
LEFT JOIN "CATEGORIES" ON "ESTOR_INVENTORIES"."CATEGORY_ID" = "CATEGORIES"."ID"
WHERE ESTOR_ITEMS.INVENTORY_ID IN (27,41, 28)
AND "ESTOR_ITEMS".CHECKOUT_DATE IS NOT NULL
AND ESTOR_ITEMS.DELETED = 0
AND ESTOR_INVENTORIES.DELETED = 0
AND ESTOR_INVENTORIES.APPROVED = 2
;

--TOTAL IN            41 28 27
SELECT 
  DISTINCT ESTOR_INVENTORIES.ID AS ID,
  ESTOR_INVENTORIES.DESCRIPTION AS DESCRIPTION,
--  ESTOR_INVENTORIES.QUANTITY AS QUANTITY,
  COUNT ("ESTOR_ITEMS".ID) OVER (PARTITION BY ESTOR_INVENTORIES.ID) QUANTITY, 
  SUM ("ESTOR_ITEMS".UNIT_PRICE) OVER (PARTITION BY ESTOR_INVENTORIES.ID) TOTAL_PRICE 
--  SUM("ESTOR_ITEMS".UNIT_PRICE) AS TOTAL_PRICE 
FROM "ESTOR_ITEMS" 
LEFT JOIN "ESTOR_INVENTORIES" ON "ESTOR_ITEMS"."INVENTORY_ID" = "ESTOR_INVENTORIES"."ID" 
LEFT JOIN "CATEGORIES" ON "ESTOR_INVENTORIES"."CATEGORY_ID" = "CATEGORIES"."ID"
WHERE ESTOR_ITEMS.INVENTORY_ID IN (27,41, 28)
AND "ESTOR_ITEMS".CHECKIN_DATE IS NULL
AND ESTOR_ITEMS.DELETED = 0
AND ESTOR_INVENTORIES.DELETED = 0
AND ESTOR_INVENTORIES.APPROVED = 2
;

--TOTAL PRICE 1 ITEMS            41 28 27
SELECT 
  ESTOR_INVENTORIES.ID AS ID,
  "ESTOR_ITEMS".UNIT_PRICE
--  SUM (DISTINCT "ESTOR_ITEMS".UNIT_PRICE) DISTINCT_TOTAL_PRICE 
--  SUM("ESTOR_ITEMS".UNIT_PRICE) AS TOTAL_PRICE 
FROM "ESTOR_ITEMS" 
LEFT JOIN "ESTOR_INVENTORIES" ON "ESTOR_ITEMS"."INVENTORY_ID" = "ESTOR_INVENTORIES"."ID" 
LEFT JOIN "CATEGORIES" ON "ESTOR_INVENTORIES"."CATEGORY_ID" = "CATEGORIES"."ID"
WHERE "ESTOR_ITEMS".CHECKOUT_DATE IS NULL
AND ESTOR_ITEMS.INVENTORY_ID IN (41)
AND ESTOR_ITEMS.DELETED = 0
AND ESTOR_INVENTORIES.DELETED = 0
AND ESTOR_INVENTORIES.APPROVED = 2
;



--REPORT
SELECT 
  ID,
  DESCRIPTION,
  OLD_QUANTITY,  --i
  OLD_TOTAL_PRICE,  --a
  IN_QUANTITY,    --ii
  IN_TOTAL_PRICE,    --b
  OUT_QUANTITY,  --iii
  OUT_TOTAL_PRICE,  --c
  NOW_QUANTITY,
  NOW_TOTAL_PRICE --d
  
FROM
(
  SELECT 
    REPORT_NOW.ID,
    REPORT_NOW.DESCRIPTION,
    REPORT_NOW.QUANTITY AS OLD_QUANTITY,  --i
    REPORT_NOW.TOTAL_PRICE AS OLD_TOTAL_PRICE,  --a
    REPORT_IN.QUANTITY AS IN_QUANTITY,    --ii
    REPORT_IN.TOTAL_PRICE AS IN_TOTAL_PRICE,    --b
    REPORT_OUT.QUANTITY AS OUT_QUANTITY,  --iii
    REPORT_OUT.TOTAL_PRICE AS OUT_TOTAL_PRICE,  --c
    SUM(REPORT_NOW.QUANTITY + REPORT_IN.QUANTITY - REPORT_OUT.QUANTITY) OVER (PARTITION BY REPORT_NOW.ID) NOW_QUANTITY,
    SUM(REPORT_NOW.TOTAL_PRICE + REPORT_IN.TOTAL_PRICE - REPORT_OUT.TOTAL_PRICE) OVER (PARTITION BY REPORT_NOW.ID) NOW_TOTAL_PRICE --d
  FROM 
  (
  SELECT 
    DISTINCT ESTOR_INVENTORIES.ID AS ID,
    ESTOR_INVENTORIES.DESCRIPTION AS DESCRIPTION,
  --  ESTOR_INVENTORIES.QUANTITY AS QUANTITY,
    COUNT ("ESTOR_ITEMS".ID) OVER (PARTITION BY ESTOR_INVENTORIES.ID) QUANTITY, 
    SUM ("ESTOR_ITEMS".UNIT_PRICE) OVER (PARTITION BY ESTOR_INVENTORIES.ID) TOTAL_PRICE 
  --  SUM("ESTOR_ITEMS".UNIT_PRICE) AS TOTAL_PRICE 
  FROM "ESTOR_ITEMS" 
  LEFT JOIN "ESTOR_INVENTORIES" ON "ESTOR_ITEMS"."INVENTORY_ID" = "ESTOR_INVENTORIES"."ID" 
  LEFT JOIN "CATEGORIES" ON "ESTOR_INVENTORIES"."CATEGORY_ID" = "CATEGORIES"."ID"
  WHERE ESTOR_ITEMS.INVENTORY_ID IN (27,41, 28)
  AND "ESTOR_ITEMS".CHECKOUT_DATE IS NULL
  AND ESTOR_ITEMS.DELETED = 0
  AND ESTOR_INVENTORIES.DELETED = 0
  AND ESTOR_INVENTORIES.APPROVED = 2
  ) REPORT_NOW
  LEFT JOIN
  (
      SELECT 
        DISTINCT ESTOR_INVENTORIES.ID AS ID,
        ESTOR_INVENTORIES.DESCRIPTION AS DESCRIPTION,
      --  ESTOR_INVENTORIES.QUANTITY AS QUANTITY,
        COUNT ("ESTOR_ITEMS".ID) OVER (PARTITION BY ESTOR_INVENTORIES.ID) QUANTITY, 
        SUM ("ESTOR_ITEMS".UNIT_PRICE) OVER (PARTITION BY ESTOR_INVENTORIES.ID) TOTAL_PRICE 
      --  SUM("ESTOR_ITEMS".UNIT_PRICE) AS TOTAL_PRICE 
      FROM "ESTOR_ITEMS" 
      LEFT JOIN "ESTOR_INVENTORIES" ON "ESTOR_ITEMS"."INVENTORY_ID" = "ESTOR_INVENTORIES"."ID" 
      LEFT JOIN "CATEGORIES" ON "ESTOR_INVENTORIES"."CATEGORY_ID" = "CATEGORIES"."ID"
      WHERE ESTOR_ITEMS.INVENTORY_ID IN (27,41, 28)
      AND "ESTOR_ITEMS".CHECKIN_DATE IS NULL
      AND ESTOR_ITEMS.DELETED = 0
      AND ESTOR_INVENTORIES.DELETED = 0
      AND ESTOR_INVENTORIES.APPROVED = 2
  ) REPORT_IN
   ON REPORT_IN.ID = REPORT_NOW.ID
  LEFT JOIN 
  (
      SELECT 
        DISTINCT ESTOR_INVENTORIES.ID AS ID,
        ESTOR_INVENTORIES.DESCRIPTION AS DESCRIPTION,
      --  ESTOR_INVENTORIES.QUANTITY AS QUANTITY,
        COUNT ("ESTOR_ITEMS".ID) OVER (PARTITION BY ESTOR_INVENTORIES.ID) QUANTITY, 
        SUM ("ESTOR_ITEMS".UNIT_PRICE) OVER (PARTITION BY ESTOR_INVENTORIES.ID) TOTAL_PRICE 
      --  SUM("ESTOR_ITEMS".UNIT_PRICE) AS TOTAL_PRICE 
      FROM "ESTOR_ITEMS" 
      LEFT JOIN "ESTOR_INVENTORIES" ON "ESTOR_ITEMS"."INVENTORY_ID" = "ESTOR_INVENTORIES"."ID" 
      LEFT JOIN "CATEGORIES" ON "ESTOR_INVENTORIES"."CATEGORY_ID" = "CATEGORIES"."ID"
      WHERE ESTOR_ITEMS.INVENTORY_ID IN (27,41, 28)
      AND "ESTOR_ITEMS".CHECKOUT_DATE IS NOT NULL
      AND ESTOR_ITEMS.DELETED = 0
      AND ESTOR_INVENTORIES.DELETED = 0
      AND ESTOR_INVENTORIES.APPROVED = 2
  ) REPORT_OUT 
  ON REPORT_OUT.ID = REPORT_NOW.ID
) REPORT
;

select to_char(created_date, 'Q') from estor_items where estor_items.deleted <> 1 and rownum = 1;






















==================================================================
				new
==================================================================

SELECT 
  '1' YEAR,
--  TO_CHAR("ESTOR_ITEMS".CHECKIN_DATE, 'Q') QUARTER,
--  COUNT ("ESTOR_ITEMS".ID) OVER (ORDER BY CHECKIN_DATE RANGE BETWEEN 50 PRECEDING AND
--                        150 FOLLOWING) QUANTITY,
--  SUM ("ESTOR_ITEMS".UNIT_PRICE) TOTAL_PRICE 
--  (select to_date('2005-03-18 10:33:02','yyyy-mm-dd hh24:mi:ss') d from dual),
  extract(year) y,1+trunc(extract(month from d)/4) q from yourdate
FROM "ESTOR_ITEMS" 
WHERE "ESTOR_ITEMS".CHECKIN_DATE IS NOT NULL
AND ESTOR_ITEMS.DELETED = 0;

SELECT * FROM ESTOR_ITEMS;

with yourdate as
(select to_date(CREATED_DATE,'dd-mm-yy hh24:mi:ss') d from ESTOR_ITEMS)
select extract(year from d) y,1+trunc(extract(month from d)/4) q from yourdate;


select CHECKIN_DATE, extract(year from CHECKIN_DATE) y,1+trunc(extract(month from CHECKIN_DATE)/4) q from ESTOR_ITEMS;

select 
  CHECKIN_DATE, 
  extract(year from CHECKIN_DATE) year,
  to_char(CHECKIN_DATE,'Q') quarter 
from ESTOR_ITEMS;

--2015 1 EACH QUATER
--2016 Q1:3,Q2:4,Q3:3,Q4:3 ALL:13

SELECT COUNT(*) FROM REPORT_IN_QUARTER WHERE YEAR = 2016 AND QUARTER = 4;
SELECT 
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM REPORT_IN_QUARTER
ORDER BY YEAR,QUARTER;

SELECT COUNT(*) FROM REPORT_IN_QUARTER WHERE QUARTER = 1 AND YEAR = 2016;

SELECT 
  COUNT(DISTINCT YEAR) OVER (PARTITION BY YEAR) BY_YEAR,
  COUNT(*) OVER (PARTITION BY QUARTER) BY_QUARTER
FROM REPORT_IN_QUARTER;

SELECT * FROM REPORT_IN_QUARTER;

WITH DATA1 AS (select 
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKIN_DATE, extract(year from CHECKIN_DATE) year,
  to_char(CHECKIN_DATE,'Q') quarter 
from ESTOR_ITEMS
WHERE CHECKIN_DATE IS NOT NULL)

SELECT 
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;



WITH DATA1 AS (select 
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKIN_DATE, extract(year from CHECKIN_DATE) year,
  to_char(CHECKIN_DATE,'Q') quarter 
from ESTOR_ITEMS
WHERE CHECKIN_DATE IS NOT NULL)
SELECT * FROM DATA1
pivot 
(
   count(YEAR)
   for YEAR in ('2015','2016')
);

select 
    REPORT_IN_QUARTER.YEAR,
    REPORT_IN_QUARTER.QUARTER,
    NVL(REPORT_CURRENT.COUNT, '0') count_CURRENT,
    NVL(REPORT_CURRENT.TOTAL_PRICE, '0') price_CURRENT,
    NVL(REPORT_IN_QUARTER.COUNT, '0') count_in,
    NVL(REPORT_IN_QUARTER.TOTAL_PRICE, '0') price_in,
    NVL(REPORT_out_QUARTER.COUNT, '0') count_out,
    NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0') price_out
from REPORT_IN_QUARTER 
left join REPORT_OUT_QUARTER 
on (REPORT_IN_QUARTER.YEAR = REPORT_OUT_QUARTER.YEAR and REPORT_IN_QUARTER.QUARTER = REPORT_OUT_QUARTER.QUARTER)
left join REPORT_CURRENT
on (REPORT_IN_QUARTER.YEAR = REPORT_CURRENT.YEAR and REPORT_IN_QUARTER.QUARTER = REPORT_CURRENT.QUARTER)
order by REPORT_IN_QUARTER.YEAR, REPORT_IN_QUARTER.QUARTER;




WITH DATA1 AS (select 
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKIN_DATE, extract(year from CHECKIN_DATE) year,
  to_char(CHECKIN_DATE,'Q') quarter 
from ESTOR_ITEMS
WHERE CHECKIN_DATE IS NOT NULL and CHECKOUT_DATE IS NULL )

SELECT 
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;



--quarter 1
WITH DATA1 AS (select 
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKIN_DATE, extract(year from CHECKIN_DATE) year,
  to_char(CHECKIN_DATE,'Q') quarter 
from ESTOR_ITEMS
WHERE CHECKIN_DATE IS NOT NULL and CHECKOUT_DATE IS NULL 
  or to_char(CHECKOUT_DATE,'Q') > 1 
)

SELECT 
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;





select sum(count_current) from REPORT_ALL;
select sum(count_in) from REPORT_ALL;
select sum(count_out) from REPORT_ALL;
select sum(count) from REPORT_IN_QUARTER;
select sum(count) from REPORT_OUT_QUARTER;
select count(*) from ESTOR_ITEMS; 


