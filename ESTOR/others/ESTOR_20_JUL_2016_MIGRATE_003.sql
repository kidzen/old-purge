--------------------------------------------------------
--  File created - Wednesday-July-20-2016   
--------------------------------------------------------
DROP TABLE "ACTIVITY_LOGS" cascade constraints;
DROP TABLE "AIRCOND_LIST" cascade constraints;
DROP TABLE "AUTH_ITEM" cascade constraints;
DROP TABLE "AUTH_RULE" cascade constraints;
DROP TABLE "ESTOR_CACHE" cascade constraints;
DROP TABLE "ESTOR_INVENTORIES" cascade constraints;
DROP TABLE "ESTOR_ITEMS" cascade constraints;
DROP TABLE "ESTOR_SESSION" cascade constraints;
DROP TABLE "PACKAGE" cascade constraints;
DROP TABLE "STOCKS" cascade constraints;
DROP TABLE "STOCK_ITEMS" cascade constraints;
DROP TABLE "USAGE_LIST" cascade constraints;
DROP TABLE "VEHICLE_LIST" cascade constraints;
DROP TABLE "VENDORS" cascade constraints;
DROP SEQUENCE "AUTH_ITEM_SEQ";
DROP SEQUENCE "AUTH_RULE_SEQ";
DROP SEQUENCE "ACTIVITY_LOGS_SEQ";
DROP SEQUENCE "AIRCOND_LIST_SEQ";
DROP SEQUENCE "ESTOR_CACHE_SEQ";
DROP SEQUENCE "ESTOR_INVENTORIES_SEQ";
DROP SEQUENCE "ESTOR_INVENTORIES_SEQ1";
DROP SEQUENCE "ESTOR_INVENTORIES_SEQ2";
DROP SEQUENCE "ESTOR_ITEMS_SEQ";
DROP SEQUENCE "ESTOR_ITEMS_SEQ1";
DROP SEQUENCE "ESTOR_ITEMS_SEQ2";
DROP SEQUENCE "ESTOR_SESSION_SEQ";
DROP SEQUENCE "PACKAGE_SEQ";
DROP SEQUENCE "STOCKS_SEQ";
DROP SEQUENCE "STOCK_ITEMS_SEQ";
DROP SEQUENCE "USAGE_LIST_SEQ";
DROP SEQUENCE "USAGE_LIST_SEQ1";
DROP SEQUENCE "VEHICLE_LIST_SEQ";
DROP SEQUENCE "VENDORS_SEQ";
DROP VIEW "LIST_YEAR";
DROP VIEW "REPORT_ALL";
DROP VIEW "REPORT_ALL2";
DROP VIEW "REPORT_CURRENT";
DROP VIEW "REPORT_IN_QUARTER";
DROP VIEW "REPORT_OUT_QUARTER";
DROP VIEW "REPORT_PREVIOUS_Q1";

--------------------------------------------------------
--  DDL for Sequence ACTIVITY_LOGS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."ACTIVITY_LOGS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence AIRCOND_LIST_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."AIRCOND_LIST_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence AUTH_ITEM_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."AUTH_ITEM_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence AUTH_RULE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."AUTH_RULE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence ESTOR_CACHE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."ESTOR_CACHE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence ESTOR_INVENTORIES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."ESTOR_INVENTORIES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence ESTOR_ITEMS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."ESTOR_ITEMS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence ESTOR_SESSION_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."ESTOR_SESSION_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PACKAGE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."PACKAGE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence STOCKS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."STOCKS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence STOCK_ITEMS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."STOCK_ITEMS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence USAGE_LIST_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."USAGE_LIST_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence VEHICLE_LIST_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."VEHICLE_LIST_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 386 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence VENDORS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."VENDORS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table ACTIVITY_LOGS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."ACTIVITY_LOGS" 
   (	"ID" NUMBER(11,0), 
	"PERSON_ID" NUMBER(11,0), 
	"ACTION" VARCHAR2(255), 
	"MODEL" VARCHAR2(255), 
	"MODEL_ID" NUMBER(11,0), 
	"TITLE" VARCHAR2(255), 
	"DESCRIPTION" VARCHAR2(255), 
	"CREATED" TIMESTAMP (0), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0)
   ) ;
--------------------------------------------------------
--  DDL for Table AIRCOND_LIST
--------------------------------------------------------

  CREATE TABLE "ESTOR"."AIRCOND_LIST" 
   (	"ID" NUMBER(11,0), 
	"LOCATION" VARCHAR2(50), 
	"FLOOR" VARCHAR2(5), 
	"ADDRESS" VARCHAR2(20), 
	"TYPE" VARCHAR2(20), 
	"BRAND" VARCHAR2(20), 
	"HP" FLOAT(126), 
	"SIRI_NO" VARCHAR2(20), 
	"KEW_PA" VARCHAR2(20), 
	"MODEL" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table AUTH_ITEM
--------------------------------------------------------

  CREATE TABLE "ESTOR"."AUTH_ITEM" 
   (	"NAME" VARCHAR2(64), 
	"TYPE" NUMBER, 
	"DESCRIPTION" VARCHAR2(64), 
	"RULE_NAME" VARCHAR2(64), 
	"DATA" VARCHAR2(64), 
	"CREATED_AT" NUMBER, 
	"UPDATED_AT" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table AUTH_RULE
--------------------------------------------------------

  CREATE TABLE "ESTOR"."AUTH_RULE" 
   (	"NAME" VARCHAR2(64), 
	"DATA" VARCHAR2(64), 
	"CREATED_AT" NUMBER, 
	"UPDATED_AT" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table ESTOR_CACHE
--------------------------------------------------------

  CREATE TABLE "ESTOR"."ESTOR_CACHE" 
   (	"id" CHAR(128), 
	"expire" NUMBER(*,0), 
	"data" BLOB
   ) ;
--------------------------------------------------------
--  DDL for Table ESTOR_INVENTORIES
--------------------------------------------------------

  CREATE TABLE "ESTOR"."ESTOR_INVENTORIES" 
   (	"ID" NUMBER(11,0), 
	"CARD_NO" VARCHAR2(255), 
	"CODE_NO" VARCHAR2(255), 
	"DESCRIPTION" VARCHAR2(255), 
	"QUANTITY" NUMBER DEFAULT 0, 
	"CREATED_DATE" TIMESTAMP (0), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_DATE" TIMESTAMP (0), 
	"UPDATED_BY" VARCHAR2(255), 
	"PURCHASED_DATE" DATE, 
	"APPROVED_BY" VARCHAR2(255), 
	"APPROVED_DATE" TIMESTAMP (0), 
	"APPROVED" NUMBER(1,0) DEFAULT 1, 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"CATEGORY_ID" NUMBER(11,0), 
	"MIN_STOCK" NUMBER(11,0) DEFAULT 0, 
	"LOCATION" VARCHAR2(255), 
	 PRIMARY KEY ("ID") ENABLE
   ) ORGANIZATION INDEX NOCOMPRESS ;
--------------------------------------------------------
--  DDL for Table ESTOR_ITEMS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."ESTOR_ITEMS" 
   (	"ID" NUMBER(11,0), 
	"SKU" VARCHAR2(50), 
	"UNIT_PRICE" NUMBER, 
	"CHECKIN_DATE" DATE, 
	"CHECKOUT_DATE" DATE, 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" NUMBER(11,0), 
	"UPDATED_BY" NUMBER(11,0), 
	"INVENTORY_ID" NUMBER(11,0), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (6), 
	"CHECKOUT_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table ESTOR_SESSION
--------------------------------------------------------

  CREATE TABLE "ESTOR"."ESTOR_SESSION" 
   (	"id" CHAR(40), 
	"expire" NUMBER(*,0), 
	"data" BLOB
   ) ;
--------------------------------------------------------
--  DDL for Table PACKAGE
--------------------------------------------------------

  CREATE TABLE "ESTOR"."PACKAGE" 
   (	"ID" NUMBER(11,0), 
	"DETAIL" VARCHAR2(255), 
	"DELIVERY" VARCHAR2(255), 
	"PACKAGE_BY" VARCHAR2(255), 
	"PACKAGE_DATE" DATE, 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (6), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" NUMBER, 
	"UPDATED_BY" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table STOCKS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."STOCKS" 
   (	"ID" NUMBER(11,0), 
	"ORDER_DATE" DATE, 
	"ORDERED_BY" VARCHAR2(255), 
	"TYPE" NUMBER, 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"APPROVED_BY" VARCHAR2(255), 
	"APPROVED_DATE" TIMESTAMP (0), 
	"APPROVED" NUMBER(1,0) DEFAULT 1, 
	"USAGE" VARCHAR2(20 CHAR), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_BY" VARCHAR2(255), 
	"REQUIRED_DATE" DATE, 
	"CHECKOUT_DATE" DATE, 
	"CHECKOUT_BY" VARCHAR2(255), 
	"ORDER_NO" VARCHAR2(20), 
	"PACKAGE_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table STOCK_ITEMS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."STOCK_ITEMS" 
   (	"ID" NUMBER(11,0), 
	"INVENTORY_ID" NUMBER(11,0), 
	"RQ_QUANTITY" NUMBER, 
	"APP_QUANTITY" NUMBER, 
	"CURRENT_BALANCE" NUMBER, 
	"UNIT_PRICE" NUMBER(12,2), 
	"STOCK_ID" NUMBER(11,0), 
	"SKU" VARCHAR2(255), 
	"DESCRIPTION" VARCHAR2(255), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_BY" VARCHAR2(255), 
	"TOTAL_PRICE" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table VEHICLE_LIST
--------------------------------------------------------

  CREATE TABLE "ESTOR"."VEHICLE_LIST" 
   (	"ID" NUMBER(11,0), 
	"REG_NO" VARCHAR2(20),  
	"MODEL" VARCHAR2(100), 
	"TYPE" VARCHAR2(20),  
	"DELETED" NUMBER(1,0), 
	"DELETED_DATE" TIMESTAMP (6), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(20), 
	"UPDATED_BY" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table USAGE_LIST
--------------------------------------------------------

  CREATE TABLE "ESTOR"."USAGE_LIST" 
   (	"ID" NUMBER(11,0), 
	"NAME" VARCHAR2(20),  
	"DELETED" NUMBER(1,0), 
	"DELETED_DATE" TIMESTAMP (6), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(20), 
	"UPDATED_BY" VARCHAR2(20)
   ) ;
--------------------------------------------------------
--  DDL for Table VENDORS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."VENDORS" 
   (	"ID" NUMBER(11,0), 
	"NAME" VARCHAR2(255), 
	"DELETED" NUMBER(1,0) DEFAULT 0, 
	"DELETED_DATE" TIMESTAMP (0), 
	"ADDRESS" VARCHAR2(255), 
	"CONTACT_NO" VARCHAR2(50), 
	"CREATED_DATE" TIMESTAMP (6), 
	"UPDATED_DATE" TIMESTAMP (6), 
	"CREATED_BY" VARCHAR2(255), 
	"UPDATED_BY" VARCHAR2(255)
   ) ;
--------------------------------------------------------
--  DDL for Index SYS_C0010455
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."SYS_C0010455" ON "ESTOR"."STOCKS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index ACTIVITY_LOGS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."ACTIVITY_LOGS_PK" ON "ESTOR"."ACTIVITY_LOGS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0010453
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."SYS_C0010453" ON "ESTOR"."VENDORS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index USAGE_LIST_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."USAGE_LIST_PK" ON "ESTOR"."USAGE_LIST" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index VEHICLE_LIST_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."VEHICLE_LIST_PK" ON "ESTOR"."VEHICLE_LIST" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index AUTH_ITEM_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."AUTH_ITEM_PK" ON "ESTOR"."AUTH_ITEM" ("NAME") 
  ;
--------------------------------------------------------
--  DDL for Index ESTOR_SESSION_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."ESTOR_SESSION_PK" ON "ESTOR"."ESTOR_SESSION" ("id") 
  ;
--------------------------------------------------------
--  DDL for Index ESTOR_ITEMS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."ESTOR_ITEMS_PK" ON "ESTOR"."ESTOR_ITEMS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0012441
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."SYS_C0012441" ON "ESTOR"."STOCK_ITEMS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index AIRCOND_LIST_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."AIRCOND_LIST_PK" ON "ESTOR"."AIRCOND_LIST" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index PACKAGE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."PACKAGE_PK" ON "ESTOR"."PACKAGE" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index AUTH_RULE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."AUTH_RULE_PK" ON "ESTOR"."AUTH_RULE" ("NAME") 
  ;
--------------------------------------------------------
--  DDL for Index ESTOR_CACHE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."ESTOR_CACHE_PK" ON "ESTOR"."ESTOR_CACHE" ("id") 
  ;
--------------------------------------------------------
--  Constraints for Table ESTOR_ITEMS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."ESTOR_ITEMS" ADD CONSTRAINT "ESTOR_ITEMS_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "ESTOR"."ESTOR_ITEMS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ACTIVITY_LOGS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."ACTIVITY_LOGS" ADD CONSTRAINT "ACTIVITY_LOGS_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "ESTOR"."ACTIVITY_LOGS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PACKAGE
--------------------------------------------------------

  ALTER TABLE "ESTOR"."PACKAGE" ADD CONSTRAINT "PACKAGE_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "ESTOR"."PACKAGE" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ESTOR_CACHE
--------------------------------------------------------

  ALTER TABLE "ESTOR"."ESTOR_CACHE" ADD CONSTRAINT "ESTOR_CACHE_PK" PRIMARY KEY ("id") ENABLE;
 
  ALTER TABLE "ESTOR"."ESTOR_CACHE" MODIFY ("id" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table VENDORS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."VENDORS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "ESTOR"."VENDORS" ADD CONSTRAINT "VENDORS_PK" PRIMARY KEY ("ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table STOCK_ITEMS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CONSTRAINT "STOCK_ITEMS_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ESTOR_INVENTORIES
--------------------------------------------------------

  ALTER TABLE "ESTOR"."ESTOR_INVENTORIES" ADD PRIMARY KEY ("ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table AIRCOND_LIST
--------------------------------------------------------

  ALTER TABLE "ESTOR"."AIRCOND_LIST" ADD CONSTRAINT "AIRCOND_LIST_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "ESTOR"."AIRCOND_LIST" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table STOCKS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."STOCKS" ADD CONSTRAINT "STOCKS_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "ESTOR"."STOCKS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ESTOR_SESSION
--------------------------------------------------------

  ALTER TABLE "ESTOR"."ESTOR_SESSION" ADD CONSTRAINT "ESTOR_SESSION_PK" PRIMARY KEY ("id") ENABLE;
 
  ALTER TABLE "ESTOR"."ESTOR_SESSION" MODIFY ("id" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USAGE_LIST
--------------------------------------------------------

  ALTER TABLE "ESTOR"."USAGE_LIST" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "ESTOR"."USAGE_LIST" ADD CONSTRAINT "USAGE_LIST_PK" PRIMARY KEY ("ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table VEHICLE_LIST
--------------------------------------------------------

  ALTER TABLE "ESTOR"."VEHICLE_LIST" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "ESTOR"."VEHICLE_LIST" ADD CONSTRAINT "VEHICLE_LIST_PK" PRIMARY KEY ("ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table CATEGORIES
--------------------------------------------------------

  ALTER TABLE "ESTOR"."CATEGORIES" ADD CONSTRAINT "CATEGORIES_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "ESTOR"."CATEGORIES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table ESTOR_INVENTORIES
--------------------------------------------------------

  ALTER TABLE "ESTOR"."ESTOR_INVENTORIES" ADD CONSTRAINT "ESTOR_INVENTORIES_FK1" FOREIGN KEY ("CATEGORY_ID")
	  REFERENCES "ESTOR"."CATEGORIES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ESTOR_ITEMS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."ESTOR_ITEMS" ADD CONSTRAINT "ESTOR_ITEMS_FK1" FOREIGN KEY ("INVENTORY_ID")
	  REFERENCES "ESTOR"."ESTOR_INVENTORIES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table STOCK_ITEMS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CONSTRAINT "STOCK_ITEMS_FK1" FOREIGN KEY ("INVENTORY_ID")
	  REFERENCES "ESTOR"."ESTOR_INVENTORIES" ("ID") ENABLE;
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CONSTRAINT "STOCK_ITEMS_FK2" FOREIGN KEY ("STOCK_ID")
	  REFERENCES "ESTOR"."STOCKS" ("ID") ENABLE;
--------------------------------------------------------
--  DDL for Trigger ACTIVITY_LOGS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."ACTIVITY_LOGS_TRG" 
BEFORE INSERT ON ACTIVITY_LOGS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT ACTIVITY_LOGS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."ACTIVITY_LOGS_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AIRCOND_LIST_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."AIRCOND_LIST_TRG" 
BEFORE INSERT ON AIRCOND_LIST 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT AIRCOND_LIST_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."AIRCOND_LIST_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUTH_ITEM_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."AUTH_ITEM_TRG" 
BEFORE INSERT ON AUTH_ITEM 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.NAME IS NULL THEN
      SELECT AUTH_ITEM_SEQ.NEXTVAL INTO :NEW.NAME FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."AUTH_ITEM_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUTH_RULE_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."AUTH_RULE_TRG" 
BEFORE INSERT ON AUTH_RULE 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.NAME IS NULL THEN
      SELECT AUTH_RULE_SEQ.NEXTVAL INTO :NEW.NAME FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."AUTH_RULE_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ESTOR_CACHE_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."ESTOR_CACHE_TRG" 
BEFORE INSERT ON ESTOR_CACHE 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW."id" IS NULL THEN
      SELECT ESTOR_CACHE_SEQ.NEXTVAL INTO :NEW."id" FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."ESTOR_CACHE_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ESTOR_INVENTORIES_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."ESTOR_INVENTORIES_TRG" 
BEFORE INSERT ON ESTOR_INVENTORIES 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT ESTOR_INVENTORIES_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."ESTOR_INVENTORIES_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ESTOR_ITEMS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."ESTOR_ITEMS_TRG" 
BEFORE INSERT ON ESTOR_ITEMS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT ESTOR_ITEMS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."ESTOR_ITEMS_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ESTOR_SESSION_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."ESTOR_SESSION_TRG" 
BEFORE INSERT ON ESTOR_SESSION 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW."id" IS NULL THEN
      SELECT ESTOR_SESSION_SEQ.NEXTVAL INTO :NEW."id" FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."ESTOR_SESSION_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PACKAGE_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."PACKAGE_TRG" 
BEFORE INSERT ON PACKAGE 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT PACKAGE_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."PACKAGE_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger STOCKS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."STOCKS_TRG" 
BEFORE INSERT ON STOCKS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT STOCKS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."STOCKS_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger STOCK_ITEMS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."STOCK_ITEMS_TRG" 
BEFORE INSERT ON STOCK_ITEMS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT STOCK_ITEMS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."STOCK_ITEMS_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger USAGE_LIST_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."USAGE_LIST_TRG" 
BEFORE INSERT ON USAGE_LIST 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT USAGE_LIST_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."USAGE_LIST_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger VEHICLE_LIST_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."VEHICLE_LIST_TRG" 
BEFORE INSERT ON VEHICLE_LIST 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT VEHICLE_LIST_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."VEHICLE_LIST_TRG" ENABLE;
--------------------------------------------------------
--  DDL for Trigger VENDORS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."VENDORS_TRG" 
BEFORE INSERT ON VENDORS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT VENDORS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "ESTOR"."VENDORS_TRG" ENABLE;

--------------------------------------------------------
--  DDL for View REPORT_IN_QUARTER
--------------------------------------------------------

  CREATE OR REPLACE VIEW "ESTOR"."REPORT_IN_QUARTER" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (select 
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKIN_DATE, extract(year from CHECKIN_DATE) year,
  to_char(CHECKIN_DATE,'Q') quarter 
from ESTOR_ITEMS
WHERE CHECKIN_DATE IS NOT NULL and deleted = 0)

SELECT 
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;
--------------------------------------------------------
--  DDL for View REPORT_OUT_QUARTER
--------------------------------------------------------

  CREATE OR REPLACE VIEW "ESTOR"."REPORT_OUT_QUARTER" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (select 
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKOUT_DATE, extract(year from CHECKOUT_DATE) year,
  to_char(CHECKOUT_DATE,'Q') quarter 
from ESTOR_ITEMS
WHERE CHECKOUT_DATE IS NOT NULL and deleted = 0)

SELECT 
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;
--------------------------------------------------------
--  DDL for View LIST_YEAR
--------------------------------------------------------

  CREATE OR REPLACE VIEW "ESTOR"."LIST_YEAR" ("YEAR", "QUARTER") AS 
  select year,quarter from REPORT_IN_QUARTER union
select year,quarter from REPORT_OUT_QUARTER;
--------------------------------------------------------
--  DDL for View REPORT_CURRENT
--------------------------------------------------------

  CREATE OR REPLACE VIEW "ESTOR"."REPORT_CURRENT" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (select 
  ESTOR_ITEMS.SKU,
  ESTOR_ITEMS.UNIT_PRICE,
  ESTOR_ITEMS.INVENTORY_ID,
  CHECKIN_DATE, extract(year from CHECKIN_DATE) year,
  to_char(CHECKIN_DATE,'Q') quarter 
from ESTOR_ITEMS
WHERE CHECKIN_DATE IS NOT NULL and CHECKOUT_DATE IS NULL or CHECKOUT_DATE IS NULL or to_char(CHECKOUT_DATE,'q') < 4)

SELECT 
  DISTINCT YEAR,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;
--------------------------------------------------------
--  DDL for View REPORT_PREVIOUS_Q1
--------------------------------------------------------

  CREATE OR REPLACE VIEW "ESTOR"."REPORT_PREVIOUS_Q1" ("QUARTER", "QUANTITY", "TOTAL_PRICE") AS 
  SELECT 
  '1' QUARTER,
  COUNT ("ESTOR_ITEMS".ID) QUANTITY, 
  SUM ("ESTOR_ITEMS".UNIT_PRICE) TOTAL_PRICE 
FROM "ESTOR_ITEMS" 
WHERE "ESTOR_ITEMS".CHECKOUT_DATE IS NULL
AND ESTOR_ITEMS.DELETED = 0;
--------------------------------------------------------
--  DDL for View REPORT_ALL
--------------------------------------------------------

  CREATE OR REPLACE VIEW "ESTOR"."REPORT_ALL" ("YEAR", "QUARTER", "COUNT_IN", "PRICE_IN", "COUNT_OUT", "PRICE_OUT", "COUNT_CURRENT", "PRICE_CURRENT") AS 
  select
    list_year.YEAR,
    list_year.QUARTER,
    NVL(REPORT_IN_QUARTER.COUNT, '0') count_in,
    NVL(REPORT_IN_QUARTER.TOTAL_PRICE, '0') price_in,
    NVL(REPORT_out_QUARTER.COUNT, '0') count_out,
    NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0') price_out,

      sum(NVL(REPORT_IN_QUARTER.COUNT, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(REPORT_OUT_QUARTER.COUNT, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING)
    count_current,
      sum(NVL(REPORT_in_QUARTER.TOTAL_PRICE, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING)
    price_current

from list_year
left join REPORT_IN_QUARTER 
on (REPORT_IN_QUARTER.YEAR = list_year.YEAR and list_year.QUARTER = REPORT_IN_QUARTER.QUARTER)
left join REPORT_OUT_QUARTER 
on (list_year.YEAR = REPORT_OUT_QUARTER.YEAR and list_year.QUARTER = REPORT_OUT_QUARTER.QUARTER)
order by list_year.YEAR, list_year.QUARTER;
--------------------------------------------------------
--  DDL for View REPORT_ALL2
--------------------------------------------------------

  CREATE OR REPLACE VIEW "ESTOR"."REPORT_ALL2" ("YEAR", "QUARTER", "COUNT_CURRENT", "PRICE_CURRENT", "COUNT_IN", "PRICE_IN", "COUNT_OUT", "PRICE_OUT") AS 
  select
    REPORT_IN_QUARTER.YEAR,
    REPORT_IN_QUARTER.QUARTER,
    NVL(REPORT_CURRENT.COUNT, '0') count_CURRENT,
    NVL(REPORT_CURRENT.TOTAL_PRICE, '0') price_CURRENT,
    NVL(REPORT_IN_QUARTER.COUNT, '0') count_in,
    NVL(REPORT_IN_QUARTER.TOTAL_PRICE, '0') price_in,
    NVL(REPORT_out_QUARTER.COUNT, '0') count_out,
    NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0') price_out
from REPORT_IN_QUARTER 
left join REPORT_OUT_QUARTER 
on (REPORT_IN_QUARTER.YEAR = REPORT_OUT_QUARTER.YEAR and REPORT_IN_QUARTER.QUARTER = REPORT_OUT_QUARTER.QUARTER)
left join REPORT_CURRENT
on (REPORT_IN_QUARTER.YEAR = REPORT_CURRENT.YEAR and REPORT_IN_QUARTER.QUARTER = REPORT_CURRENT.QUARTER)
order by REPORT_IN_QUARTER.YEAR, REPORT_IN_QUARTER.QUARTER;




REM INSERTING into ESTOR.VEHICLE_LIST
SET DEFINE OFF;
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (1,'PAC 365','TOYOTA LANDCRUISER HJ45','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (2,'PBH 2800','MERCEDES BENZ 200','CAR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (3,'PBL 3829','ISUZU NHR542','TAIL LIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (4,'PCC 7634','MITSUBISHI L049','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (5,'PCD 6535','ISUZU TFR54','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (6,'PCF 642','ISUZU FSR11H','OPEN TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (7,'PCM 5005','MITSUBISHI L049','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (8,'PCR 4365','TOYOTA HILUX LN106','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (9,'PCR 8371','NISSAN CMF88H','SKYLIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (10,'PCX 885','CASE 580 SUPER K(2WD)','BACKHOE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (11,'PCX 1769','PROTON WIRA 1.6XLI A','CAR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (12,'PCX 3203','ISUZU FSR11H','CARGO AM',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (13,'PCX 3204','ISUZU FSR11H','CARGO AM',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (14,'PCX 3205','NISSAN CPB15N','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (15,'PCX 5350','ISUZU FTR12H','WATER TANK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (16,'PCY 3927','ISUZU NPR59P','MINI TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (17,'PDA 5657','TATA S1313','TOWING',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (18,'PDC 9380','MITSUBISHI HDF25','FORK LIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (19,'PDD 7421','HONDA C100','MOTOCYCLE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (20,'PDD 7422','HONDA C100','MOTOCYCLE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (21,'PDD 7687','HONDA C100','MOTOCYCLE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (22,'PDE 2909','NISSAN CMF88H','OPEN TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (23,'PDE 7301','ISUZU NPR59P','MINI TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (24,'PDF 1453','HONDA C100','MOTOCYCLE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (25,'PDF 1454','HONDA C100','MOTOCYCLE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (26,'PDK 2606','TOYOTA LANDCRUISER HZJ80','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (27,'PDM 4837','TOYOTA HIACE LH112','VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (28,'PDM 5056','ISUZU FSR11H','WATER TANK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (29,'PDN 7817','ISUZU FSR11H','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (30,'PDN 7825','ISUZU FSR11H','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (31,'PDN 8035','ISUZU FSR11H','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (32,'PDN 8051','ISUZU FSR11H','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (33,'PDR 5728','TOYOTA LANDCRUISER HZJ80','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (34,'PDS 2638','TOYOTA 5FG30','FORK LIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (35,'PDT 3413','MITSUBISHI PAJERO L049','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (36,'PDT 7404','NISSAN CMF88H','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (37,'PDU 8376','ISUZU TFR54','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (38,'PDU 8391','ISUZU TFR54','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (39,'PDV 2647','TOYOTA HIACE LH112','VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (40,'PDW 4729','MITSUBISHI PAJERO L049','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (41,'PDX 649','ISUZU FTR12H','MULTI PURPOSE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (42,'PDX 4190','TOYOTA LANDCRUISER HZJ80','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (43,'PDX 4512','ISUZU NPR59P','TAIL LIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (44,'PDY 849','ISUZU NPR59P','MINI TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (45,'PEA 3819','CASE 580SL 2WD','BACKHOE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (46,'PEC 9408','ISUZU TROOPER UBS69G','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (47,'PEC 9410','ISUZU TROOPER UBS69G','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (48,'PED 5434','NISSAN NU41H5','MINI TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (49,'PEJ 6244','MITSUBISHI PAJERO V34V','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (50,'PEK 3020','ISUZU RODEO TFS55H','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (51,'PEK 5913','ISUZU FTR33H','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (52,'PEK 7024','NISSAN NU41H5','MINI TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (53,'PET 4883','TOYOTA LANDCRUISER RZJ95','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (54,'PET 9690','HICOM PERKASA MTB170','MINI TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (55,'PEU 744','FORD RANGER 205TD','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (56,'PEU 746','FORD RANGER 205TD','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (57,'PEV 1746','SCANIA K113 CRB','BUS',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (58,'PEV 8558','PROTON WAJA 1.6 SOHC (A)','CAR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (59,'PEV 8577','MITSUBISHI PAJERO V34V','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (60,'PFB 1447','PROTON WIRA 1.5GLi (A)','CAR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (61,'PFB 2059','CATERPILLAR DP15','FORK LIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (62,'PFB 5499','MITSUBISHI PAJERO V34V','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (63,'PFB 6390','TOYOTA HIACE LH112','VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (64,'PFB 6401','TOYOTA HIACE LH112','VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (65,'PFB 8677','ISUZU FSR33L','OPEN TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (66,'PFB 8684','ISUZU FSR33L','OPEN TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (67,'PFB 8690','ISUZU FSR33L','OPEN TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (68,'PFB 8697','ISUZU FSR33L','OPEN TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (69,'PFB 8704','ISUZU FSR33L','OPEN TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (70,'PFB 9542','ISUZU FTR33P','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (71,'PFB 9572','ISUZU FTR33P','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (72,'PFB 9581','ISUZU FTR33P','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (73,'PFB 9586','ISUZU FTR33H','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (74,'PFC 2104','JCB 3CXT','BACKHOE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (75,'PFC 2106','JCB 3CXT','BACKHOE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (76,'PFC 2107','JCB 3CXT','BACKHOE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (77,'PFC 6777','MITSUBISHI STORM K74','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (78,'PFD 5027','NISSAN LKA211N','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (79,'PFD 5028','NISSAN LKA211N','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (80,'PFD 5029','NISSAN LKA211N','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (82,'PFG 8685','MITSUBISHI PAJERO V34V','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (83,'PFJ 6807','HICOM PERKASA MTB170','SKYLIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (84,'PFJ 8642','HICOM PERKASA MTB170','SKYLIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (85,'WJT 7155','ISUZU FSR33L','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (86,'WKJ 7540','ISUZU FSR33L','OPEN TRUCK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (87,'PFL 8601','HICOM PERKASA MTB150DX','WATER TANK',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (88,'PFQ 7097','ISUZU FTS33H','MULTI PURPOSE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (89,'PFR 6442','MITSUBISHI PAJERO V34V','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (90,'PFR 6605','MITSUBISHI PAJERO V34V','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (91,'PFT 2002','PROTON PERDANA V6','CAR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (92,'PFX 6570','FORD RANGER UV','PICK-UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (93,'PFX 6751','PERODUA KEMBARA 1.3 GX M','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (94,'PFY 8249','MITSUBISHI PAJERO V34V','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (95,'PGB 9964','MITSUBISHI STORM K74','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (96,'PGD 2775','PERODUA KEMBARA 1.3EX M','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (97,'PGD 4782','PERODUA KEMBARA 1.3EX M','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (98,'PGD 6777','HONDA SL230','MOTOCYCLE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (99,'PGE 1001','PROTON PERDANA V6','CAR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (100,'PGE 3753','MITSUBISHI STORM K74','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (101,'WLQ 2756','TATA TELCOLINE 207','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (102,'WLT 623','HINO GD1 JLPA','CARGO',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (103,'PGJ 2885','MITSUBISHI STORM K74','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (104,'PGM 2353','TOYOTA HIACE LH174','VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (105,'PGM 2358','TOYOTA HIACE LH174','VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (106,'PGP 1361','PERODUA KEMBARA 1.3 GX M','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (107,'PGP 2082','PERODUA KEMBARA 1.3 GX M','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (108,'PGR 2081','TOYOTA HIACE LH174','VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (109,'PGR 2085','TOYOTA HIACE LH174','VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (110,'PGR 2089','TOYOTA HIACE LH174','VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (111,'PGR 2091','TOYOTA HIACE LH174','VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (112,'PGR 2093','TOYOTA HIACE LH174','VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (113,'PGR 2096','TOYOTA HIACE LH174','VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (114,'PGR 7530','ISUZU FTR33K','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (115,'PGR 7544','ISUZU FTR33K','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (116,'PGS 7523','HINO GH1JKPD','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (117,'PGS 7537','HINO GH1JKPD','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (118,'PGS 7540','HINO GH1JKPD','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (119,'PGS 7542','HINO GH1JKPD','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (120,'PGT 5505','HINO GDI JLKPA','TAIL LIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (121,'PGT 5518','HINO GDI JLKPA','TAIL LIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (122,'PHB 2547','HINO GD1JLPA','TAIL LIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (123,'PJR 5777','ISUZU FSR33P','SKY LIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (124,'PJT 9246','ISUZU FTR32R','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (125,'PJT 9251','ISUZU FTR32R','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (126,'PJU 1630','ISUZU FTR32R','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (127,'PJU 3981','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (128,'PJU 3987','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (129,'PJU 3991','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (130,'PJU 3994','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (131,'PJU 4002','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (132,'PJV 248','ISUZU FTR32R','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (133,'PJV 263','ISUZU FTR32R','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (134,'PJW 885','ISUZU HICOM MTB150','TOWING',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (135,'PJW 5676','ISUZU HICOM MTB140','CREW KARGO AM',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (136,'PJW 5696','ISUZU HICOM MTB140','CREW KARGO AM',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (137,'PJW 5730','ISUZU HICOM MTB140','CREW KARGO AM',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (138,'PJW 5740','ISUZU HICOM MTB140','CREW KARGO AM',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (139,'PJW 5752','ISUZU HICOM MTB140','CREW KARGO AM',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (140,'PJW 5764','ISUZU HICOM MTB140','CREW KARGO AM',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (141,'PJW 7258','ISUZU HICOM MTB150','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (142,'PJW 7270','ISUZU HICOM MTB150','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (143,'PJW 7273','ISUZU HICOM MTB150','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (144,'PJW 7284','ISUZU HICOM MTB150','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (145,'PJW 8377','HINO XZU423','SKY LIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (146,'PJW 8662','TOYOTA FORTUNER 2.5G','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (147,'PJW 8670','TOYOTA FORTUNER 2.5G','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (148,'PJW 8672','TOYOTA HILUX 2.5MT','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (149,'PJX 9655','NISSAN CWM272','HIGH PRESSURE JET',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (150,'PKE 1001','NISSAN TEANA 250XV V6 (A)','CAR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (151,'PKE 5342','HINO FG1JPP-21033','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (152,'PKE 5347','HINO FG1JPP-21033','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (153,'PKE 5350','HINO FG1JPP-21033','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (154,'PKE 5358','HINO FG1JPP-21033','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (155,'PKE 5361','HINO FG1JPP-21033','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (156,'PKE 5365','HINO FG1JPP-21033','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (157,'PKF 599','NISSAN LKC214','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (158,'PKF 617','NISSAN LKC214','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (159,'PKF 639','NISSAN LKC214','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (160,'PKF 644','NISSAN LKC214','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (161,'PKF 649','NISSAN LKC214','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (162,'PKF 658','NISSAN LKC214','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (163,'PKF 659','NISSAN LKC214','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (164,'PKF 693','NISSAN LKC214','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (165,'PKF 699','NISSAN LKC214','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (166,'PKF 704','NISSAN LKC214','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (167,'PKF 7762','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (168,'PKF 7769','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (169,'PKF 7781','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (170,'PKF 7784','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (171,'PKF 7792','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (172,'PKF 7795','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (173,'PKF 7801','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (174,'PKJ 586','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (175,'PKJ 614','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (176,'PKJ 615','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (177,'PKJ 617','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (178,'PKJ 619','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (179,'PKJ 620','TOYOTA HILUX 2.5MT','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (180,'PKJ 623','TOYOTA HILUX 2.5MT','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (181,'PKJ 629','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (182,'PKJ 8943','HINO WU410','CREW KARGO AM',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (183,'PKJ 8970','HINO WU410','CREW KARGO AM',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (184,'PKJ 9014','HINO WU410','CREW KARGO AM',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (185,'PKK 499','KAT WZ30-25','BACKHOE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (186,'PKK 548','KAT WZ30-25','BACKHOE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (187,'PKK 553','KAT WZ30-25','BACKHOE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (188,'PKK 559','KAT WZ30-25','BACKHOE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (189,'PKL 3546','AGROLUX 90DT','TRAKTOR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (190,'PKL 3554','AGROLUX 90DT','TRAKTOR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (191,'PKL 3562','AGROLUX 90DT','TRAKTOR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (192,'PKL 3570','AGROLUX 90DT','TRAKTOR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (193,'PKL 3584','AGROLUX 90DT','TRAKTOR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (194,'PKN 235','HINO FM2PNP','TOWING',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (195,'PKP 1485','HINO GD1JLPA','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (196,'PKP 1493','HINO GD1JLPA','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (197,'PKP 1497','HINO GD1JLPA','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (198,'PKR 5001','PROTON SAGA 1.3 (M)','CAR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (199,'PKU 2543','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (200,'PKU 2551','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (201,'PKU 2585','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (202,'PKU 9643','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (203,'PKU 9648','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (204,'PKU 9655','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (205,'PKU 9656','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (206,'PKV 3425','HINO FG1JPPB-UBS','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (207,'PKV 3435','HINO FG1JPPB-UBS','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (208,'PKV 4104','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (209,'PKV 4341','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (210,'PKV 5374','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (211,'PKV 5463','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (212,'PKV 6930','JCB 3CX','BACKHOE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (213,'PKV 7582','TOYOTA FORTUNER 2.7V','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (214,'PKV 7831','NISSAN PKD211RN','ROAD SWEEPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (215,'PKV 8415','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (216,'PKV 8416','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (217,'PKV 8423','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (218,'PKV 8426','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (219,'PKV 8427','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (220,'PKV 8437','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (221,'PKV 8440','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (222,'PKV 8846','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (223,'PKV 9436','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (224,'PKV 9715','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (225,'PKV 9723','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (226,'PKV 9735','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (227,'PKW 917','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (228,'PKW 925','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (229,'PKW 941','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (230,'PKW 1562','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (231,'PKW 1568','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (232,'PKW 1576','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (233,'PKW 1582','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (234,'PKW 2660','HINO GD1JLPA','CRANE',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (235,'PKW 2692','HINO XZU720R','SKYLIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (236,'PKW 4107','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (237,'PKW 4163','HINO XZU720R','SKYLIFT',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (238,'PKW 4174','ISUZU NKR55UEE','CREW KARGO AM',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (239,'PKW 6465','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (240,'PKX 7549','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (241,'PKY 254','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (242,'PKY 312','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (243,'PKY 4192','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (244,'PKY 9841','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (245,'PLA 1466','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (246,'PLA 7125','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (247,'PLB 1870','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (248,'PLB 2604','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (249,'PLB 6173','MITSUBISHI FUSO FM657','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (250,'PLB 6179','MITSUBISHI FUSO FM657','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (251,'PLB 6190','MITSUBISHI FUSO FM657','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (252,'PLB 6201','MITSUBISHI FUSO FM657','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (253,'PLB 6234','MITSUBISHI FUSO FM657','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (254,'PLB 6245','MITSUBISHI FUSO FM657','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (255,'PLB 6256','MITSUBISHI FUSO FM657','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (256,'PLB 6331','MITSUBISHI FUSO FM657','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (257,'PLB 6348','MITSUBISHI FUSO FM657','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (258,'PLB 6369','MITSUBISHI FUSO FM657','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (259,'PLB 6382','MITSUBISHI FUSO FM657','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (260,'PLB 6405','MITSUBISHI FUSO FM657','ROLL ON ROLL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (261,'PLC 396','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (262,'PLC 433','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (263,'PLC 3028','NISSAN PKD211RN','ROAD SWEEPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (264,'PLC 3082','NISSAN PKD211RN','ROAD SWEEPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (265,'PLC 4918','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (266,'PLD 596','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (267,'PLD 598','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (268,'PLD 829','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (269,'PLD 5714','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (270,'PLD 7706','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (271,'PLE 2201','NISSAN CWM272','HIGH PRESSURE JET',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (272,'PLE 2203','NISSAN CWM272','HIGH PRESSURE JET',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (273,'PLE 2198','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (274,'PLE 3676','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (275,'PLF 6971','NISSAN X-TRAIL 4WD 2.0L (AUTO)','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (276,'PLG 7314','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (277,'PLG 7625','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (278,'PLG 9166','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (279,'PLG 9180','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (280,'PLG 9215','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (281,'PLG 9234','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (282,'PLG 9244','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (283,'PLG 9250','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (284,'PLG 9254','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (285,'PLG 9436','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (286,'PLG 9516','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (287,'PLG 9526','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (288,'PLG 9529','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (289,'PLG 9531','ISUZU NKR55UEEH','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (290,'PLH 6179','TOYOTA HILUX 2.5MT','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (291,'PLH 6182','TOYOTA HIACE 2.5D','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (292,'PLJ 142','PROTON INSPIRA 1.8L (M)','CAR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (293,'PLJ 4606','PROTON EXORA FL 1.6L CVT','MPV',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (294,'PLJ 4608','PROTON EXORA FL 1.6L CVT','MPV',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (295,'PLL 3741','NEW HOLLAND TD80A','TRAKTOR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (296,'PLM 9429','NISSAN URVAN','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (297,'PLM 9441','NISSAN URVAN','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (298,'PLM 9445','NISSAN URVAN','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (299,'PLM 9451','NISSAN URVAN','WINDOW VAN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (300,'PLN 5051','NISSAN X-TRAIL 4WD 2.5L A/T','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (301,'PLN 5060','NISSAN X-TRAIL 4WD 2.5L A/T','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (302,'PLN 5078','NISSAN X-TRAIL 4WD 2.5L A/T','JIP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (303,'PLP 4524','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (304,'PLP 4532','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (305,'PLP 4542','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (306,'PLP 4544','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (307,'PLP 4547','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (308,'PLP 7319','HINO WU710R','DUMPER / TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (309,'PLQ 2401','ISUZU NKR55UEEH','CREW CAB',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (310,'PLQ 2402','ISUZU NKR55UEEH','CREW CAB',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (311,'PLQ 2214','ISUZU NKR55UEEH','CREW CAB',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (312,'PLQ 4839','HINO XZU730R (D / CAB)','KARGO AM',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (313,'PLQ 4616','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (314,'PLQ 4580','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (315,'PLQ 4602','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (316,'PLR 2102','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (317,'PLR 2026','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (318,'PLR  7872','ISUZU NKR55UEEH','LORI BONDED',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (319,'PLS 4056','HINO XZU730R (D / CAB)','LORI KAUNTER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (320,'PLT 4502','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (321,'PLT 4531','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (322,'PLT 5124','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (323,'PLT 7324','ISUZU FTR32R','PRESSURE JETTING',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (324,'PLT 8590','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (325,'PLU 2410','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (326,'PLV 1841','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (327,'PLV 3157','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (328,'PLV 4504','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (329,'PLV 6452','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (330,'PLV 6475','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (331,'PLV 6483','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (332,'PLV 7903','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (333,'PLV 7915','HINO FG1JPPB-UBS (D / CAB)','COMPACT LOADER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (334,'PLW 9213','HINO GD1JLPA','LORI KREN',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (335,'PLW 9415',' DEUTZ AGROLUX 50 4WD','TRAKTOR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (336,'PLW 9420','DEUTZ AGROLUX 50 4WD','TRAKTOR',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (337,'PLW 4030','HONDA NBC110MD','MOTOSIKAL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (338,'PLW 4031','HONDA NBC110MD','MOTOSIKAL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (339,'PLW 4032','HONDA NBC110MD','MOTOSIKAL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (340,'PLW 4034','HONDA NBC110MD','MOTOSIKAL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (341,'PLW 4035','HONDA NBC110MD','MOTOSIKAL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (342,'PLW 4036','HONDA NBC110MD','MOTOSIKAL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (343,'PLW 4038','HONDA NBC110MD','MOTOSIKAL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (344,'PLW 4039','HONDA NBC110MD','MOTOSIKAL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (345,'PLW 4041','HONDA NBC110MD','MOTOSIKAL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (346,'PLW 4042','HONDA NBC110MD','MOTOSIKAL',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (347,'PME 3759','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (348,'PME 3761','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (349,'PME 3779','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (350,'PME 3785','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (351,'PME 3790','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (352,'PME 3792','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (353,'PME 3802','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (354,'PME 3807','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (355,'PME 3826','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (356,'PME 3827','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (357,'PME 3829','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (358,'PME 3830','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (359,'PME 3832','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (360,'PME 3837','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (361,'PME 3855','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (362,'PME 3858','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (363,'PME 3871','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (364,'PME 5249','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (365,'PME 3875','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (366,'PME 3894','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (367,'PME 3901','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (368,'PME 5202','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (369,'PME 5204','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (370,'PME 5208','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (371,'PME 5209','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (372,'PME 5220','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (373,'PME 5232','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (374,'PME 5233','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (375,'PME 5234','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (376,'PME 5242','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (377,'PME 5251','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (378,'PME 5256','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (379,'PME 5261','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (380,'PME 5263','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (381,'PME 5271','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (382,'PME 5274','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (383,'PME 5277','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (384,'PME 5286','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (385,'PME 5293','NKR55UEEH','DUMPER/TIPPER',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
Insert into ESTOR.VEHICLE_LIST (ID,REG_NO,MODEL,TYPE,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (386,'PMH 3193','TOYOTA HILUX DOUBLE CAB 2.5 AT','PICK UP',0,null,to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('23-JUL-16 11.19.47.404000000 PM','DD-MON-RR HH.MI.SSXFF AM'),'1','1');
