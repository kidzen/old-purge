--------------------------------------------------------
--  File created - Friday-April-29-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence CATEGORIES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."CATEGORIES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence ESTOR_INVENTORIES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."ESTOR_INVENTORIES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 41 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence ESTOR_INVENTORIES_SEQ1
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."ESTOR_INVENTORIES_SEQ1"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence ESTOR_INVENTORIES_SEQ2
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."ESTOR_INVENTORIES_SEQ2"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 41 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence ESTOR_ITEMS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."ESTOR_ITEMS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence ESTOR_ITEMS_SEQ1
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."ESTOR_ITEMS_SEQ1"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence ESTOR_ITEMS_SEQ2
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."ESTOR_ITEMS_SEQ2"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence ESTOR_ITEMS_SEQ3
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."ESTOR_ITEMS_SEQ3"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence ESTOR_ROLES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."ESTOR_ROLES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 41 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence PACKAGE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."PACKAGE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence PEOPLE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."PEOPLE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 61 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence STOCKS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."STOCKS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 121 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence STOCKS_SEQ1
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."STOCKS_SEQ1"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence STOCK_ITEMS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."STOCK_ITEMS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence USAGE_LIST_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."USAGE_LIST_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Sequence VENDORS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ESTOR"."VENDORS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 61 CACHE 20 NOORDER  NOCYCLE
--------------------------------------------------------
--  DDL for Table ACTIVITY_LOGS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."ACTIVITY_LOGS" ("ID" NUMBER(11,0), "PERSON_ID" NUMBER(11,0), "ACTION" VARCHAR2(255), "MODEL" VARCHAR2(255), "MODEL_ID" NUMBER(11,0), "TITLE" VARCHAR2(255), "DESCRIPTION" VARCHAR2(255), "CREATED" TIMESTAMP (0), "DELETED" NUMBER(1,0) DEFAULT 0, "DELETED_DATE" TIMESTAMP (0))
--------------------------------------------------------
--  DDL for Table AUTH_ITEM
--------------------------------------------------------

  CREATE TABLE "ESTOR"."AUTH_ITEM" ("NAME" VARCHAR2(64), "TYPE" NUMBER, "DESCRIPTION" VARCHAR2(64), "RULE_NAME" VARCHAR2(64), "DATA" VARCHAR2(64), "CREATED_AT" NUMBER, "UPDATED_AT" NUMBER)
--------------------------------------------------------
--  DDL for Table AUTH_RULE
--------------------------------------------------------

  CREATE TABLE "ESTOR"."AUTH_RULE" ("NAME" VARCHAR2(64), "DATA" VARCHAR2(64), "CREATED_AT" NUMBER, "UPDATED_AT" NUMBER)
--------------------------------------------------------
--  DDL for Table CATEGORIES
--------------------------------------------------------

  CREATE TABLE "ESTOR"."CATEGORIES" ("ID" NUMBER(11,0), "NAME" VARCHAR2(255), "DELETED" NUMBER(1,0) DEFAULT 0, "DELETED_DATE" TIMESTAMP (0), "CREATED_DATE" TIMESTAMP (6), "UPDATED_DATE" TIMESTAMP (6), "CREATED_BY" VARCHAR2(255), "UPDATED_BY" VARCHAR2(255))
--------------------------------------------------------
--  DDL for Table ESTOR_INVENTORIES
--------------------------------------------------------

  CREATE TABLE "ESTOR"."ESTOR_INVENTORIES" ("ID" NUMBER(11,0), "CARD_NO" VARCHAR2(255), "CODE_NO" VARCHAR2(255), "DESCRIPTION" VARCHAR2(255), "QUANTITY" NUMBER DEFAULT 0, "CREATED_DATE" TIMESTAMP (0), "CREATED_BY" VARCHAR2(255), "UPDATED_DATE" TIMESTAMP (0), "UPDATED_BY" VARCHAR2(255), "PURCHASED_DATE" DATE, "APPROVED_BY" VARCHAR2(255), "APPROVED_DATE" TIMESTAMP (0), "APPROVED" NUMBER(1,0) DEFAULT 1, "DELETED" NUMBER(1,0) DEFAULT 0, "DELETED_DATE" TIMESTAMP (0), "CATEGORY_ID" NUMBER(11,0), "MIN_STOCK" NUMBER(11,0), "LOCATION" VARCHAR2(255),  PRIMARY KEY ("ID") ENABLE) ORGANIZATION INDEX NOCOMPRESS
--------------------------------------------------------
--  DDL for Table ESTOR_ITEMS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."ESTOR_ITEMS" ("ID" NUMBER(11,0), "SKU" NUMBER, "UNIT_PRICE" NUMBER, "CHECKIN_DATE" DATE, "CHECKOUT_DATE" DATE, "CREATED_DATE" TIMESTAMP (6), "UPDATED_DATE" TIMESTAMP (6), "CREATED_BY" NUMBER(11,0), "UPDATED_BY" NUMBER(11,0), "INVENTORY_ID" NUMBER(11,0), "DELETED" NUMBER(1,0) DEFAULT 0, "DELETED_DATE" TIMESTAMP (6))
--------------------------------------------------------
--  DDL for Table ESTOR_ROLES
--------------------------------------------------------

  CREATE TABLE "ESTOR"."ESTOR_ROLES" ("ID" NUMBER(11,0), "NAME" VARCHAR2(255), "DELETED" NUMBER(1,0) DEFAULT 0, "DELETED_DATE" TIMESTAMP (0), "CREATED_DATE" TIMESTAMP (6), "UPDATED_DATE" TIMESTAMP (6), "CREATED_BY" VARCHAR2(255), "UPDATED_BY" VARCHAR2(255),  PRIMARY KEY ("ID") ENABLE) ORGANIZATION INDEX NOCOMPRESS
--------------------------------------------------------
--  DDL for Table PACKAGE
--------------------------------------------------------

  CREATE TABLE "ESTOR"."PACKAGE" ("ID" NUMBER(11,0), "DETAIL" VARCHAR2(255), "DELIVERY" VARCHAR2(255), "PACKAGE_BY" VARCHAR2(255), "PACKAGE_DATE" DATE, "DELETED" NUMBER(1,0) DEFAULT 0, "DELETED_DATE" TIMESTAMP (6), "CREATED_DATE" TIMESTAMP (6), "UPDATED_DATE" TIMESTAMP (6), "CREATED_BY" NUMBER, "UPDATED_BY" NUMBER)
--------------------------------------------------------
--  DDL for Table PEOPLE
--------------------------------------------------------

  CREATE TABLE "ESTOR"."PEOPLE" ("ID" NUMBER(11,0), "USERNAME" VARCHAR2(255), "EMAIL" VARCHAR2(255), "PASSWORD" VARCHAR2(255), "ROLE_ID" NUMBER(11,0), "DELETED" NUMBER(1,0) DEFAULT 0, "DELETED_DATE" TIMESTAMP (0), "CREATED_DATE" TIMESTAMP (6), "UPDATED_DATE" TIMESTAMP (6), "PASSWORD_RESET_TOKEN" VARCHAR2(255), "AUTH_KEY" VARCHAR2(255))
--------------------------------------------------------
--  DDL for Table STOCKS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."STOCKS" ("ID" NUMBER(11,0), "ORDER_DATE" DATE, "ORDERED_BY" VARCHAR2(255), "TYPE" NUMBER, "DELETED" NUMBER(1,0) DEFAULT 0, "DELETED_DATE" TIMESTAMP (0), "APPROVED_BY" VARCHAR2(255), "APPROVED_DATE" TIMESTAMP (0), "APPROVED" NUMBER(1,0) DEFAULT 1, "USAGE" VARCHAR2(20 CHAR), "CREATED_DATE" TIMESTAMP (6), "UPDATED_DATE" TIMESTAMP (6), "CREATED_BY" VARCHAR2(255), "UPDATED_BY" VARCHAR2(255), "REQUIRED_DATE" DATE, "CHECKOUT_DATE" DATE, "CHECKOUT_BY" VARCHAR2(255), "ORDER_NO" VARCHAR2(20), "PACKAGE_ID" NUMBER(11,0))
--------------------------------------------------------
--  DDL for Table STOCK_ITEMS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."STOCK_ITEMS" ("ID" NUMBER(11,0), "INVENTORY_ID" NUMBER(11,0), "RQ_QUANTITY" NUMBER, "APP_QUANTITY" NUMBER, "CURRENT_BALANCE" NUMBER, "UNIT_PRICE" NUMBER(12,2), "STOCK_ID" NUMBER(11,0), "SKU" VARCHAR2(255), "DESCRIPTION" VARCHAR2(255), "DELETED" NUMBER(1,0) DEFAULT 0, "DELETED_DATE" TIMESTAMP (0), "CREATED_DATE" TIMESTAMP (6), "UPDATED_DATE" TIMESTAMP (6), "CREATED_BY" VARCHAR2(255), "UPDATED_BY" VARCHAR2(255), "TOTAL_PRICE" NUMBER)
--------------------------------------------------------
--  DDL for Table USAGE_LIST
--------------------------------------------------------

  CREATE TABLE "ESTOR"."USAGE_LIST" ("ID" NUMBER(3,0), "NAME" VARCHAR2(20), "DELETED" NUMBER(1,0), "DELETED_DATE" TIMESTAMP (6), "CREATED_DATE" TIMESTAMP (6), "UPDATED_DATE" TIMESTAMP (6), "CREATED_BY" VARCHAR2(20), "UPDATED_BY" VARCHAR2(20))
--------------------------------------------------------
--  DDL for Table VENDORS
--------------------------------------------------------

  CREATE TABLE "ESTOR"."VENDORS" ("ID" NUMBER(11,0), "NAME" VARCHAR2(255), "DELETED" NUMBER(1,0) DEFAULT 0, "DELETED_DATE" TIMESTAMP (0), "ADDRESS" VARCHAR2(255), "CONTACT_NO" VARCHAR2(50), "CREATED_DATE" TIMESTAMP (6), "UPDATED_DATE" TIMESTAMP (6), "CREATED_BY" VARCHAR2(255), "UPDATED_BY" VARCHAR2(255))
REM INSERTING into ESTOR.ACTIVITY_LOGS
SET DEFINE OFF;
REM INSERTING into ESTOR.AUTH_ITEM
SET DEFINE OFF;
REM INSERTING into ESTOR.AUTH_RULE
SET DEFINE OFF;
REM INSERTING into ESTOR.CATEGORIES
SET DEFINE OFF;
Insert into ESTOR.CATEGORIES (ID,NAME,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (1,'CAT',0,null,to_timestamp('31/03/2016 17:45:04.502000000','DD/MM/RRRR HH24:MI:SSXFF'),to_timestamp('07/04/2016 21:32:19.084000000','DD/MM/RRRR HH24:MI:SSXFF'),'5','5');
REM INSERTING into ESTOR.ESTOR_INVENTORIES
SET DEFINE OFF;
Insert into ESTOR.ESTOR_INVENTORIES (ID,CARD_NO,CODE_NO,DESCRIPTION,QUANTITY,CREATED_DATE,CREATED_BY,UPDATED_DATE,UPDATED_BY,PURCHASED_DATE,APPROVED_BY,APPROVED_DATE,APPROVED,DELETED,DELETED_DATE,CATEGORY_ID,MIN_STOCK,LOCATION) values (21,'123','123','Carburetor',10,to_timestamp('24/04/2016 07:41:24.000000000','DD/MM/RRRR HH24:MI:SSXFF'),'5',to_timestamp('24/04/2016 08:03:44.000000000','DD/MM/RRRR HH24:MI:SSXFF'),'5',null,null,null,0,0,null,1,1,null);
Insert into ESTOR.ESTOR_INVENTORIES (ID,CARD_NO,CODE_NO,DESCRIPTION,QUANTITY,CREATED_DATE,CREATED_BY,UPDATED_DATE,UPDATED_BY,PURCHASED_DATE,APPROVED_BY,APPROVED_DATE,APPROVED,DELETED,DELETED_DATE,CATEGORY_ID,MIN_STOCK,LOCATION) values (23,'2222','33333','MOTOR',12,to_timestamp('24/04/2016 14:04:07.000000000','DD/MM/RRRR HH24:MI:SSXFF'),'5',to_timestamp('24/04/2016 14:04:07.000000000','DD/MM/RRRR HH24:MI:SSXFF'),'5',null,null,null,0,0,null,null,1,null);
REM INSERTING into ESTOR.ESTOR_ITEMS
SET DEFINE OFF;
Insert into ESTOR.ESTOR_ITEMS (ID,SKU,UNIT_PRICE,CHECKIN_DATE,CHECKOUT_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY,INVENTORY_ID,DELETED,DELETED_DATE) values (12,222,2,null,null,to_timestamp('24/04/2016 08:03:43.532000000','DD/MM/RRRR HH24:MI:SSXFF'),to_timestamp('24/04/2016 08:03:43.532000000','DD/MM/RRRR HH24:MI:SSXFF'),5,5,21,0,null);
Insert into ESTOR.ESTOR_ITEMS (ID,SKU,UNIT_PRICE,CHECKIN_DATE,CHECKOUT_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY,INVENTORY_ID,DELETED,DELETED_DATE) values (13,1111111,1,null,null,to_timestamp('24/04/2016 08:03:43.533000000','DD/MM/RRRR HH24:MI:SSXFF'),to_timestamp('24/04/2016 08:03:43.533000000','DD/MM/RRRR HH24:MI:SSXFF'),5,5,21,0,null);
Insert into ESTOR.ESTOR_ITEMS (ID,SKU,UNIT_PRICE,CHECKIN_DATE,CHECKOUT_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY,INVENTORY_ID,DELETED,DELETED_DATE) values (14,333,3,null,null,to_timestamp('24/04/2016 08:03:43.534000000','DD/MM/RRRR HH24:MI:SSXFF'),to_timestamp('24/04/2016 08:03:43.534000000','DD/MM/RRRR HH24:MI:SSXFF'),5,5,21,0,null);
Insert into ESTOR.ESTOR_ITEMS (ID,SKU,UNIT_PRICE,CHECKIN_DATE,CHECKOUT_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY,INVENTORY_ID,DELETED,DELETED_DATE) values (1,4444,1,null,null,to_timestamp('24/04/2016 14:04:07.142000000','DD/MM/RRRR HH24:MI:SSXFF'),to_timestamp('24/04/2016 14:04:07.142000000','DD/MM/RRRR HH24:MI:SSXFF'),5,5,23,0,null);
REM INSERTING into ESTOR.ESTOR_ROLES
SET DEFINE OFF;
Insert into ESTOR.ESTOR_ROLES (ID,NAME,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (1,'Administrator',0,null,null,null,null,null);
Insert into ESTOR.ESTOR_ROLES (ID,NAME,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (2,'Pegawai Stor',0,null,null,null,null,null);
Insert into ESTOR.ESTOR_ROLES (ID,NAME,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (4,'User',0,null,null,to_timestamp('16/04/2016 13:13:19.684000000','DD/MM/RRRR HH24:MI:SSXFF'),null,'5');
Insert into ESTOR.ESTOR_ROLES (ID,NAME,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (5,'Manager',0,null,to_timestamp('12/04/2016 03:54:19.810000000','DD/MM/RRRR HH24:MI:SSXFF'),to_timestamp('12/04/2016 03:54:19.810000000','DD/MM/RRRR HH24:MI:SSXFF'),'2','2');
REM INSERTING into ESTOR.PACKAGE
SET DEFINE OFF;
REM INSERTING into ESTOR.PEOPLE
SET DEFINE OFF;
Insert into ESTOR.PEOPLE (ID,USERNAME,EMAIL,PASSWORD,ROLE_ID,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,PASSWORD_RESET_TOKEN,AUTH_KEY) values (1,'Admin','admin@epuskeb.appgrid.my','$2y$13$WdzJ8.TIx4ClbL2ZtoeRGO4YNMSMyyABpNgRs3JlUzzEuzGOd1Gp6',1,0,null,null,to_timestamp('06/04/2016 04:09:05.434000000','DD/MM/RRRR HH24:MI:SSXFF'),null,null);
Insert into ESTOR.PEOPLE (ID,USERNAME,EMAIL,PASSWORD,ROLE_ID,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,PASSWORD_RESET_TOKEN,AUTH_KEY) values (3,'Pegawai Stor','store.officer@epuskeb.appgrid.my','2ae4a9e42594b2b0cf331e590953bc47872a655e',2,0,null,null,null,null,null);
Insert into ESTOR.PEOPLE (ID,USERNAME,EMAIL,PASSWORD,ROLE_ID,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,PASSWORD_RESET_TOKEN,AUTH_KEY) values (4,'Shabeila','shabeila@mpsp.gov.my','2ae4a9e42594b2b0cf331e590953bc47872a655e',2,0,null,null,null,null,null);
Insert into ESTOR.PEOPLE (ID,USERNAME,EMAIL,PASSWORD,ROLE_ID,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,PASSWORD_RESET_TOKEN,AUTH_KEY) values (6,'admin3','admin3@gmail.com','$2y$13$IvErxApLM.Pdi1CUOUcvkeXEXJy6QE1XtYm0SvFCKuuux4ZPWcs3m',null,0,null,to_timestamp('31/03/2016 17:34:10.836000000','DD/MM/RRRR HH24:MI:SSXFF'),to_timestamp('10/04/2016 19:43:44.622000000','DD/MM/RRRR HH24:MI:SSXFF'),null,null);
Insert into ESTOR.PEOPLE (ID,USERNAME,EMAIL,PASSWORD,ROLE_ID,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,PASSWORD_RESET_TOKEN,AUTH_KEY) values (5,'admin2','admin2@gmail.com','$2y$13$rUjzwePDcK8UfxvuLE4.l.NkITfKpO5zO4Hi1.iTC1IhA6CjAQoly',1,0,null,null,to_timestamp('03/04/2016 05:05:15.300000000','DD/MM/RRRR HH24:MI:SSXFF'),null,'fZU4wd6K3fYBvVVC1zVC5KlydCAUirBh');
Insert into ESTOR.PEOPLE (ID,USERNAME,EMAIL,PASSWORD,ROLE_ID,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,PASSWORD_RESET_TOKEN,AUTH_KEY) values (41,'user1','user1@gmail.com','$2y$13$fJv0ldd5EXCJoXWvoH9.eeJJiV9JF0GJeB7yyF0vhOcTn1Rytj2fO',4,0,null,to_timestamp('11/04/2016 08:02:46.545000000','DD/MM/RRRR HH24:MI:SSXFF'),to_timestamp('11/04/2016 08:02:46.545000000','DD/MM/RRRR HH24:MI:SSXFF'),null,'CIo92mKYPHd9pXdeOxU6R9EGxNnOqcGg');
Insert into ESTOR.PEOPLE (ID,USERNAME,EMAIL,PASSWORD,ROLE_ID,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,PASSWORD_RESET_TOKEN,AUTH_KEY) values (42,'pegstor','pegstor@gmail.com','$2y$13$/Z6LtCnTuOM3X8BfA5wBKueCNNuLnUrZsdVJIdLYjbuz9ZGxG8qlW',4,0,null,to_timestamp('12/04/2016 03:50:52.219000000','DD/MM/RRRR HH24:MI:SSXFF'),to_timestamp('12/04/2016 03:50:52.219000000','DD/MM/RRRR HH24:MI:SSXFF'),null,'i1GWYXhfk6yuZUoMTlMeOuz0vz3ar2yd');
Insert into ESTOR.PEOPLE (ID,USERNAME,EMAIL,PASSWORD,ROLE_ID,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,PASSWORD_RESET_TOKEN,AUTH_KEY) values (43,'manager','manager@gmail.com','$2y$13$AMwIhHBZ8q8YjS63EFE8P.2s88/blwlVbk7Mi4fQIW5izw43/XoUy',4,0,null,to_timestamp('12/04/2016 03:51:54.539000000','DD/MM/RRRR HH24:MI:SSXFF'),to_timestamp('12/04/2016 03:51:54.539000000','DD/MM/RRRR HH24:MI:SSXFF'),null,'DXWmpi_etnalBLRUdspJno0BVCcWXR4D');
Insert into ESTOR.PEOPLE (ID,USERNAME,EMAIL,PASSWORD,ROLE_ID,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,PASSWORD_RESET_TOKEN,AUTH_KEY) values (2,'admin1','kidzenfxc@gmail.com','$2y$13$hPJ8J7GIPPLvfWuWNFl8W.tMDxVTKaBDB5RgZIZAgkKvpQYyncW.W',2,0,null,null,to_timestamp('12/04/2016 10:09:12.640000000','DD/MM/RRRR HH24:MI:SSXFF'),'XqebdU5z-IP9yi2Wmyj8OOUHcXlokapD_1460416550',null);
Insert into ESTOR.PEOPLE (ID,USERNAME,EMAIL,PASSWORD,ROLE_ID,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,PASSWORD_RESET_TOKEN,AUTH_KEY) values (21,'ps','ps@gmail.com','$2y$13$M3YOxuTLgoy07DGczw1agOkFV6lfSOtqpkLhpQZPN/BDrUrqgbaNG',2,0,null,to_timestamp('06/04/2016 05:18:55.905000000','DD/MM/RRRR HH24:MI:SSXFF'),to_timestamp('06/04/2016 05:31:40.100000000','DD/MM/RRRR HH24:MI:SSXFF'),null,'u4FDSc6-TSbyy9SPNF2NY62kg7IUg67c');
REM INSERTING into ESTOR.STOCKS
SET DEFINE OFF;
REM INSERTING into ESTOR.STOCK_ITEMS
SET DEFINE OFF;
REM INSERTING into ESTOR.USAGE_LIST
SET DEFINE OFF;
Insert into ESTOR.USAGE_LIST (ID,NAME,DELETED,DELETED_DATE,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (2,'CHAINSAW',0,null,to_timestamp('07/04/2016 18:31:30.316000000','DD/MM/RRRR HH24:MI:SSXFF'),to_timestamp('07/04/2016 18:31:30.316000000','DD/MM/RRRR HH24:MI:SSXFF'),'5','5');
REM INSERTING into ESTOR.VENDORS
SET DEFINE OFF;
Insert into ESTOR.VENDORS (ID,NAME,DELETED,DELETED_DATE,ADDRESS,CONTACT_NO,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (1,'SYKT A SDN BHD',0,null,'Butterworth','1234567',null,null,'1','1');
Insert into ESTOR.VENDORS (ID,NAME,DELETED,DELETED_DATE,ADDRESS,CONTACT_NO,CREATED_DATE,UPDATED_DATE,CREATED_BY,UPDATED_BY) values (41,'hai',0,null,'asd','asd',to_timestamp('11/04/2016 06:55:27.953000000','DD/MM/RRRR HH24:MI:SSXFF'),to_timestamp('11/04/2016 06:55:39.030000000','DD/MM/RRRR HH24:MI:SSXFF'),'5','5');
--------------------------------------------------------
--  DDL for Index ACTIVITY_LOGS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."ACTIVITY_LOGS_PK" ON "ESTOR"."ACTIVITY_LOGS" ("ID")
--------------------------------------------------------
--  DDL for Index AUTH_ITEM_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."AUTH_ITEM_PK" ON "ESTOR"."AUTH_ITEM" ("NAME")
--------------------------------------------------------
--  DDL for Index AUTH_RULE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."AUTH_RULE_PK" ON "ESTOR"."AUTH_RULE" ("NAME")
--------------------------------------------------------
--  DDL for Index CATEGORIES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."CATEGORIES_PK" ON "ESTOR"."CATEGORIES" ("ID")
--------------------------------------------------------
--  DDL for Index ESTOR_ITEMS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."ESTOR_ITEMS_PK" ON "ESTOR"."ESTOR_ITEMS" ("ID")
--------------------------------------------------------
--  DDL for Index PACKAGE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."PACKAGE_PK" ON "ESTOR"."PACKAGE" ("ID")
--------------------------------------------------------
--  DDL for Index PEOPLE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."PEOPLE_PK" ON "ESTOR"."PEOPLE" ("ID")
--------------------------------------------------------
--  DDL for Index SYS_C0010453
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."SYS_C0010453" ON "ESTOR"."VENDORS" ("ID")
--------------------------------------------------------
--  DDL for Index SYS_C0010455
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."SYS_C0010455" ON "ESTOR"."STOCKS" ("ID")
--------------------------------------------------------
--  DDL for Index SYS_C0012441
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."SYS_C0012441" ON "ESTOR"."STOCK_ITEMS" ("ID")
--------------------------------------------------------
--  DDL for Index SYS_IOT_TOP_74594
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."SYS_IOT_TOP_74594" ON "ESTOR"."ESTOR_INVENTORIES" ("ID")
--------------------------------------------------------
--  DDL for Index SYS_IOT_TOP_74596
--------------------------------------------------------

  CREATE UNIQUE INDEX "ESTOR"."SYS_IOT_TOP_74596" ON "ESTOR"."ESTOR_ROLES" ("ID")
--------------------------------------------------------
--  DDL for Index USAGE_LIST_INDEX1
--------------------------------------------------------

  CREATE INDEX "ESTOR"."USAGE_LIST_INDEX1" ON "ESTOR"."USAGE_LIST" ("ID")
--------------------------------------------------------
--  Constraints for Table ACTIVITY_LOGS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."ACTIVITY_LOGS" ADD CONSTRAINT "ACTIVITY_LOGS_PK" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "ESTOR"."ACTIVITY_LOGS" MODIFY ("ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table AUTH_ITEM
--------------------------------------------------------

  ALTER TABLE "ESTOR"."AUTH_ITEM" ADD CONSTRAINT "AUTH_ITEM_PK" PRIMARY KEY ("NAME") ENABLE
 
  ALTER TABLE "ESTOR"."AUTH_ITEM" MODIFY ("NAME" NOT NULL ENABLE)
 
  ALTER TABLE "ESTOR"."AUTH_ITEM" MODIFY ("TYPE" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table AUTH_RULE
--------------------------------------------------------

  ALTER TABLE "ESTOR"."AUTH_RULE" ADD CONSTRAINT "AUTH_RULE_PK" PRIMARY KEY ("NAME") ENABLE
 
  ALTER TABLE "ESTOR"."AUTH_RULE" MODIFY ("NAME" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table CATEGORIES
--------------------------------------------------------

  ALTER TABLE "ESTOR"."CATEGORIES" ADD CONSTRAINT "CATEGORIES_PK" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "ESTOR"."CATEGORIES" MODIFY ("ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table ESTOR_INVENTORIES
--------------------------------------------------------

  ALTER TABLE "ESTOR"."ESTOR_INVENTORIES" MODIFY ("ID" NOT NULL ENABLE)
 
  ALTER TABLE "ESTOR"."ESTOR_INVENTORIES" ADD PRIMARY KEY ("ID") ENABLE
--------------------------------------------------------
--  Constraints for Table ESTOR_ITEMS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."ESTOR_ITEMS" ADD CONSTRAINT "ESTOR_ITEMS_PK" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "ESTOR"."ESTOR_ITEMS" MODIFY ("ID" NOT NULL ENABLE)
 
  ALTER TABLE "ESTOR"."ESTOR_ITEMS" MODIFY ("INVENTORY_ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table ESTOR_ROLES
--------------------------------------------------------

  ALTER TABLE "ESTOR"."ESTOR_ROLES" MODIFY ("ID" NOT NULL ENABLE)
 
  ALTER TABLE "ESTOR"."ESTOR_ROLES" ADD PRIMARY KEY ("ID") ENABLE
--------------------------------------------------------
--  Constraints for Table PACKAGE
--------------------------------------------------------

  ALTER TABLE "ESTOR"."PACKAGE" ADD CONSTRAINT "PACKAGE_PK" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "ESTOR"."PACKAGE" MODIFY ("ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table PEOPLE
--------------------------------------------------------

  ALTER TABLE "ESTOR"."PEOPLE" ADD CONSTRAINT "PEOPLE_PK" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "ESTOR"."PEOPLE" MODIFY ("ID" NOT NULL ENABLE)
--------------------------------------------------------
--  Constraints for Table STOCKS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."STOCKS" ADD PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "ESTOR"."STOCKS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."STOCKS" MODIFY ("ID" NOT NULL ENABLE)
 
  ALTER TABLE "ESTOR"."STOCKS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."STOCKS" ADD CHECK ("ID" IS NOT NULL) ENABLE
--------------------------------------------------------
--  Constraints for Table STOCK_ITEMS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CONSTRAINT "SYS_C0012441" PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" MODIFY ("ID" NOT NULL ENABLE)
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" MODIFY ("INVENTORY_ID" NOT NULL ENABLE)
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" MODIFY ("STOCK_ID" NOT NULL ENABLE)
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CHECK ("ID" IS NOT NULL) ENABLE
--------------------------------------------------------
--  Constraints for Table USAGE_LIST
--------------------------------------------------------

  ALTER TABLE "ESTOR"."USAGE_LIST" MODIFY ("ID" NOT NULL ENABLE)
 
  ALTER TABLE "ESTOR"."USAGE_LIST" ADD CONSTRAINT "USAGE_LIST_PK" PRIMARY KEY ("ID") ENABLE
--------------------------------------------------------
--  Constraints for Table VENDORS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."VENDORS" ADD PRIMARY KEY ("ID") ENABLE
 
  ALTER TABLE "ESTOR"."VENDORS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."VENDORS" MODIFY ("ID" NOT NULL ENABLE)
 
  ALTER TABLE "ESTOR"."VENDORS" ADD CHECK ("ID" IS NOT NULL) ENABLE
 
  ALTER TABLE "ESTOR"."VENDORS" ADD CHECK ("ID" IS NOT NULL) ENABLE
--------------------------------------------------------
--  Ref Constraints for Table AUTH_ITEM
--------------------------------------------------------

  ALTER TABLE "ESTOR"."AUTH_ITEM" ADD CONSTRAINT "AUTH_ITEM_FK1" FOREIGN KEY ("RULE_NAME") REFERENCES "ESTOR"."AUTH_RULE" ("NAME") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table ESTOR_INVENTORIES
--------------------------------------------------------

  ALTER TABLE "ESTOR"."ESTOR_INVENTORIES" ADD CONSTRAINT "ESTOR_INVENTORIES_FK1" FOREIGN KEY ("CATEGORY_ID") REFERENCES "ESTOR"."CATEGORIES" ("ID") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table ESTOR_ITEMS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."ESTOR_ITEMS" ADD CONSTRAINT "ESTOR_ITEMS_FK1" FOREIGN KEY ("INVENTORY_ID") REFERENCES "ESTOR"."ESTOR_INVENTORIES" ("ID") ON DELETE CASCADE ENABLE
--------------------------------------------------------
--  Ref Constraints for Table PEOPLE
--------------------------------------------------------

  ALTER TABLE "ESTOR"."PEOPLE" ADD CONSTRAINT "FK_PPL_ROLE" FOREIGN KEY ("ROLE_ID") REFERENCES "ESTOR"."ESTOR_ROLES" ("ID") ENABLE
--------------------------------------------------------
--  Ref Constraints for Table STOCKS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."STOCKS" ADD CONSTRAINT "STOCKS_FK1" FOREIGN KEY ("PACKAGE_ID") REFERENCES "ESTOR"."PACKAGE" ("ID") ON DELETE CASCADE ENABLE
--------------------------------------------------------
--  Ref Constraints for Table STOCK_ITEMS
--------------------------------------------------------

  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CONSTRAINT "STOCK_ITEMS_FK1" FOREIGN KEY ("INVENTORY_ID") REFERENCES "ESTOR"."ESTOR_INVENTORIES" ("ID") ENABLE
 
  ALTER TABLE "ESTOR"."STOCK_ITEMS" ADD CONSTRAINT "STOCK_ITEMS_FK2" FOREIGN KEY ("STOCK_ID") REFERENCES "ESTOR"."STOCKS" ("ID") ENABLE
--------------------------------------------------------
--  DDL for Trigger CATEGORIES_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."CATEGORIES_TRG" 
BEFORE INSERT ON CATEGORIES 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT CATEGORIES_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."CATEGORIES_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger ESTOR_INVENTORIES_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."ESTOR_INVENTORIES_TRG" 
BEFORE INSERT ON ESTOR_INVENTORIES 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    NULL;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."ESTOR_INVENTORIES_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger ESTOR_INVENTORIES_TRG1
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."ESTOR_INVENTORIES_TRG1" 
BEFORE INSERT ON ESTOR_INVENTORIES 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    NULL;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."ESTOR_INVENTORIES_TRG1" ENABLE
--------------------------------------------------------
--  DDL for Trigger ESTOR_INVENTORIES_TRG2
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."ESTOR_INVENTORIES_TRG2" 
BEFORE INSERT ON ESTOR_INVENTORIES 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT ESTOR_INVENTORIES_SEQ2.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."ESTOR_INVENTORIES_TRG2" ENABLE
--------------------------------------------------------
--  DDL for Trigger ESTOR_ITEMS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."ESTOR_ITEMS_TRG" 
BEFORE INSERT ON ESTOR_ITEMS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    NULL;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."ESTOR_ITEMS_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger ESTOR_ITEMS_TRG1
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."ESTOR_ITEMS_TRG1" 
BEFORE INSERT ON ESTOR_ITEMS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    NULL;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."ESTOR_ITEMS_TRG1" ENABLE
--------------------------------------------------------
--  DDL for Trigger ESTOR_ITEMS_TRG2
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."ESTOR_ITEMS_TRG2" 
BEFORE INSERT ON ESTOR_ITEMS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    NULL;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."ESTOR_ITEMS_TRG2" ENABLE
--------------------------------------------------------
--  DDL for Trigger ESTOR_ITEMS_TRG3
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."ESTOR_ITEMS_TRG3" 
BEFORE INSERT ON ESTOR_ITEMS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT ESTOR_ITEMS_SEQ3.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."ESTOR_ITEMS_TRG3" ENABLE
--------------------------------------------------------
--  DDL for Trigger ESTOR_ROLES_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."ESTOR_ROLES_TRG" 
BEFORE INSERT ON ESTOR_ROLES 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT ESTOR_ROLES_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."ESTOR_ROLES_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger PACKAGE_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."PACKAGE_TRG" 
BEFORE INSERT ON PACKAGE 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    NULL;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."PACKAGE_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger PEOPLE_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."PEOPLE_TRG" 
BEFORE INSERT ON PEOPLE 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT PEOPLE_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."PEOPLE_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger STOCKS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."STOCKS_TRG" 
BEFORE INSERT ON STOCKS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    NULL;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."STOCKS_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger STOCKS_TRG1
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."STOCKS_TRG1" 
BEFORE INSERT ON STOCKS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    NULL;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."STOCKS_TRG1" ENABLE
--------------------------------------------------------
--  DDL for Trigger STOCK_ITEMS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."STOCK_ITEMS_TRG" 
BEFORE INSERT ON STOCK_ITEMS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    NULL;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."STOCK_ITEMS_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger STOCK_ITEMS_TRG1
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."STOCK_ITEMS_TRG1" 
BEFORE INSERT ON STOCK_ITEMS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT STOCK_ITEMS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."STOCK_ITEMS_TRG1" ENABLE
--------------------------------------------------------
--  DDL for Trigger USAGE_LIST_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."USAGE_LIST_TRG" 
BEFORE INSERT ON USAGE_LIST 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT USAGE_LIST_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."USAGE_LIST_TRG" ENABLE
--------------------------------------------------------
--  DDL for Trigger VENDORS_TRG
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "ESTOR"."VENDORS_TRG" 
BEFORE INSERT ON VENDORS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT VENDORS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
ALTER TRIGGER "ESTOR"."VENDORS_TRG" ENABLE
