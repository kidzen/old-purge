--------------------------------------------------------
--  File created - Wednesday-November-23-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View APPROVAL
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."APPROVAL" ("ID", "AVAILABLE_PER_ORDER", "ID_INVENTORY_ITEMS", "ID_INVENTORIES", "ID_CATEGORIES", "ID_INVENTORIES_CHECKIN", "ID_ORDER_ITEMS", "ID_ORDERS", "SKU", "UNIT_PRICE", "ORDER_NO", "ITEMS_CATEGORY_NAME", "ITEMS_INVENTORY_NAME", "RQ_QUANTITY", "APP_QUANTITY", "APPROVED", "CURRENT_BALANCE", "TOTAL_PRICE", "ORDER_DATE", "REQUIRED_DATE") AS 
  select
  rownum as id
  ,count(*) over(partition by oi.id) available_per_order
  ,ii.id id_inventory_items
  ,i.id id_inventories
  ,c.id id_categories
  ,ic.id id_inventories_checkin
  ,oi.id id_order_items
  ,o.id id_orders
  ,ii.sku
  ,ii.UNIT_PRICE
--  ,t.TYPE
  ,o.ORDER_NO
--  ,t.CHECK_DATE transaction_date
  ,c.NAME items_category_name
  ,i.DESCRIPTION items_inventory_name
  ,oi.RQ_QUANTITY
  ,oi.APP_QUANTITY
  ,o.APPROVED
  ,oi.CURRENT_BALANCE
  ,oi.UNIT_PRICE total_price
  ,o.ORDER_DATE
  ,o.REQUIRED_DATE
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
left join INVENTORIES i on i.id = ic.INVENTORY_ID
left join CATEGORIES c on c.id = i.CATEGORY_ID
left join ORDER_ITEMS oi on oi.inventory_id = i.id  
left join ORDERS o on o.id = oi.order_ID
--  left join vehicle_list vl on vl.id = o.vehicle_id
--  left join TRANSACTIONS t on t.id = o.TRANSACTION_ID
where ii.CHECKOUT_TRANSACTION_ID is null and ii.deleted = 0 and NVL(oi.app_quantity,0) < oi.rq_quantity;
--------------------------------------------------------
--  DDL for View I_QUANTITY_CHECK
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."I_QUANTITY_CHECK" ("ID", "INVENTORY_QUANTITY", "ACTUAL_INVENTORY_QUANTITY") AS 
  select "ID","INVENTORY_QUANTITY","ACTUAL_INVENTORY_QUANTITY" from (
select distinct
  i.id,
  i.QUANTITY inventory_quantity,
  count(*) over (partition by i.id) actual_inventory_quantity
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN iC on ic.id = ii.CHECKIN_TRANSACTION_ID
left join inventories i on i.id = ic.INVENTORY_ID
where ii.CHECKOUT_TRANSACTION_ID is null
)
where inventory_quantity != actual_inventory_quantity;
--------------------------------------------------------
--  DDL for View IC_PRICE_CHECK
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."IC_PRICE_CHECK" ("IV_ID", "IC_ID", "IC_TOTAL_PRICE", "ACTUAL_TOTAL_PRICE") AS 
  select distinct "IV_ID","IC_ID","IC_TOTAL_PRICE","ACTUAL_TOTAL_PRICE" from (
  select  
    iv_id,ic_id
    ,ic_total_price
    ,sum(unit_price) over (partition by ic_id) actual_total_price
  from (
    select 
      i.id iv_id
      ,ic.id ic_id
      ,i.quantity iv_quantity
      ,ic.ITEMS_QUANTITY ic_quantity
      ,ii.unit_price
      ,ic.ITEMS_TOTAL_PRICE ic_total_price
    from INVENTORY_ITEMS ii
    left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
    left join INVENTORIES i on i.id = ic.INVENTORY_ID
    where ii.deleted = 0 and i.deleted = 0 and ic.deleted = 0
  )
)
where ic_total_price != actual_total_price;
--------------------------------------------------------
--  DDL for View IC_QUANTITY_CHECK
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."IC_QUANTITY_CHECK" ("IV_ID", "IC_ID", "IV_QUANTITY", "ACTUAL_QUANTITY") AS 
  select distinct "IV_ID","IC_ID","IV_QUANTITY","ACTUAL_QUANTITY" from (
  select
    iv_id,ic_id
    ,iv_quantity
    ,count(*) over (partition by iv_id) actual_quantity
--    ,ic_total_price
  --  ,sum(unit_price) over (partition by iv_id) actual_total_price
  from (
    select 
      i.id iv_id
      ,ic.id ic_id
      ,i.quantity iv_quantity
      ,ic.ITEMS_QUANTITY ic_quantity
      ,ii.unit_price
      ,ic.ITEMS_TOTAL_PRICE ic_total_price
    from INVENTORY_ITEMS ii
    left join INVENTORIES_CHECKIN ic on ic.id = ii.CHECKIN_TRANSACTION_ID
    left join INVENTORIES i on i.id = ic.INVENTORY_ID
    where ii.checkout_transaction_id is null 
    and ii.deleted = 0 and i.deleted = 0 and ic.deleted = 0
    --and ii.INVENTORY_ID = 149
  )
)
where iv_quantity != actual_quantity;
--------------------------------------------------------
--  DDL for View LAPORAN_PENYELENGARAAN_SISKEN
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."LAPORAN_PENYELENGARAAN_SISKEN" ("ID", "TAHUN", "BULAN", "NO_KERJA", "ARAHAN_KERJA", "JK_JG", "BENGKEL_PANEL_ID", "NAMA_PANEL", "KATEGORI_PANEL", "SELENGGARA_ID", "TEMPOH_SELENGGARA", "ALASAN_ID", "ALASAN", "TARIKH_ARAHAN", "NO_SEBUTHARGA", "TARIKH_KENDERAAN_TIBA", "TARIKH_JANGKA_SIAP", "TARIKH_SEBUTHARGA", "STATUS_SEBUTHARGA", "NO_DO", "TARIKH_SIAP", "TEMPOH_JAMINAN", "TARIKH_DO", "TARIKH_CETAK", "STATUS", "CATATAN_SEBUTHARGA", "CATATAN", "NAMA_PIC", "TARIKH_PERMOHONAN", "ID_KENDERAAN", "KATEGORI_KEROSAKAN_ID", "KATEGORI_KEROSAKAN", "KATEGORI", "ISSERVICE", "ISPANCIT", "ODOMETER_TERKINI", "NO_PLAT", "MODEL", "KOD_JABATAN", "NAMA_JABATAN", "JUMLAH_KOS") AS 
  select "ID","TAHUN","BULAN","NO_KERJA","ARAHAN_KERJA","JK_JG","BENGKEL_PANEL_ID","NAMA_PANEL","KATEGORI_PANEL","SELENGGARA_ID","TEMPOH_SELENGGARA","ALASAN_ID","ALASAN","TARIKH_ARAHAN","NO_SEBUTHARGA","TARIKH_KENDERAAN_TIBA","TARIKH_JANGKA_SIAP","TARIKH_SEBUTHARGA","STATUS_SEBUTHARGA","NO_DO","TARIKH_SIAP","TEMPOH_JAMINAN","TARIKH_DO","TARIKH_CETAK","STATUS","CATATAN_SEBUTHARGA","CATATAN","NAMA_PIC","TARIKH_PERMOHONAN","ID_KENDERAAN","KATEGORI_KEROSAKAN_ID","KATEGORI_KEROSAKAN","KATEGORI","ISSERVICE","ISPANCIT","ODOMETER_TERKINI","NO_PLAT","MODEL","KOD_JABATAN","NAMA_JABATAN","JUMLAH_KOS" from USERSISKEN.LAPORAN_PENYELENGGARAAN_VIEW;
--------------------------------------------------------
--  DDL for View LAPORAN_PENYELENGGARAAN_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."LAPORAN_PENYELENGGARAAN_VIEW" ("TAHUN", "BULAN", "ID", "NO_KERJA", "ARAHAN_KERJA", "JK_JG", "BENGKEL_PANEL_ID", "NAMA_PANEL", "KATEGORI_PANEL", "SELENGGARA_ID", "TEMPOH_SELENGGARA", "ALASAN_ID", "ALASAN", "TARIKH_ARAHAN", "NO_SEBUTHARGA", "TARIKH_KENDERAAN_TIBA", "TARIKH_JANGKA_SIAP", "TARIKH_SEBUTHARGA", "STATUS_SEBUTHARGA", "NO_DO", "TARIKH_SIAP", "TEMPOH_JAMINAN", "TARIKH_DO", "TARIKH_CETAK", "STATUS", "CATATAN_SEBUTHARGA", "CATATAN", "NAMA_PIC", "TARIKH_PERMOHONAN", "ID_KENDERAAN", "KATEGORI_KEROSAKAN_ID", "KATEGORI_KEROSAKAN", "KATEGORI", "ISSERVICE", "ISPANCIT", "ODOMETER_TERKINI", "NO_PLAT", "MODEL", "KOD_JABATAN", "NAMA_JABATAN", "JUMLAH_KOS") AS 
  SELECT "TAHUN","BULAN","ID","NO_KERJA","ARAHAN_KERJA","JK_JG","BENGKEL_PANEL_ID","NAMA_PANEL","KATEGORI_PANEL","SELENGGARA_ID","TEMPOH_SELENGGARA","ALASAN_ID","ALASAN","TARIKH_ARAHAN","NO_SEBUTHARGA","TARIKH_KENDERAAN_TIBA","TARIKH_JANGKA_SIAP","TARIKH_SEBUTHARGA","STATUS_SEBUTHARGA","NO_DO","TARIKH_SIAP","TEMPOH_JAMINAN","TARIKH_DO","TARIKH_CETAK","STATUS","CATATAN_SEBUTHARGA","CATATAN","NAMA_PIC","TARIKH_PERMOHONAN","ID_KENDERAAN","KATEGORI_KEROSAKAN_ID","KATEGORI_KEROSAKAN","KATEGORI","ISSERVICE","ISPANCIT","ODOMETER_TERKINI","NO_PLAT","MODEL","KOD_JABATAN","NAMA_JABATAN","JUMLAH_KOS" FROM USERSISKEN.LAPORAN_PENYELENGGARAAN_VIEW;

--------------------------------------------------------
--  DDL for View MPSP_STAFF
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."MPSP_STAFF" ("NO_PEKERJA", "NAMA", "JAWATAN", "KETERANGAN") AS 
  SELECT "NO_PEKERJA","NAMA","JAWATAN","KETERANGAN" from payroll.paymas_estor_view;
--------------------------------------------------------
--  DDL for View OI_APP_QUANTITY_CHECK
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."OI_APP_QUANTITY_CHECK" ("ID", "APP_QUANTITY", "ACTUAL_APP_QUANTITY") AS 
  select "ID","APP_QUANTITY","ACTUAL_APP_QUANTITY" from (
select distinct
  oi.id,app_quantity, count(ii.id) over (partition by oi.id) actual_app_quantity
from order_items oi
left join inventory_items ii on oi.id = ii.CHECKOUT_TRANSACTION_ID
)
where app_quantity != actual_app_quantity;
--------------------------------------------------------
--  DDL for View REPORT_IN_QUARTER
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."REPORT_IN_QUARTER" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (
select 
  ii.sku
  ,ii.unit_price
  ,ic.inventory_id
  ,t.check_date
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
from inventory_items ii
left join inventories_checkin ic on ic.id = ii.checkin_transaction_id
left join transactions t on t.id = ic.transaction_id
where ii.deleted = 0
)

SELECT 
  DISTINCT YEAR,
--  rownum as id,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;
--------------------------------------------------------
--  DDL for View REPORT_OUT_QUARTER
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."REPORT_OUT_QUARTER" ("YEAR", "QUARTER", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (
select 
  ii.sku
  ,ii.unit_price
  ,oi.inventory_id
  ,t.check_date
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
from inventory_items ii
left join order_items oi on oi.id = ii.checkout_transaction_id
left join orders o on o.id = oi.order_id
left join transactions t on t.id = o.transaction_id
where ii.deleted = 0 and ii.checkout_transaction_id is not null
)

SELECT 
  DISTINCT YEAR,
--  rownum as id,
  QUARTER,
  COUNT(*) OVER (PARTITION BY YEAR,QUARTER) COUNT,
  SUM(UNIT_PRICE) OVER (PARTITION BY YEAR,QUARTER) TOTAL_PRICE
FROM DATA1
ORDER BY YEAR,QUARTER;
--------------------------------------------------------
--  DDL for View LIST_YEAR
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."LIST_YEAR" ("YEAR", "QUARTER") AS 
  select year,quarter from REPORT_IN_QUARTER union 
select year,quarter from REPORT_OUT_QUARTER;
--------------------------------------------------------
--  DDL for View REPORT_ALL
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."REPORT_ALL" ("ID", "YEAR", "QUARTER", "COUNT_IN", "PRICE_IN", "COUNT_OUT", "PRICE_OUT", "COUNT_CURRENT", "PRICE_CURRENT") AS 
  select
    rownum as id,
    list_year.YEAR,
    list_year.QUARTER,
    NVL(REPORT_IN_QUARTER.COUNT, '0') count_in,
    NVL(REPORT_IN_QUARTER.TOTAL_PRICE, '0') price_in,
    NVL(REPORT_out_QUARTER.COUNT, '0') count_out,
    NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0') price_out,

      sum(NVL(REPORT_IN_QUARTER.COUNT, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(REPORT_OUT_QUARTER.COUNT, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING)
    count_current,
      sum(NVL(REPORT_in_QUARTER.TOTAL_PRICE, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING) - 
      sum(NVL(REPORT_out_QUARTER.TOTAL_PRICE, '0')) over (ORDER BY list_year.YEAR ROWS UNBOUNDED PRECEDING)
    price_current

from list_year
left join REPORT_IN_QUARTER 
on (REPORT_IN_QUARTER.YEAR = list_year.YEAR and list_year.QUARTER = REPORT_IN_QUARTER.QUARTER)
left join REPORT_OUT_QUARTER 
on (list_year.YEAR = REPORT_OUT_QUARTER.YEAR and list_year.QUARTER = REPORT_OUT_QUARTER.QUARTER)
order by list_year.YEAR, list_year.QUARTER;
--------------------------------------------------------
--  DDL for View TEMP_CHECKOUT_PRICE_QUANTITY
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."TEMP_CHECKOUT_PRICE_QUANTITY" ("CHECKIN_TRANSACTION_ID", "CHECKOUT_TRANSACTION_ID", "QUANTITY_BY_PART", "UNIT_PRICE", "PRICE_BY_PART", "QUANTITY_TOTAL", "TOTAL_PRICE") AS 
  select distinct
  CHECKIN_TRANSACTION_ID
  ,CHECKOUT_TRANSACTION_ID
  ,count(*) over (partition by unit_price) quantity_by_part
  ,unit_price
  ,sum(unit_price) over (partition by CHECKIN_TRANSACTION_ID) price_by_part
  ,count(*) over (partition by CHECKOUT_TRANSACTION_ID) quantity_total
  ,sum(unit_price) over (partition by CHECKOUT_TRANSACTION_ID) total_price
from INVENTORY_ITEMS where DELETED = 0;
--------------------------------------------------------
--  DDL for View TRANSACTION_IN
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."TRANSACTION_IN" ("ID", "TYPE", "CHECK_DATE", "CHECK_BY", "REFFERENCE", "APPROVED_DATE", "APPROVED_BY", "INVENTORY_ID", "UNIT_PRICE", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (
select 
  t.id
  ,t.type
  ,ii.sku
  ,ii.unit_price
  ,ic.inventory_id
  ,v.name vendor_name
  ,t.check_date
  ,t.check_by
  ,to_char(ic.approved_at,'dd-MON-yy') approved_date
  ,ic.approved_by
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
  ,COUNT(*) OVER (PARTITION BY t.ID) COUNT
  ,SUM(ii.UNIT_PRICE) OVER (PARTITION BY t.ID) TOTAL_PRICE
from inventory_items ii
left join inventories_checkin ic on ic.id = ii.checkin_transaction_id
left join transactions t on t.id = ic.transaction_id
left join vendors v on v.id = ic.vendor_id
where ii.deleted = 0
)

SELECT 
  ID,
  TYPE,  
  CHECK_DATE,  
  CHECK_BY,  
  VENDOR_NAME REFFERENCE,
  APPROVED_DATE,  
  APPROVED_BY,  
  INVENTORY_ID,
--  rownum as id,
--  QUARTER,
  COUNT,
  UNIT_PRICE,
  TOTAL_PRICE
FROM DATA1
group by id,count,unit_price,total_price, type, check_date, check_by, approved_date, approved_by, inventory_id, vendor_name
ORDER BY id;
--------------------------------------------------------
--  DDL for View TRANSACTION_OUT
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."TRANSACTION_OUT" ("ID", "TYPE", "CHECK_DATE", "CHECK_BY", "REFFERENCE", "APPROVED_DATE", "APPROVED_BY", "INVENTORY_ID", "UNIT_PRICE", "COUNT", "TOTAL_PRICE") AS 
  WITH DATA1 AS (
select 
  t.id
  ,t.type
  ,LISTAGG(ii.sku, ', ') WITHIN GROUP (ORDER BY ii.id) items
--  ,ii.sku
  ,ii.unit_price
  ,oi.inventory_id
  ,t.check_date
  ,t.check_by
  ,to_char(o.approved_at,'dd-MON-yy') approved_date
  ,o.order_no
  ,o.approved_by
  ,extract(year from t.check_date) year
  ,to_char(t.check_date,'Q') quarter 
  ,COUNT(*) OVER (PARTITION BY t.ID) COUNT
  ,SUM(ii.UNIT_PRICE) OVER (PARTITION BY t.ID) TOTAL_PRICE
from inventory_items ii
left join order_items oi on oi.id = ii.checkout_transaction_id
left join orders o on o.id = oi.order_id
left join transactions t on t.id = o.transaction_id
--where ii.deleted = 0 and ii.checkout_transaction_id is not null
  where ii.deleted = 0 and ii.checkout_transaction_id is not null and o.APPROVED = 1
  group by   
    o.id
    ,oi.ID
  --  ,ii.id
    ,ii.UNIT_PRICE
    ,oi.APP_QUANTITY
    ,t.id
  ,t.type
  ,oi.inventory_id
  ,t.check_date
  ,t.check_by
  ,o.order_no
  ,o.approved_by
  ,o.approved_at
  ,t.check_date
  
  order by o.id
)

SELECT 
--  *
  ID,
  TYPE,  
  CHECK_DATE,  
  CHECK_BY,  
  ORDER_NO REFFERENCE,  
  APPROVED_DATE,  
  APPROVED_BY,  
  INVENTORY_ID,
--  rownum as id,
--  QUARTER,
  UNIT_PRICE,
  COUNT,
  TOTAL_PRICE
FROM DATA1
group by id,count,unit_price,total_price, type, check_date, check_by, approved_date, approved_by, inventory_id,order_no
--group by 
ORDER BY id;
--------------------------------------------------------
--  DDL for View TRANSACTIONS_ALL
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."TRANSACTIONS_ALL" ("ID", "TYPE", "CHECK_DATE", "CHECK_BY", "REFFERENCE", "INVENTORY_ID", "UNIT_PRICE", "COUNT_IN", "PRICE_IN", "COUNT_OUT", "PRICE_OUT", "COUNT_CURRENT", "PRICE_CURRENT") AS 
  select 
  t."ID",t."TYPE",t."CHECK_DATE",t."CHECK_BY",t."REFFERENCE",t."INVENTORY_ID",t."UNIT_PRICE",t."COUNT_IN",t."PRICE_IN",t."COUNT_OUT",t."PRICE_OUT",
  sum(COUNT_IN - COUNT_OUT) over(PARTITION BY INVENTORY_ID ORDER BY ID ROWS UNBOUNDED PRECEDING) count_current,
  sum(PRICE_IN - PRICE_OUT) over(PARTITION BY INVENTORY_ID ORDER BY ID ROWS UNBOUNDED PRECEDING) price_current
from (
    select
    --    rownum as id,
        IDS.ID,
        IDS.TYPE,
        IDS.CHECK_DATE,
        IDS.CHECK_BY,
        IDS.REFFERENCE,
        IDS.INVENTORY_ID,
        IDS.UNIT_PRICE,
        case when IDS.TYPE = 1 then IDS.COUNT else 0 end count_in,
        case when IDS.TYPE = 1 then IDS.TOTAL_PRICE else 0 end price_in,
        case when IDS.TYPE = 2 then IDS.COUNT else 0 end count_out,
        case when IDS.TYPE = 2 then IDS.TOTAL_PRICE else 0 end price_out
    from (select * from TRANSACTION_OUT union 
    select * from TRANSACTION_IN
    ) IDS
    order by IDS.ID
) t;
--------------------------------------------------------
--  DDL for View TRANSACTION_OUT_SISKEN
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."TRANSACTION_OUT_SISKEN" ("ID", "ARAHAN_KERJA_ID", "ORDER_NO", "CHECK_DATE", "STATUS", "ORDER_ITEMS_ID", "CATEGORY_NAME", "CARD_NO", "CODE_NO", "INVENTORY_DESCRIPTION", "ITEMS", "QUANTITY", "UNIT_PRICE", "TOTAL_PRICE") AS 
  select rownum as id,t."ARAHAN_KERJA_ID",t."ORDER_NO",t."CHECK_DATE",t."STATUS",t."ORDER_ITEMS_ID",t."CATEGORY_NAME",t."CARD_NO",t."CODE_NO",t."INVENTORY_DESCRIPTION",t."ITEMS",t."QUANTITY",t."UNIT_PRICE",t."TOTAL_PRICE" from
(
select  
    arahan_kerja_id
    ,order_no
    ,check_date
    ,status
    ,order_items_id
    ,category_name
    ,card_no
    ,code_no
    ,inventory_description
    , LISTAGG(sku, ', ') WITHIN GROUP (ORDER BY sku) items
    ,quantity
    ,unit_price
    ,total_price
from
  (
  select
    o.arahan_kerja_id ARAHAN_KERJA_id
    ,o.order_no
    ,to_char(o.approved_at,'dd-MON-yy') check_date
    ,o.approved status
    ,oi.id order_items_id
    ,c.name category_name
    ,i.card_no
    ,i.code_no
    ,i.description inventory_description
    ,ii.sku
    ,ii.unit_price
    ,count(*) over (partition by o.order_no) quantity
    ,sum(ii.unit_price) over (partition by o.order_no) total_price
  from inventory_items ii
  left join order_items oi on oi.id = ii.checkout_transaction_id
  left join orders o on o.id = oi.order_id
  left join transactions t on t.id = o.transaction_id
  left join inventories i on i.id = oi.inventory_id
  left join categories c on c.id = i.category_id
  where ii.checkout_transaction_id is not null 
  and o.arahan_kerja_id is not null
  order by o.order_no desc
  )
group by 
    arahan_kerja_id
    ,order_no
    ,check_date
    ,status
    ,order_items_id
    ,category_name
    ,card_no
    ,code_no
    ,inventory_description
--    ,sku
    , quantity
    , unit_price
    , total_price
order by order_no desc,order_items_id desc
) t;

--------------------------------------------------------
--  DDL for View VEHICLE_REPORT
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."VEHICLE_REPORT" ("REG_NO", "SUM_USAGE") AS 
  select 
--  rownum ID
  vl.REG_NO
--  ,vl.REG_NO
--  ,o.ORDER_NO order_no
--  ,oi.UNIT_PRICE
  ,sum(NVL(oi.UNIT_PRICE, '0')) sum_usage
--  ,sum(NVL(oi.UNIT_PRICE, '0')) over (GROUP BY vl.REG_NO) sum
--  ,sum(NVL(oi.UNIT_PRICE, '0')) over (group by vl.REG_NO) sum1
from VEHICLE_LIST vl
left join orders o on o.VEHICLE_ID = vl.id
left join ORDER_ITEMS oi on oi.order_id = o.id
GROUP BY vl.REG_NO;
--------------------------------------------------------
--  DDL for View VENDORS_REPORT
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "ESTOR"."VENDORS_REPORT" ("ID", "NAME", "TOTAL") AS 
  select rownum as id, t."NAME",t."TOTAL" from (
select 
  v.NAME
  ,SUM (ic.ITEMS_TOTAL_PRICE) total
--  ,vl.REG_NO
--  ,o.ORDER_NO order_no
--  ,oi.UNIT_PRICE
--  ,sum(NVL(oi.UNIT_PRICE, '0')) sum_usage
--  ,sum(NVL(oi.UNIT_PRICE, '0')) over (GROUP BY vl.REG_NO) sum
--  ,sum(NVL(oi.UNIT_PRICE, '0')) over (group by vl.REG_NO) sum1
from VENDORS v
left join inventories_checkin ic on ic.VENDOR_ID = v.id
GROUP BY v.NAME
) t;
