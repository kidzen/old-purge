select LISTAGG(inventory_id, ',') WITHIN GROUP (ORDER BY inventory_id) from (
select 
  inventory_id 
--  ,count_current
from transactions_all
where count_current < 0
group by inventory_id
--order by count_current
)
;
select * from inventories where id in
--(21,41,46,63,65,66,69,78,81,134,149,150,187,188,194,225,230,232,236,237,241,242,248,249,254,256,286,293,296,299,301,311,313,314,376,377,437,441,481,509,522,539,563,566,653,656,661,663,863,885,886,887,888,928)
(
  select 
    inventory_id 
  --  ,count_current
  from transactions_all
  where count_current < 0
  group by inventory_id
)
;
select * from TRANSACTIONS_ALL where inventory_id = 21 
--and id=999
order by check_date,id
;


select 
  t."ID",t."TYPE",t."CHECK_DATE",t."CHECK_BY",t."REFFERENCE",t."INVENTORY_ID",t."UNIT_PRICE",t."COUNT_IN",t."PRICE_IN",t."COUNT_OUT",t."PRICE_OUT",
  sum(COUNT_IN - COUNT_OUT) over(PARTITION BY INVENTORY_ID ORDER BY ID ROWS UNBOUNDED PRECEDING) count_current,
  sum(PRICE_IN - PRICE_OUT) over(PARTITION BY INVENTORY_ID ORDER BY ID ROWS UNBOUNDED PRECEDING) price_current
from (
    select
    --    rownum as id,
        IDS.ID,
        IDS.TYPE,
        IDS.CHECK_DATE,
        IDS.CHECK_BY,
        IDS.REFFERENCE,
        IDS.INVENTORY_ID,
        IDS.UNIT_PRICE,
        case when IDS.TYPE = 1 then IDS.COUNT else 0 end count_in,
        case when IDS.TYPE = 1 then IDS.TOTAL_PRICE else 0 end price_in,
        case when IDS.TYPE = 2 then IDS.COUNT else 0 end count_out,
        case when IDS.TYPE = 2 then IDS.TOTAL_PRICE else 0 end price_out
    from (select * from TRANSACTION_OUT union 
    select * from TRANSACTION_IN
    ) IDS
    order by IDS.ID
) t;


select distinct * from (
select 
  CHECKIN_TRANSACTION_ID
  ,CHECKOUT_TRANSACTION_ID
  ,trunc(ii.CREATED_AT) CREATED_AT
  ,trunc(ii.UPDATED_AT) UPDATED_AT
  ,count(*) over (PARTITION BY ic.INVENTORY_ID ORDER BY ii.created_at ROWS UNBOUNDED PRECEDING) cnt
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.checkin_transaction_id
--where ic.inventory_id = 21
--and CHECKOUT_TRANSACTION_ID is null
order by ii.id
)
--where UPDATED_AT < '22-NOV-16'
;
select * from inventories where id = 21;
select --distinct 
  trunc(ii.created_at)
  ,COUNT(*) OVER (PARTITION BY ii.id ORDER BY ii.id
--    RANGE NUMTODSINTERVAL(1, 'day') PRECEDING
  )
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.checkin_transaction_id
where ic.inventory_id = 21 
--and CHECKOUT_TRANSACTION_ID is null
;

select * from TRANSACTION_IN where inventory_id = 21;
select 
  t.*
  ,
  sum(count) over (partition by inventory_id order by id) c
from TRANSACTION_OUT t where inventory_id = 21
order by id
;
select * from TRANSACTIONS_ALL where inventory_id = 21
and REFFERENCE = '2016/11-143'
;
select * from TRANSACTIONS_ALL order by inventory_id;

select * from inventory_items ;


select * from orders where order_no = '2016/11-143';
select * from order_items where order_id = 369;
select * 
from INVENTORY_ITEMS ii
left join INVENTORIES_CHECKIN ic on ic.id = ii.checkin_transaction_id
where ic.inventory_id = 149 
--and ii.CHECKOUT_TRANSACTION_ID between 532 and 535 
;

select * from transactions where id between 910 and 920 order by created_at;
select * from TRANSACTION_IN where inventory_id = 509;
select * from TRANSACTION_OUT where inventory_id = 509;
select * from TRANSACTIONS_ALL 
--where count_current < 0
where inventory_id in (149,232,311,437,441,509)
--where inventory_id = 149
order by inventory_id
;
select * from orders where order_no = '2016/11-177';
select * from order_items where order_id = 435;
select CODE_NO,DESCRIPTION from inventories where id = 311;


------------------------------------------------------------
------------------------------------------------------------
select sum(count) over (partition by inventory_id order by id rows unbounded PRECEDING) sum,t.* from transaction_in t where inventory_id = 149;
select sum(count) over (partition by inventory_id order by id rows unbounded PRECEDING) sum,t.* from transaction_out t where inventory_id = 149;
select sum(count_current) over (partition by inventory_id order by id rows unbounded PRECEDING) sum,t.* from transactions_all t where inventory_id = 149;
select t.id,t.type,t.CHECK_DATE, u.approved_at from transactions t
left join 
(
select transaction_id,approved_at from inventories_checkin ic union 
select transaction_id,approved_at from  orders o
) u on u.transaction_id = t.id
where t.id in (
  select id from transaction_in where inventory_id = 149 
  union 
  select id from transaction_out where inventory_id = 149
)
order by u.approved_at
;
select * from transactions_all;
  select * from transaction_in where inventory_id = 149 
  union 
  select * from transaction_out where inventory_id = 149
;
select transaction_id,approved_at from inventories_checkin ic union 
select transaction_id,approved_at from  orders o
;


select t.* 
from inventory_items ii
;
from transactions t
left join
;
select transaction_id,approved_at from INVENTORIES_CHECKIN ic union
select transaction_id,approved_at from orders o 
;
left join order_items oi on oi.id = o.id
order by 
;


select 
  id,t.* 
from 
(
select  
    id
    ,arahan_kerja_id
    ,order_no
    ,check_date
    ,status
    ,order_items_id
    ,category_name
    ,card_no
    ,code_no
    ,inventory_description
--    , LISTAGG(sku, ', ') WITHIN GROUP (ORDER BY sku) items
    ,unit_price
    ,batch_quantity
    ,batch_total_price
    ,total_quantity
    ,total_price
from
  (
  select
    t.id
    ,o.arahan_kerja_id ARAHAN_KERJA_id
    ,o.order_no
    ,to_char(o.approved_at,'dd-MON-yy') check_date
    ,o.approved status
    ,oi.id order_items_id
    ,c.name category_name
    ,i.card_no
    ,i.code_no
    ,i.description inventory_description
    ,ii.sku
    ,ii.unit_price
    ,count(*) over (partition by oi.id,ii.unit_price) batch_quantity
    ,sum(ii.unit_price) over (partition by oi.id,ii.unit_price) batch_total_price
    ,count(*) over (partition by o.order_no) total_quantity
    ,sum(ii.unit_price) over (partition by o.order_no) total_price
  from inventory_items ii
  left join order_items oi on oi.id = ii.checkout_transaction_id
  left join orders o on o.id = oi.order_id
  left join transactions t on t.id = o.transaction_id
  left join inventories i on i.id = oi.inventory_id
  left join categories c on c.id = i.category_id
  where ii.checkout_transaction_id is not null 
  order by o.approved_at desc
  )
group by 
    id
    ,arahan_kerja_id
    ,order_no
    ,check_date
    ,status
    ,order_items_id
    ,category_name
    ,card_no
    ,code_no
    ,inventory_description
--    ,sku
    , unit_price
    , batch_quantity
    , batch_total_price
    , total_quantity
    , total_price
order by order_no desc,order_items_id desc
) t;
;
---------------------------------------------------
---------------------------------------------------
select 
  t1.id
  ,t1.type
  ,tt.* 
--  ,sum(quantity)
from transactions t1 
left join 
--from
(
  select 
    out1.*
    ,0 quantity_in
    ,count(*) over (partition by id,inventory_id order by inventory_id rows unbounded preceding) quantity_out
  from (
    select  --distinct  
      t.id
      ,t.type
      ,oi.inventory_id
--      ,0 quantity_in
--      ,count(*) over (partition by oi.inventory_id,ii.UNIT_PRICE order by oi.id rows unbounded preceding) quantity_out
      ,o.approved_at
--    from inventory_items ii
--    left join order_items oi on oi.id = ii.CHECKOUT_TRANSACTION_ID
--    left join orders o on o.id = oi.order_id
--    left join transactions t on t.id = o.transaction_id
--    where ii.CHECKOUT_TRANSACTION_ID is not null 
--    order by o.approved_at
    from transactions t
    left join orders o on t.id = o.transaction_id
    left join order_items oi on o.id = oi.order_ID
    left join inventory_items ii on oi.id = ii.checkout_transaction_id
    where ii.CHECKOUT_TRANSACTION_ID is not null 
    order by o.approved_at
  ) out1
union
  select 
    in1.*
    ,count(*) over (partition by id,inventory_id order by inventory_id rows unbounded preceding) quantity_in
    ,0 quantity_out
  from (
    select  --distinct  
      t.id
      ,t.type
      ,ic.inventory_id
      ,count(*) over (partition by ic.inventory_id order by ic.approved_at rows unbounded preceding) quantity_in
      ,0 quantity_out
      ,ic.approved_at
--    from inventory_items ii
--    left join inventories_checkin ic on ic.id = ii.CHECKOUT_TRANSACTION_ID
--    left join transactions t on t.id = ic.transaction_id
--    order by ic.approved_at
    from transactions t
    left join inventories_checkin ic on ic.transaction_id = t.ID
    left join inventory_items ii on ii.CHECKIN_TRANSACTION_ID = ic.id
--    group by 
--      t.id
--      ,t.type
--      ,ic.inventory_id
--      ,ic.approved_at
    order by ic.approved_at
  ) in1
) tt 
on t1.id = tt.id
--where tt.id is null
order by t1.id--,approved_at
;
---------------------------------------------------
---------------------------------------------------
drop view tin;
create view tin as (
  select distinct
    in1.*
    ,count(*) over (partition by id) quantity_in
    ,0 quantity_out
  from (
    select  --distinct  
      t.id
      ,t.type
      ,ic.inventory_id
--      ,count(*) over (partition by ic.inventory_id order by ic.approved_at rows unbounded preceding) quantity_in2
--      ,0 quantity_out2
      ,ic.approved_at
    from inventory_items ii
    left join inventories_checkin ic on ic.id = ii.CHECKOUT_TRANSACTION_ID
    left join transactions t on t.id = ic.transaction_id
--    order by ic.approved_at
--    from transactions t
--    left join inventories_checkin ic on ic.transaction_id = t.ID
--    left join inventory_items ii on ii.CHECKIN_TRANSACTION_ID = ic.id
--    group by 
--      t.id
--      ,t.type
--      ,ic.inventory_id
--      ,ic.approved_at
    where ii.deleted = 0
    order by ic.approved_at
  ) in1
  order by approved_at
)
;

---------------------------------------------------
---------------------------------------------------
select * from transactions order by id;
select * from inventories_checkin where transaction_id = 3;
select count(*) from inventory_items where checkin_transaction_id = 2;
select * from transaction_in;



select * from transactions_all
--where count_current < 0 
;

CREATE VIEW ITEMS AS SELECT * FROM INVENTORY_ITEMS;

--CREATE VIEW ITEMS2 AS 
SELECT II.ID FROM INVENTORY_ITEMS II
LEFT JOIN INVENTORIES_CHECKIN IC ON IC.ID = II.CHECKIN_TRANSACTION_ID 
WHERE II.DELETED = 0 AND II.ID BETWEEN 1000 AND 9999;

SELECT II.ID FROM ITEMS II
LEFT JOIN INVENTORIES_CHECKIN IC ON IC.ID = II.CHECKIN_TRANSACTION_ID 
WHERE II.DELETED = 0 AND II.ID BETWEEN 1000 AND 9999;

SELECT * FROM ITEMS2;