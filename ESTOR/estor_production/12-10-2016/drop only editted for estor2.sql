DROP TABLE "ACTIVITY_LOGS" cascade constraints;
DROP TABLE "AIRCOND_LIST" cascade constraints;
DROP TABLE "AUTH_ITEM" cascade constraints;
DROP TABLE "AUTH_RULE" cascade constraints;
DROP TABLE "CATEGORIES" cascade constraints;
DROP TABLE "INVENTORIES" cascade constraints;
DROP TABLE "INVENTORIES_CHECKIN" cascade constraints;
DROP TABLE "INVENTORY_ITEMS" cascade constraints;
DROP TABLE "ORDERS" cascade constraints;
DROP TABLE "ORDER_ITEMS" cascade constraints;
DROP TABLE "PACKAGE" cascade constraints;
DROP TABLE "PEOPLE" cascade constraints;
DROP TABLE "ROLES" cascade constraints;
DROP TABLE "TRANSACTIONS" cascade constraints;
DROP TABLE "USAGE_LIST" cascade constraints;
DROP TABLE "VEHICLE_LIST" cascade constraints;
DROP TABLE "VENDORS" cascade constraints;
DROP VIEW "APPROVAL";
DROP VIEW "LIST_YEAR";
DROP VIEW "REPORT_ALL";
DROP VIEW "REPORT_IN_QUARTER";
DROP VIEW "REPORT_OUT_QUARTER";
DROP VIEW "TRANSACTIONS_ALL";
DROP VIEW "TRANSACTION_IN";
DROP VIEW "TRANSACTION_OUT";
DROP VIEW "VEHICLE_REPORT";
DROP VIEW "VENDORS_REPORT";