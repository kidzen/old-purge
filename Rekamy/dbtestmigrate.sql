/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.17 : Database - rekamy
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rekamy` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `rekamy`;

/*Table structure for table `achievement` */

DROP TABLE IF EXISTS `achievement`;

CREATE TABLE `achievement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `score` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `achievement_user_id_fk` (`user_id`),
  KEY `achievement_topic_id_fk` (`topic_id`),
  CONSTRAINT `achievement_topic_id_fk` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`),
  CONSTRAINT `achievement_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `achievement` */

insert  into `achievement`(`id`,`user_id`,`topic_id`,`score`,`comment`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,3,2,'1',NULL,0,NULL,NULL,NULL,NULL,0,NULL),(2,3,2,'1',NULL,0,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `answer_bank` */

DROP TABLE IF EXISTS `answer_bank`;

CREATE TABLE `answer_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `answer_question_id_fk` (`question_id`),
  CONSTRAINT `answer_question_id_fk` FOREIGN KEY (`question_id`) REFERENCES `question_bank` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `answer_bank` */

insert  into `answer_bank`(`id`,`question_id`,`answer`,`type`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,1,'cse1 tpc1 q1 true',1,0,NULL,NULL,NULL,NULL,0,NULL),(2,1,'cse1 tpc1 q1 false 1',0,0,NULL,NULL,NULL,NULL,0,NULL),(3,1,'Choice option no 2 for Q1 false 2',0,0,NULL,NULL,NULL,NULL,0,NULL),(6,2,'This is the right answer for Q2',1,0,NULL,NULL,NULL,NULL,0,NULL),(7,2,'The wrong answer for Q2',0,0,NULL,NULL,NULL,NULL,0,NULL),(8,5,'soalan 5 jawapan betul',1,0,NULL,NULL,NULL,NULL,0,NULL),(9,5,'soalan 5 jawapan salah 1',0,0,NULL,NULL,NULL,NULL,0,NULL),(10,5,'soalan 5 jawapan salah 2',0,0,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `assessment` */

DROP TABLE IF EXISTS `assessment`;

CREATE TABLE `assessment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_answer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag_answered` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assessment_question_id_fk` (`question_id`),
  KEY `assessment_user_id_fk` (`user_id`),
  CONSTRAINT `assessment_question_id_fk` FOREIGN KEY (`question_id`) REFERENCES `question_bank` (`id`),
  CONSTRAINT `assessment_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `assessment` */

insert  into `assessment`(`id`,`question_id`,`user_id`,`user_answer`,`flag_answered`,`type`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (7,2,3,'6',1,1,0,NULL,NULL,NULL,NULL,0,NULL),(8,1,3,'3',0,1,0,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `class_list` */

DROP TABLE IF EXISTS `class_list`;

CREATE TABLE `class_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `class_list` */

/*Table structure for table `course_list` */

DROP TABLE IF EXISTS `course_list`;

CREATE TABLE `course_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `course_list` */

insert  into `course_list`(`id`,`course_name`,`level`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,'Science',NULL,0,NULL,NULL,NULL,NULL,0,NULL),(2,'Mathematics',NULL,0,NULL,NULL,NULL,NULL,0,NULL),(3,'English',NULL,0,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`) values ('m000000_000000_base',1468333489),('m130524_201442_init',1468353744),('m160712_193542_profile',1468357638),('m160712_200924_course_list',1468510122),('m160712_201638_user_course_list',1468510123),('m160712_210131_class_list',1468510123),('m160713_052644_topic_under_course',1468510123),('m160713_052707_question_bank',1468510124),('m160714_051642_achievement',1468971714),('m160714_052648_answer',1468971715),('m160716_164844_assessment',1468971715);

/*Table structure for table `profile` */

DROP TABLE IF EXISTS `profile`;

CREATE TABLE `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `race` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `poscode` int(11) DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted` smallint(6) NOT NULL DEFAULT '0',
  `deleted_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_profile_to_user` (`user_id`),
  CONSTRAINT `fk_profile_to_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `profile` */

insert  into `profile`(`id`,`user_id`,`firstname`,`lastname`,`gender`,`race`,`address`,`state`,`poscode`,`country`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,3,'studentname','studentfather','Male','Malay','NO 23, Jalan 22, Taman Bukit Kuchai,','Selangor',87000,'Malaysia',0,0,0,0,0,0,0),(2,2,'teachername','teacherfather','Female','Indian','','',NULL,'',0,0,0,0,0,0,0),(3,2,'studentname2','studentfather2','Male','Chineese','','',NULL,'',0,0,0,0,0,0,0);

/*Table structure for table `question_bank` */

DROP TABLE IF EXISTS `question_bank`;

CREATE TABLE `question_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) DEFAULT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hint` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_bank_topic_fk` (`topic_id`),
  CONSTRAINT `question_bank_topic_fk` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `question_bank` */

insert  into `question_bank`(`id`,`topic_id`,`question`,`hint`,`type`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,2,'Cse1 Tpc1 Q1?','pick the wright answer',1,0,NULL,NULL,NULL,NULL,0,NULL),(2,2,'cse1 tpc1 Q2?','question no2',1,0,NULL,NULL,NULL,NULL,0,NULL),(3,4,'Question  1 for chapter 2','',1,0,NULL,NULL,NULL,NULL,0,NULL),(4,5,'Science Ch1 Q2?','',1,0,NULL,NULL,NULL,NULL,0,NULL),(5,6,'Topic Soalan 5 Science Soalan 1','',1,0,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `topic` */

DROP TABLE IF EXISTS `topic`;

CREATE TABLE `topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `topic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `topic_course_id_fk` (`course_id`),
  CONSTRAINT `topic_course_id_fk` FOREIGN KEY (`course_id`) REFERENCES `course_list` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `topic` */

insert  into `topic`(`id`,`course_id`,`topic`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (2,1,'Chapter 1',0,NULL,NULL,NULL,NULL,0,NULL),(4,2,'Chapter 2',0,NULL,NULL,NULL,NULL,0,NULL),(5,1,'Chapter 2',0,NULL,NULL,NULL,NULL,0,NULL),(6,1,'Soalan 5',0,NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` smallint(6) DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted` smallint(6) NOT NULL DEFAULT '0',
  `deleted_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`role`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,'admin2','bGoHgLbJDGEel0JJdU2rWtftB0YB_VgU','$2y$13$mKUr5Cjm1t6bkLSKvkRGKe/ZvB.1Otx.L8MM3MlBi6..76yndgOqG',NULL,'admin2@gmail.com',1,0,1468373181,0,1468373181,0,0,0),(2,'teacher','ucEXsLgVNMh578tHXrsRdsEkFm8UfnFX','$2y$13$l8nx/aCJOujeJ9u44bne3O..TGdR8CPKaxA7FJQvMcB4pd1CNArW2',NULL,'teacher@gmail.com',2,0,1468373264,0,1468373264,0,0,0),(3,'student','W6NFyKram8NEwfDaOrA5lHBF5XT2oAXy','$2y$13$BJ/mJWZtIwwkm81yA8lEhOKiU7uDleU2dq.RLOzm6s9A74jYrJBaO',NULL,'student@gmail.com',3,0,1468373292,0,1468373292,0,0,0),(4,'manager','ZeLm5aEWGsqGyYI9KAZK9-tAcK49myfj','$2y$13$VphaSeHUSNJiasD0wckPBeuCUWW90DSNs9qM/oI0BqzLCMepGC6lK',NULL,'manager@gmail.com',4,0,1468373323,0,1468373323,0,0,0),(5,'developer','NpsnFcvUeaOUzOUg8UsPl2eRGN6p4x_L','$2y$13$ss1gF.xUB7Ce8RD2P5YcZ.FVa7qJnD0EKIfRxy5jn2KKm8lIh93ue',NULL,'developer@gmail.com',5,0,1468373362,0,1468373362,0,0,0);

/*Table structure for table `user_course_list` */

DROP TABLE IF EXISTS `user_course_list`;

CREATE TABLE `user_course_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `status` smallint(6) DEFAULT '0',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` smallint(6) DEFAULT '0',
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_course_list_course_list` (`course_id`),
  KEY `fk_user_course_list_user` (`user_id`),
  CONSTRAINT `fk_user_course_list_course_list` FOREIGN KEY (`course_id`) REFERENCES `course_list` (`id`),
  CONSTRAINT `fk_user_course_list_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user_course_list` */

insert  into `user_course_list`(`id`,`user_id`,`course_id`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted`,`deleted_at`) values (1,3,1,0,NULL,NULL,NULL,NULL,0,NULL),(3,3,2,0,NULL,NULL,NULL,NULL,0,NULL),(4,2,1,0,NULL,NULL,NULL,NULL,0,NULL),(6,2,2,0,NULL,NULL,NULL,NULL,0,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
