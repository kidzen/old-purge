insert into "data" (select * from sismek."data");
update "data" set "site" = 'Depo Pembersihan Ampang Jajar  (SPT)' where "id" in (189, 190, 191);

Insert into SISMEK1."category_class_lookup" ("id","name","description","status","created_at","updated_at","created_by","updated_by","deleted","deleted_at") values (1,'Kelas 1','Aircond Kelas 1',1,null,null,1,1,0,null);
Insert into SISMEK1."category_type_lookup" ("id","name","description","status","created_at","updated_at","created_by","updated_by","deleted","deleted_at") values (1,'Jenis 1','Aircond Jenis 1',1,to_timestamp('06-FEB-17 03.39.43.170000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('06-FEB-17 03.39.43.170000000 AM','DD-MON-RR HH.MI.SSXFF AM'),1,1,0,null);
Insert into SISMEK1."category" ("id","class_id","type_id","name","description","status","created_at","updated_at","created_by","updated_by","deleted","deleted_at") values (1,1,1,null,'Aircond',1,to_timestamp('06-FEB-17 03.40.04.951000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('06-FEB-17 03.40.04.951000000 AM','DD-MON-RR HH.MI.SSXFF AM'),1,1,0,null);

------------------------------------------------------------------------------------------------
-- GEOSITE
------------------------------------------------------------------------------------------------
create or replace view "geosite" as (
    select distinct
      "geolocation" "o_geolocation"
      ,"site" "o_site"
      ,"floor" "o_floor"
      ,"location" "o_location"
      ,"id" "o_id"
      ,trim(both from substr(TRIM(TRAILING  from replace("site",'. ','')),0,LENGTH(TRIM(TRAILING  from replace("site",'. ',''))) - 5)) "site"
      ,TRIM(BOTH ')' FROM TRIM(BOTH '(' FROM REGEXP_SUBSTR("site", '\(.*.\)'))) "short_code"
      ,trim(both from "geolocation") "geolocation"
    from "data"
)
;
------------------------------------------------------------------------------------------------
-- GEOLOCATION
------------------------------------------------------------------------------------------------
insert into "geolocation"("short_code","name") 
(
    select distinct
      "geosite"."short_code","geolocation" "name"
    from "geosite"
);
------------------------------------------------------------------------------------------------
-- SITE
------------------------------------------------------------------------------------------------
insert into "site" ("geolocation_id","name") 
(
  select * from (
    select distinct
      gl."id" "geolocation_id"
      ,gs."site" "name"
    from "geosite" gs 
    left join "data" d on gs."o_site" = d."site"
    left join "geolocation" gl on gl."short_code" = gs."short_code"
    order by gl."id",gs."site"
  )
)
;
------------------------------------------------------------------------------------------------
-- FLOOR
------------------------------------------------------------------------------------------------
insert into "floor" ("site_id","name")
(
  select * from (
    select distinct
      s."id" "site_id"
      ,d."floor" "name"
    from "data" d
    left join "geosite" gs on gs."o_site" = d."site"
    left join "site" s on s."name" = gs."site"
    order by s."id",d."floor"
  )
)
;
------------------------------------------------------------------------------------------------
-- LOCATION
------------------------------------------------------------------------------------------------
insert into "location" ("floor_id","name")
(
  select * from ( -- 287
    select distinct
      f."id" as "floor_id"
      ,d."location" "name"
    from "data" d
    left join "geosite" gs 
      on gs."o_floor" = d."floor" 
      and d."site" = gs."o_site" 
      and d."location" = gs."o_location" 
    left join "site" s on s."name" = gs."site"
    left join "floor" f on f."name" = gs."o_floor" and f."site_id" = s."id"
    order by f."id",d."location"
  )
)
;
------------------------------------------------------------------------------------------------
-- ITEM_TYPE
------------------------------------------------------------------------------------------------
insert into "item_type_lookup" ("name") (
  select distinct
    "type"
  from "data"
);
------------------------------------------------------------------------------------------------
-- ITEM_BRAND
------------------------------------------------------------------------------------------------
insert into "item_brand_lookup" ("name") (
  select distinct
    "brand"
  from "data"
);

------------------------------------------------------------------------------------------------
-- ITEM_MODEL
------------------------------------------------------------------------------------------------
insert into "item_model_lookup" ("name") (
  select distinct
    "model" 
  from "data"
);

------------------------------------------------------------------------------------------------
-- ITEM
------------------------------------------------------------------------------------------------
insert into "item" ("category_id","location_id","type","brand","model","horse_power"
,"serial_no","kew_pa") 
(
  select "category_id","location_id","type","brand","model","horse_power","serial_no","kew_pa" from (
    select distinct
      d."id"
      ,cast('1' as NUMBER) "category_id"
      ,l."id" "location_id"
      ,itl."id" "type"
      ,ibl."id" "brand"
      ,iml."id" "model"
      ,d."horse_power" "horse_power"
      ,d."serial_no" "serial_no"
      ,d."kew_pa" "kew_pa"
    from "data" d
    left join "geosite" gs 
      on d."geolocation" = gs."o_geolocation" 
      and d."site" = gs."o_site" 
      and d."floor" = gs."o_floor"   
      and d."location" = gs."o_location" 
    left join "geolocation" gl on gl."name" = gs."geolocation" 
    left join "site" s on s."name" = gs."site" and s."geolocation_id" = gl."id"
    left join "floor" f on f."name" = gs."o_floor" and f."site_id" = s."id"
    left join "location" l on l."name" = gs."o_location" and l."floor_id" = f."id"
    left join "item_type_lookup" itl on itl."name" = d."type"
    left join "item_brand_lookup" ibl on ibl."name" = d."brand"
    left join "item_model_lookup" iml on iml."name" = d."model"
    order by l."id",d."serial_no"
  )  
)
;

create or replace view analytic as 
with all_inventory as (
  select 
    i.*
    ,itl."name" itl_name
    ,ibl."name" ibl_name
    ,iml."name" iml_name
  from "item" i
  left join "item_type_lookup" itl on itl."id" = i."type"
  left join "item_brand_lookup" ibl on ibl."id" = i."brand"
  left join "item_model_lookup" iml on iml."id" = i."model"
)
, count as (
  select distinct
    "id"
    ,ibl_name brand
    ,count(*) over (partition by ibl_name) by_brand 
    ,iml_name model
    ,count(*) over (partition by iml_name) by_model 
    ,itl_name type
    ,count(*) over (partition by itl_name) by_type 
  from all_inventory ai
)

select distinct brand,type,model,by_brand from count
;
commit;
