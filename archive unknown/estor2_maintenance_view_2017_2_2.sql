
--------------------------------------------------------
--              ALL CHECKIN TRANSACTIONS
--------------------------------------------------------

with checkin as (
  select 
  ii.UPDATED_BY ii_UPDATED_BY,ii.CHECKIN_TRANSACTION_ID ii_CHECKIN_TRANSACTION_ID,ii.INVENTORY_ID ii_INVENTORY_ID
  ,ii.DELETED ii_DELETED,ii.SKU ii_SKU,ii.ID ii_ID,ii.CHECKOUT_TRANSACTION_ID ii_CHECKOUT_TRANSACTION_ID
  ,ii.CREATED_BY ii_CREATED_BY,ii.CREATED_AT ii_CREATED_AT,ii.DELETED_AT ii_DELETED_AT,ii.UPDATED_AT ii_UPDATED_AT
  ,ii.UNIT_PRICE ii_UNIT_PRICE
  ,ic.UPDATED_AT ic_UPDATED_AT,ic.APPROVED_AT ic_APPROVED_AT,ic.CREATED_BY ic_CREATED_BY
  ,ic.CREATED_AT ic_CREATED_AT,ic.DELETED ic_DELETED,ic.APPROVED ic_APPROVED,ic.CHECK_BY ic_CHECK_BY
  ,ic.ITEMS_TOTAL_PRICE ic_ITEMS_TOTAL_PRICE,ic.INVENTORY_ID ic_INVENTORY_ID,ic.DELETED_AT ic_DELETED_AT
  ,ic.APPROVED_BY ic_APPROVED_BY,ic.CHECK_DATE ic_CHECK_DATE,ic.UPDATED_BY ic_UPDATED_BY
  ,ic.ITEMS_QUANTITY ic_ITEMS_QUANTITY,ic.VENDOR_ID ic_VENDOR_ID,ic.ID ic_ID,ic.TRANSACTION_ID ic_TRANSACTION_ID
  ,i.APPROVED i_APPROVED,i.APPROVED_BY i_APPROVED_BY,i.CATEGORY_ID i_CATEGORY_ID,i.CREATED_BY i_CREATED_BY
  ,i.CREATED_AT i_CREATED_AT,i.APPROVED_AT i_APPROVED_AT,i.LOCATION i_LOCATION,i.CARD_NO i_CARD_NO,i.DELETED i_DELETED
  ,i.DESCRIPTION i_DESCRIPTION,i.DELETED_AT i_DELETED_AT,i.UPDATED_BY i_UPDATED_BY,i.UPDATED_AT i_UPDATED_AT
  ,i.MIN_STOCK i_MIN_STOCK,i.CODE_NO i_CODE_NO,i.QUANTITY i_QUANTITY,i.ID i_ID,c.DELETED c_DELETED,c.NAME c_NAME
  ,c.UPDATED_BY c_UPDATED_BY,c.CREATED_AT c_CREATED_AT,c.CREATED_BY c_CREATED_BY,c.DELETED_AT c_DELETED_AT,c.ID c_ID
  ,c.UPDATED_AT c_UPDATED_AT,t.CREATED_BY t_CREATED_BY,t.UPDATED_AT t_UPDATED_AT,t.DELETED t_DELETED
  ,t.DELETED_AT t_DELETED_AT,t.UPDATED_BY t_UPDATED_BY,t.CHECK_BY t_CHECK_BY,t.ID t_ID,t.CREATED_AT t_CREATED_AT
  ,t.CHECK_DATE t_CHECK_DATE,t.TYPE t_TYPE
  from inventory_items ii
  left join inventories_checkin ic on ic.id = ii.checkin_transaction_id
  left join transactions t on t.id = ic.transaction_id
  left join inventories i on i.id = ic.inventory_id
  left join categories c on c.id = i.category_id
)
--------------------------------------------------------
--              ALL CHECKOUT TRANSACTIONS
--------------------------------------------------------

, checkout as (
  select 
  ii.UPDATED_BY ii_UPDATED_BY,ii.CHECKIN_TRANSACTION_ID ii_CHECKIN_TRANSACTION_ID,ii.INVENTORY_ID ii_INVENTORY_ID
  ,ii.DELETED ii_DELETED,ii.SKU ii_SKU,ii.ID ii_ID,ii.CHECKOUT_TRANSACTION_ID ii_CHECKOUT_TRANSACTION_ID
  ,ii.CREATED_BY ii_CREATED_BY,ii.CREATED_AT ii_CREATED_AT,ii.DELETED_AT ii_DELETED_AT,ii.UPDATED_AT ii_UPDATED_AT
  ,ii.UNIT_PRICE ii_UNIT_PRICE
  ,oi.UNIT_PRICE oi_UNIT_PRICE,oi.DELETED_AT oi_DELETED_AT,oi.CREATED_AT oi_CREATED_AT
  ,oi.RQ_QUANTITY oi_RQ_QUANTITY,oi.CREATED_BY oi_CREATED_BY,oi.APP_QUANTITY oi_APP_QUANTITY
  ,oi.CURRENT_BALANCE oi_CURRENT_BALANCE,oi.INVENTORY_ID oi_INVENTORY_ID,oi.UPDATED_BY oi_UPDATED_BY
  ,oi.ORDER_ID oi_ORDER_ID,oi.DELETED oi_DELETED,oi.UPDATED_AT oi_UPDATED_AT,oi.ID oi_ID,o.APPROVED o_APPROVED
  ,o.ORDERED_BY o_ORDERED_BY,o.REQUIRED_DATE o_REQUIRED_DATE,o.CREATED_AT o_CREATED_AT,o.APPROVED_BY o_APPROVED_BY
  ,o.ID o_ID,o.DELETED_AT o_DELETED_AT,o.DELETED o_DELETED,o.UPDATED_BY o_UPDATED_BY,o.CHECKOUT_DATE o_CHECKOUT_DATE
  ,o.CREATED_BY o_CREATED_BY,o.CHECKOUT_BY o_CHECKOUT_BY,o.APPROVED_AT o_APPROVED_AT,o.TRANSACTION_ID o_TRANSACTION_ID
  ,o.VEHICLE_ID o_VEHICLE_ID,o.ORDER_NO o_ORDER_NO,o.ORDER_DATE o_ORDER_DATE,o.ARAHAN_KERJA_ID o_ARAHAN_KERJA_ID
  ,o.UPDATED_AT o_UPDATED_AT
  ,i.APPROVED i_APPROVED,i.APPROVED_BY i_APPROVED_BY,i.CATEGORY_ID i_CATEGORY_ID,i.CREATED_BY i_CREATED_BY
  ,i.CREATED_AT i_CREATED_AT,i.APPROVED_AT i_APPROVED_AT,i.LOCATION i_LOCATION,i.CARD_NO i_CARD_NO,i.DELETED i_DELETED
  ,i.DESCRIPTION i_DESCRIPTION,i.DELETED_AT i_DELETED_AT,i.UPDATED_BY i_UPDATED_BY,i.UPDATED_AT i_UPDATED_AT
  ,i.MIN_STOCK i_MIN_STOCK,i.CODE_NO i_CODE_NO,i.QUANTITY i_QUANTITY,i.ID i_ID,c.DELETED c_DELETED,c.NAME c_NAME
  ,c.UPDATED_BY c_UPDATED_BY,c.CREATED_AT c_CREATED_AT,c.CREATED_BY c_CREATED_BY,c.DELETED_AT c_DELETED_AT,c.ID c_ID
  ,c.UPDATED_AT c_UPDATED_AT,t.CREATED_BY t_CREATED_BY,t.UPDATED_AT t_UPDATED_AT,t.DELETED t_DELETED
  ,t.DELETED_AT t_DELETED_AT,t.UPDATED_BY t_UPDATED_BY,t.CHECK_BY t_CHECK_BY,t.ID t_ID,t.CREATED_AT t_CREATED_AT
  ,t.CHECK_DATE t_CHECK_DATE,t.TYPE t_TYPE
  from inventory_items ii
  left join order_items oi on oi.id = ii.checkout_transaction_id
  left join orders o on o.id = oi.order_id
  left join transactions t on t.id = o.transaction_id
  left join inventories i on i.id = oi.inventory_id
  left join categories c on c.id = i.category_id
  where ii.checkout_transaction_id is not null
)
--------------------------------------------------------
-- INVENTORY QUANTITY COLUMN INTEGRIRTY CHECK
--------------------------------------------------------

, i_quantity_check as (
  select * from (
    select distinct
    'int' type,
    'inventories' table_name,
    'quantity' col_name,
    i_id data_id,
    i_QUANTITY fault_value,
    count(*) over (partition by i_id) true_value
    from checkin
    where ii_CHECKOUT_TRANSACTION_ID is null
    )
  where fault_value != true_value
)
--------------------------------------------------------
-- INVENTORIES_CHECKIN ITEMS_TOTAL_PRICE COLUMN INTEGRIRTY CHECK
--------------------------------------------------------

, ic_price_check as (
  select * from (
    select distinct 
    'int' type,
    'inventories_checkin' table_name,
    'items_total_price' col_name,
    ic_id data_id,
    IC_TOTAL_PRICE fault_value,
    sum(ii_unit_price) over (partition by ic_id) true_value  
    from (
      select 
      i_id
      ,ic_id 
      ,i_quantity iv_quantity
      ,ic_ITEMS_QUANTITY ic_quantity
      ,ii_unit_price
      ,ic_ITEMS_TOTAL_PRICE ic_total_price
      from checkin
      where ii_deleted = 0 and i_deleted = 0 and ic_deleted = 0
      )
    )
  where fault_value != true_value
)
--------------------------------------------------------
-- INVENTORIES_CHECKIN ITEMS_QUANTITY COLUMN INTEGRIRTY CHECK
--------------------------------------------------------

, ic_quantity_check as (
  select *
  from (
    select distinct 
    'int' type
    ,'inventories_checkin' table_name
    ,'items_quantity' col_name
    ,ic_id data_id
    ,ic_quantity fault_value
    ,count(*) over (partition by ic_id) true_value
    from (
      select 
      i_id i_id
      ,ic_id ic_id
      ,i_quantity i_quantity
      ,ic_ITEMS_QUANTITY ic_quantity
      ,ii_unit_price
      ,ic_ITEMS_TOTAL_PRICE ic_total_price
      from checkin 
      where ii_deleted = 0 and i_deleted = 0 and ic_deleted = 0
      )
    )
  where fault_value != true_value
)
--------------------------------------------------------
-- MAINTENANCE CHECKOUT SUMMARY (NOT USED FOR MAINTENANCE) 
--------------------------------------------------------

, maintenance_check as (
  select 
  "II_PRICE","II_CHECKOUT_ID","OI_ID","IC_INVENTORY","II_BATCH_QUANTITY_CHECKOUT","OI_QUANTITY_CHECKOUT","OI_BATCH_PRICE","II_BATCH_PRICE" 
  from (
    select 
    ii_unit_price ii_price
    , ii_CHECKOUT_TRANSACTION_ID ii_checkout_id
    , oi_id oi_id
    , oi_INVENTORY_ID ic_inventory
    , COUNT(*) over (PARTITION BY ii_CHECKOUT_TRANSACTION_ID) ii_batch_quantity_checkout
    , oi_APP_QUANTITY oi_quantity_checkout
    , oi_UNIT_PRICE oi_batch_price
    , sum(ii_unit_price) over (partition by ii_CHECKOUT_TRANSACTION_ID) ii_batch_price
    from checkout
    order by ii_updated_at
    )
  where 1=1
  and ii_checkout_id != oi_id
  or oi_quantity_checkout != ii_batch_quantity_checkout
  or oi_batch_price != ii_batch_price
  and ii_checkout_id is not null
)
--------------------------------------------------------
-- INVENTORY_ITEMS CHECKOUT_TRANSACTION_ID COLUMN INTEGRIRTY CHECK (CURRENTLY NOT USED FOR MAINTENANCE)
--------------------------------------------------------

, ii_checkout_id_check as (
  select distinct
  "II_PRICE","II_CHECKOUT_ID","OI_ID","OI_INVENTORY","II_BATCH_QUANTITY_CHECKOUT","OI_QUANTITY_CHECKOUT","OI_BATCH_PRICE","II_BATCH_PRICE" 
  from (
    select 
    ii_unit_price ii_price
    , ii_CHECKOUT_TRANSACTION_ID ii_checkout_id
    , oi_id oi_id
    , oi_INVENTORY_ID oi_inventory
    , COUNT(*) over (PARTITION BY ii_CHECKOUT_TRANSACTION_ID) ii_batch_quantity_checkout
    , oi_APP_QUANTITY oi_quantity_checkout
    , oi_UNIT_PRICE oi_batch_price
    , sum(ii_unit_price) over (partition by ii_CHECKOUT_TRANSACTION_ID) ii_batch_price
    from checkout
    order by ii_updated_at
    )
  where  ii_checkout_id is not null
  and ii_checkout_id != oi_id
  or oi_quantity_checkout != ii_batch_quantity_checkout
  or oi_batch_price != ii_batch_price
)
--------------------------------------------------------
-- ORDER_ITEMS APP_QUANTITY COLUMN INTEGRIRTY CHECK
--------------------------------------------------------

,oi_app_quantity_check as (
  select distinct
  type,table_name,col_name,data_id,fault_value,true_value 
  from (
    select 
    'int' type
    ,'order_items' table_name
    ,'app_quantity' col_name
    ,oi_id data_id
    ,"OI_INVENTORY"
    ,"OI_QUANTITY_CHECKOUT" fault_value
    ,"II_BATCH_QUANTITY_CHECKOUT" true_value
    from (
      select 
      ii_unit_price ii_price, ii_CHECKOUT_TRANSACTION_ID ii_checkout_id, oi_INVENTORY_ID oi_inventory
      ,oi_id
      , COUNT(*) over (PARTITION BY ii_CHECKOUT_TRANSACTION_ID) ii_batch_quantity_checkout
      , oi_APP_QUANTITY oi_quantity_checkout
      from checkout
      order by ii_updated_at
      )
    where ii_checkout_id is not null
    )
  where fault_value != true_value
)
--------------------------------------------------------
-- ORDER_ITEMS UNIT_PRICE COLUMN INTEGRIRTY CHECK
--------------------------------------------------------

,oi_unit_price_check as (
  select distinct
  type,table_name,col_name,data_id,fault_value,true_value 
  from (
    select distinct
    'int' type
    ,'order_items' table_name
    ,'unit_price' col_name
    ,oi_id data_id
    ,"II_PRICE","II_CHECKOUT_ID","OI_INVENTORY"
    ,"OI_BATCH_PRICE" fault_value
    ,"II_BATCH_PRICE" true_value 
    from (
      select
      oi_id,ii_unit_price ii_price, ii_CHECKOUT_TRANSACTION_ID ii_checkout_id, oi_INVENTORY_ID oi_inventory
      , oi_UNIT_PRICE oi_batch_price
      , sum(ii_unit_price) over (partition by ii_CHECKOUT_TRANSACTION_ID) ii_batch_price
      from checkout
      order by ii_updated_at
      )
    where ii_checkout_id is not null
    )
  where fault_value != true_value
)
--------------------------------------------------------
-- ORDERS ORDER_NO COLUMN INTEGRITY CHECK (CURRENTLY NOT USED FOR MAINTENANCE)
--------------------------------------------------------
, o_order_no_check1 as (
  SELECT * FROM (
    SELECT 
    'text' type
    ,'orders' table_name
    ,'order_no' col_name
    ,DATA_ID
    , FAULT_VALUE
--      , CONCAT(CONCAT(CONCAT(CONCAT(YEAR,'/'),MONTH),'-'),CNT) TRUE_VALUE
, new_order_no true_value
FROM (
  SELECT 
  ID DATA_ID
  ,EXTRACT(YEAR FROM CREATED_AT) YEAR
  ,TO_CHAR(CREATED_AT,'mm') MONTH
  ,COUNT(*) OVER (PARTITION BY EXTRACT(YEAR FROM CREATED_AT),EXTRACT(MONTH FROM CREATED_AT) ORDER BY CREATED_AT) CNT
  ,concat(to_char(created_at,'yyyy/mm-'),count(*) OVER (partition by to_char(created_at,'yyyy/mm') ORDER BY created_at)) new_order_no 
  ,ORDER_NO FAULT_VALUE
  ,SUBSTR(ORDER_NO,0,4) SUBSTR
  FROM ORDERS
  )  
) 
  WHERE SUBSTR(FAULT_VALUE,0,4) = 'MPSP'
)
--------------------------------------------------------
-- ORDERS ORDER_NO COLUMN INTEGRITY CHECK 
--------------------------------------------------------

, o_order_no_check2 as (
  SELECT * FROM (
    SELECT 
    'text' type
    ,'orders' table_name
    ,'order_no' col_name
    ,DATA_ID
    , FAULT_VALUE
    , TRUE_VALUE
    FROM (
      SELECT 
      ID DATA_ID
      ,ORDER_NO FAULT_VALUE
      ,SUBSTR(ORDER_NO,6) TRUE_VALUE
      FROM ORDERS
      )  
    ) 
  WHERE SUBSTR(FAULT_VALUE,0,4) = 'MPSP'
)
--------------------------------------------------------
-- ALL COLUMN INTEGRITY CHECK
--------------------------------------------------------

, all_col_check as (
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from ic_quantity_check 
  union all
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from ic_price_check 
  union all
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from i_quantity_check
  union all
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from oi_unit_price_check
  union all
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from oi_app_quantity_check
  union all
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from o_order_no_check2
  union all
  select 
  type,table_name,col_name,data_id
  ,CAST(fault_value AS VARCHAR2(200)) fault_value
  ,CAST(true_value AS VARCHAR2(200)) true_value 
  from o_order_no_check1
)
--------------------------------------------------------
            -- SUMMARY
--------------------------------------------------------
select ROWNUM AS ID,all_col_check.* from all_col_check
;
