-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2017 at 10:48 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mada`
--

-- --------------------------------------------------------

--
-- Table structure for table `lookup_soalan`
--

CREATE TABLE `lookup_soalan` (
  `id` int(10) UNSIGNED NOT NULL,
  `soalan` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bahagian` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lookup_soalan`
--

INSERT INTO `lookup_soalan` (`id`, `soalan`, `bahagian`) VALUES
(1, 'Tempat Bertugas', 'Bahagian A: Maklumat Asas Responden'),
(2, 'Jantina', 'Bahagian A: Maklumat Asas Responden'),
(3, 'Kumpulan Gred', 'Bahagian A: Maklumat Asas Responden'),
(4, 'Umur', 'Bahagian A: Maklumat Asas Responden'),
(5, 'Tempoh Berkhidmat Di MADA', 'Bahagian A: Maklumat Asas Responden'),
(6, 'Saya bersetuju dengan penempatan sekarang', 'Bahagian B : Pengurusan Organisasi'),
(7, 'Arahan penyelia adalah jelas dan mudah difahami', 'Bahagian B : Pengurusan Organisasi'),
(8, 'Pembahagian kerja dilakukan secara adil', 'Bahagian B : Pengurusan Organisasi'),
(9, 'Keperihatinan pengurusan atasan akan kebajikan dan permasalahan warga MADA', 'Bahagian B : Pengurusan Organisasi'),
(10, 'Bidang tugas saya sesuai dengan kelayakan saya', 'Bahagian B : Pengurusan Organisasi'),
(11, 'Peluang untuk melakukan kerja yang berbeza dari semasa ke semasa', 'Bahagian C : Kepuasan Kerja'),
(12, 'Peluang untuk melakukan sesuatu yang menggunakan kebolehan dan kepakaran sendiri', 'Bahagian C : Kepuasan Kerja'),
(13, 'Peluang untuk terus maju dan berjaya dalam kerjaya', 'Bahagian C : Kepuasan Kerja'),
(14, 'Kebolehan pegawai atasan dalam membuat keputusan', 'Bahagian C : Kepuasan Kerja'),
(15, 'Pegawai atasan memberikan galakan dan pujian', 'Bahagian C : Kepuasan Kerja'),
(16, 'Ruang pejabat di bahagian / cawangan saya', 'Bahagian D : Kelengkapan Pejabat'),
(17, 'Bilangan komputer di bahagian / cawangan saya', 'Bahagian D : Kelengkapan Pejabat'),
(18, 'Bilangan pencetak di bahagian / cawangan saya', 'Bahagian D : Kelengkapan Pejabat'),
(19, 'Keadaan perabot (meja, kerusi & lain-lain) di bahagian / cawangan saya', 'Bahagian D : Kelengkapan Pejabat'),
(20, 'Keselesaan berkerja di bahagian / cawangan saya', 'Bahagian D : Kelengkapan Pejabat'),
(21, 'Mempunyai hubungan yang baik dengan penyelia / ketua', 'Bahagian E : Tekanan Di Tempat Kerja'),
(22, 'Mempunyai hubungan yang baik sesama kakitangan yang lain', 'Bahagian E : Tekanan Di Tempat Kerja'),
(23, 'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat', 'Bahagian E : Tekanan Di Tempat Kerja'),
(24, 'Semangat kekitaan di kalangan warga MADA', 'Bahagian E : Tekanan Di Tempat Kerja'),
(25, 'Rakan sekerja sentiasa memberi dorongan dan galakan', 'Bahagian E : Tekanan Di Tempat Kerja'),
(26, 'Mempunyai hubungan yang baik dengan penyelia / ketua', 'Bahagian F : Kemudahan - Kemudahan Lain'),
(27, 'Mempunyai hubungan yang baik sesama kakitangan yang lain', 'Bahagian F : Kemudahan - Kemudahan Lain'),
(28, 'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat', 'Bahagian F : Kemudahan - Kemudahan Lain'),
(29, 'Semangat kekitaan di kalangan warga MADA', 'Bahagian F : Kemudahan - Kemudahan Lain'),
(30, 'Rakan sekerja sentiasa memberi dorongan dan galakan', 'Bahagian F : Kemudahan - Kemudahan Lain'),
(31, 'Berikan pandangan anda tentang Diari MADA yang diterbitkan, dan nyatakan cadangan anda untuk penambahbaikan Diari Mada 2018 yang akan datang', 'Bahagian G : Pandangan Dan Cadangan');

-- --------------------------------------------------------

--
-- Table structure for table `maklumbalas`
--

CREATE TABLE `maklumbalas` (
  `id` int(10) UNSIGNED NOT NULL,
  `userID` int(11) NOT NULL,
  `soalanID` int(11) NOT NULL,
  `maklumbalas` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lookup_soalan`
--
ALTER TABLE `lookup_soalan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maklumbalas`
--
ALTER TABLE `maklumbalas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lookup_soalan`
--
ALTER TABLE `lookup_soalan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `maklumbalas`
--
ALTER TABLE `maklumbalas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
