/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.11 : Database - mada
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mada` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mada`;

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`) values ('m000000_000000_base',1501925683),('m130524_201442_init',1501928699);

/*Table structure for table `personel_information` */

DROP TABLE IF EXISTS `personel_information`;

CREATE TABLE `personel_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL COMMENT 'Tempat Betugas',
  `gender` int(11) DEFAULT NULL COMMENT '1 => male, 2 => female',
  `age` int(11) DEFAULT NULL COMMENT 'Umur',
  `service_duration` int(11) DEFAULT NULL COMMENT 'Tempoh Berkhidmat',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk2` (`user_id`),
  CONSTRAINT `fk2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `personel_information` */

/*Table structure for table `question` */

DROP TABLE IF EXISTS `question`;

CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` smallint(6) DEFAULT NULL COMMENT 'Type : 1 => objective, 2 => subjective, 3 => secret question, 4 => personel info question',
  `section_id` int(11) DEFAULT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk5` (`section_id`),
  CONSTRAINT `fk5` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `question` */

insert  into `question`(`id`,`type`,`section_id`,`question`,`status`,`created_at`,`updated_at`,`deleted_at`) values (1,1,2,'Tempat Bertugas',10,NULL,NULL,NULL),(2,1,2,'Jantina',10,NULL,NULL,NULL),(3,1,2,'Kumpulan Gred',10,NULL,NULL,NULL),(4,1,2,'Umur',10,NULL,NULL,NULL),(5,1,2,'Tempoh Berkhidmat Di MADA',10,NULL,NULL,NULL),(6,1,3,'Saya bersetuju dengan penempatan sekarang',10,NULL,NULL,NULL),(7,1,3,'Arahan penyelia adalah jelas dan mudah difahami',10,NULL,NULL,NULL),(8,1,3,'Pembahagian kerja dilakukan secara adil',10,NULL,NULL,NULL),(9,1,3,'Keperihatinan pengurusan atasan akan kebajikan dan permasalahan warga MADA',10,NULL,NULL,NULL),(10,1,3,'Bidang tugas saya sesuai dengan kelayakan saya',10,NULL,NULL,NULL),(11,1,4,'Peluang untuk melakukan kerja yang berbeza dari semasa ke semasa',10,NULL,NULL,NULL),(12,1,4,'Peluang untuk melakukan sesuatu yang menggunakan kebolehan dan kepakaran sendiri',10,NULL,NULL,NULL),(13,1,4,'Peluang untuk terus maju dan berjaya dalam kerjaya',10,NULL,NULL,NULL),(14,1,4,'Kebolehan pegawai atasan dalam membuat keputusan',10,NULL,NULL,NULL),(15,1,4,'Pegawai atasan memberikan galakan dan pujian',10,NULL,NULL,NULL),(16,1,5,'Ruang pejabat di bahagian / cawangan saya',10,NULL,NULL,NULL),(17,1,5,'Bilangan komputer di bahagian / cawangan saya',10,NULL,NULL,NULL),(18,1,5,'Bilangan pencetak di bahagian / cawangan saya',10,NULL,NULL,NULL),(19,1,5,'Keadaan perabot [meja, kerusi & lain-lain) di bahagian / cawangan saya',10,NULL,NULL,NULL),(20,1,5,'Keselesaan berkerja di bahagian / cawangan saya',10,NULL,NULL,NULL),(21,1,6,'Mempunyai hubungan yang baik dengan penyelia / ketua',10,NULL,NULL,NULL),(22,1,6,'Mempunyai hubungan yang baik sesama kakitangan yang lain',10,NULL,NULL,NULL),(23,1,6,'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat',10,NULL,NULL,NULL),(24,1,6,'Semangat kekitaan di kalangan warga MADA',10,NULL,NULL,NULL),(25,1,6,'Rakan sekerja sentiasa memberi dorongan dan galakan',10,NULL,NULL,NULL),(26,1,7,'Mempunyai hubungan yang baik dengan penyelia / ketua',10,NULL,NULL,NULL),(27,1,7,'Mempunyai hubungan yang baik sesama kakitangan yang lain',10,NULL,NULL,NULL),(28,1,7,'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat',10,NULL,NULL,NULL),(29,1,7,'Semangat kekitaan di kalangan warga MADA',10,NULL,NULL,NULL),(30,1,7,'Rakan sekerja sentiasa memberi dorongan dan galakan',10,NULL,NULL,NULL),(31,2,8,'Berikan pandangan anda tentang Diari MADA yang diterbitkan, dan nyatakan cadangan anda untuk penambahbaikan Diari Mada 2018 yang akan datang',10,NULL,NULL,NULL);

/*Table structure for table `response` */

DROP TABLE IF EXISTS `response`;

CREATE TABLE `response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `response` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk1` (`user_id`),
  KEY `fk3` (`question_id`),
  CONSTRAINT `fk1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk3` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `response` */

/*Table structure for table `response_lookup` */

DROP TABLE IF EXISTS `response_lookup`;

CREATE TABLE `response_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `response` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `positivity_level` smallint(6) DEFAULT NULL COMMENT 'Type : 1 => very bad, 2 => bad, 3 => good, 4 => very good, 5 => best',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk4` (`question_id`),
  CONSTRAINT `fk4` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `response_lookup` */

/*Table structure for table `section` */

DROP TABLE IF EXISTS `section`;

CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `section` */

insert  into `section`(`id`,`name`,`status`,`created_at`,`updated_at`,`deleted_at`) values (1,'Bahagian Soalan Rahsia',10,NULL,NULL,NULL),(2,'Bahagian A: Maklumat Asas Responden',10,NULL,NULL,NULL),(3,'Bahagian B : Pengurusan Organisasi',10,NULL,NULL,NULL),(4,'Bahagian C : Kepuasan Kerja',10,NULL,NULL,NULL),(5,'Bahagian D : Kelengkapan Pejabat',10,NULL,NULL,NULL),(6,'Bahagian E : Tekanan Di Tempat Kerja',10,NULL,NULL,NULL),(7,'Bahagian F : Kemudahan - Kemudahan Lain',10,NULL,NULL,NULL),(8,'Bahagian G : Pandangan Dan Cadangan',10,NULL,NULL,NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remote_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
