/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.11 : Database - sistem74
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sistem74` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sistem74`;

/*Data for the table `id_status` */

insert  into `id_status`(`id`,`kod_status`,`status`,`deleted`,`created_at`,`updated_at`) 
	values 
	(1,'B','Kerosakan memerlukan pembaikan di pasukan',NULL,NULL,NULL),
	(2,'C','Kerosakan memerlukan pembaikan di Workshop JLJ',NULL,NULL,NULL),
	(3,'W','Kenderaan/Peralatan dalam Workshop JLJ',NULL,NULL,NULL),
	(4,'M','Membuat rawatan dan senggaraan servis',NULL,NULL,NULL),
	(5,'X','Pemeriksaan Teknikal atau Bulanan BAT M 60/61',NULL,NULL,NULL),
	(6,'(M)','Rawatan/Senggaraan telah disiapkan',NULL,NULL,NULL),
	(7,'(X)','Pemeriksaan telah disiapkan',NULL,NULL,NULL),
	(8,'O','Kenderaan/Peralatan boleh digerakan',NULL,NULL,NULL),
	(9,'TBG','Kenderaan belum diselenggara',NULL,NULL,NULL),
	(10,'TBW','Kenderaan menghampiri waktu selenggara',NULL,NULL,NULL);

	/*Data for the table `jenis_kenderaan` */

	insert  into `jenis_kenderaan`(`id`,`jenis_kenderaan`,`enjin`,`created_at`,`updated_at`,`created_by`,`updated_by`) values 
		(1,'Motorsikal Kriss','no',NULL,NULL,NULL,NULL),
		(2,'Motosikal Honda 250 cc','no',NULL,NULL,NULL,NULL),
		(3,'Kereta Kls C','no',NULL,NULL,NULL,NULL),
		(4,'Kereta Utiliti','no',NULL,NULL,NULL,NULL),
		(5,'Bas/M 10/13 PN/INOKOM','no',NULL,NULL,NULL,NULL),
		(6,'Bas 24P','no',NULL,NULL,NULL,NULL),
		(7,'Bas 48P','no',NULL,NULL,NULL,NULL),
		(8,'INOKOM Jenazah','no',NULL,NULL,NULL,NULL),
		(9,'MAXUS JENAZAH','no',NULL,NULL,NULL,NULL),
		(10,'Trak 3/4 Ton LR PETROL/DSEL','no',NULL,NULL,NULL,NULL),
		(11,'Trak 3/4 Ton FFR','no',NULL,NULL,NULL,NULL),
		(12,'Trak Perkasa','no',NULL,NULL,NULL,NULL),
		(13,'Trak Isuzu ','no',NULL,NULL,NULL,NULL),
		(14,'TRAK 3 TON HICOM / M/BENZ','no',NULL,NULL,NULL,NULL),
		(15,'Trak 6-10 Ton / 7 Ton TATRA ','no',NULL,NULL,NULL,NULL),
		(16,' DROPS','no',NULL,NULL,NULL,NULL),
		(17,'Water Bowser(10 000 ltr)','no',NULL,NULL,NULL,NULL),
		(18,'BULK REFRUELLER','no',NULL,NULL,NULL,NULL),
		(19,'Trak Refrigerator / FREEZER','no',NULL,NULL,NULL,NULL),
		(20,'RT FORKLIFT','no',NULL,NULL,NULL,NULL),
		(21,'T/Pel. 2 Ton','no',NULL,NULL,NULL,NULL),
		(22,'Dapur Medan','no',NULL,NULL,NULL,NULL),
		(23,'Treler Air 250 gln ','no',NULL,NULL,NULL,NULL),
		(24,'Treler Air 150 Gln','no',NULL,NULL,NULL,NULL),
		(25,'Treler 3/4 Ton','no',NULL,NULL,NULL,NULL),
		(26,'MFDS','no',NULL,NULL,NULL,NULL),
		(27,'Treler Meal On Wheels','no',NULL,NULL,NULL,NULL),
		(28,'Treler Mobile Kitchen','no',NULL,NULL,NULL,NULL),
		(29,'HICOM HANDALAN 2','no',NULL,NULL,NULL,NULL),
		(30,'HICOM HANDALAN 1','no',NULL,NULL,NULL,NULL);

		/*Data for the table `jenis_pegangan` */

		insert  into `jenis_pegangan`(`id`,`jenis_pegangan`,`status`,`deleted`,`created_at`,`updated_at`) values (2,'Tiada',NULL,NULL,NULL,NULL),
			(3,'Pegangan KOMP ANG AM',NULL,NULL,NULL,NULL),
			(4,'Pegangan HQ/BEKALAN',NULL,NULL,NULL,NULL),
			(5,'Pegangan KOMPOSIT',NULL,NULL,NULL,NULL);

			/*Data for the table `jenis_penugasan` */

			insert  into `jenis_penugasan`(`id`,`jenis_penugasan`,`status`,`deleted`,`created_at`,`updated_at`) values (1,'Tiada',NULL,NULL,NULL,NULL),
				(2,'Latihan',NULL,NULL,NULL,NULL),
				(3,'Peluru',NULL,NULL,NULL,NULL),
				(4,'Operasi',NULL,NULL,NULL,NULL),
				(5,'Tadbir',NULL,NULL,NULL,NULL);

				/*Data for the table `jenis_rosak` */

				insert  into `jenis_rosak`(`id`,`jenis_rosak`,`status`,`deleted`,`created_at`,`updated_at`) values (1,'Tiada',NULL,NULL,NULL,NULL),
					(2,'Dalam Platun',NULL,NULL,NULL,NULL),
					(3,'Dalam 211 DBK',NULL,NULL,NULL,NULL);

					/*Data for the table `maklumat_kenderaan` */

					insert  into `maklumat_kenderaan`(`id`,`jenis_kenderaan`,`no_kenderaan`,`perjawatan`,`kod_status`,`km`,`timestamp`,`penugasan`,`pegangan`,`availability`,`catatan`,`kenderaan_rosak`,`status`,`deleted`,`created_at`,`updated_at`) values (117,'HICOM HANDALAN 2','ZB 5671','Perjawatan KOM ANG',0,0,'2016-04-05 16:05:08',NULL,'Pegangan KOMP ANG AM',0,NULL,NULL,'1',NULL,NULL,NULL),
						(120,'HICOM HANDALAN 2','ZB 5681',NULL,0,0,'2016-04-05 16:11:44','Latihan','Pegangan HQ/BEKALAN',0,NULL,NULL,'1',NULL,NULL,NULL),
						(121,'HICOM HANDALAN 1','ZB 5683',NULL,0,0,'2016-04-05 16:09:56',NULL,'Pegangan HQ/BEKALAN',0,NULL,'Dalam Platun','1',NULL,NULL,NULL),
						(126,'HICOM HANDALAN 1','ZB 5759',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(127,'HICOM HANDALAN 1','ZB 5764',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(128,'HICOM HANDALAN 1','ZB 5766',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(129,'HICOM HANDALAN 1','ZB 5768',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(130,'HICOM HANDALAN 1','ZC 8695',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(131,'HICOM HANDALAN 2','ZC 647',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(132,'HICOM HANDALAN 1','ZB 5695',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(133,'HICOM HANDALAN 1','ZB 5700',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(134,'HICOM HANDALAN 1','ZB 5701',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(135,'HICOM HANDALAN 1','ZB 5702',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(136,'HICOM HANDALAN 1','ZB 5706',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(137,'HICOM HANDALAN 1','ZB 5707',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(138,'HICOM HANDALAN 1','ZB 5709',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(139,'HICOM HANDALAN 1','ZB 5711',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(140,'HICOM HANDALAN 2','ZC 1865',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(141,'HICOM HANDALAN 1','ZB 5779',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(142,'HICOM HANDALAN 1','ZB 5788',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(143,'HICOM HANDALAN 1','ZB 5794',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(144,'HICOM HANDALAN 1','ZB 5798',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(145,'HICOM HANDALAN 2','ZB 8646',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(146,'HICOM HANDALAN 2','ZB 8663',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(147,'HICOM HANDALAN 1','ZB 5660',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(148,'HICOM HANDALAN 1','ZB 5662',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(149,'HICOM HANDALAN 1','ZB 5664',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(150,'HICOM HANDALAN 1','ZB 5666',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(151,'HICOM HANDALAN 1','ZB 5667',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(152,'HICOM HANDALAN 2','ZB 6895',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(153,'HICOM HANDALAN 1','ZB 5717',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(154,'HICOM HANDALAN 2','ZC 1866',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(155,'HICOM HANDALAN 1','ZB 5687',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(156,'HICOM HANDALAN 1','ZB 5809',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(157,'HICOM HANDALAN 1','ZB 5810',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(158,'HICOM HANDALAN 1','ZB 5813',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(159,'HICOM HANDALAN 1','ZB 5814',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(160,'HICOM HANDALAN 2','ZB 8660',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(161,'HICOM HANDALAN 2','ZB 8690',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(162,'HICOM HANDALAN 1','ZB 5674',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(163,'HICOM HANDALAN 2','ZC 1861',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(164,'HICOM HANDALAN 2','ZC 1862',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(165,'HICOM HANDALAN 2','ZC 1863',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(166,'HICOM HANDALAN 1','ZB 5815',NULL,0,0,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(170,'HICOM HANDALAN 2','ZZD 1234',NULL,0,NULL,NULL,NULL,'Pegangan HQ/BEKALAN',1,NULL,NULL,'1',NULL,NULL,NULL),
						(171,'Motorsikal Kriss','ZZ123',NULL,0,NULL,'2017-01-19 08:13:49',NULL,'Pegangan KOMP ANG AM',0,NULL,'Dalam 211 DBK','1',NULL,NULL,NULL);

						/*Data for the table `migration` */

						insert  into `migration`(`version`,`apply_time`) values ('m000000_000000_base',1484650639),
							('m130524_201442_init',1484651733);

							/*Data for the table `test1` */

							insert  into `test1`(`id`,`type`,`left_bar`) values (4,'pegangan','komp ang am'),
								(5,NULL,'hq bekal'),
								(6,NULL,'komposit'),
								(7,NULL,'platun'),
								(8,NULL,'211 dbk'),
								(9,NULL,'latihan '),
								(10,NULL,'peluru'),
								(11,NULL,'operasi'),
								(12,NULL,'tadbir');

								/*Data for the table `user` */

								insert  into `user`(`id`,`username`,`email`,`role`,`status`,`password_hash`,`password_reset_token`,`auth_key`,`created_at`,`updated_at`,`created_by`,`updated_by`) values (1,'admin1','admin1@gmail.com','Admin','10','$2y$13$ZjxlnJ6HsBXqWVt4fkhHGOWrnV2SGHRFEBxYnlLwwYzoOnt4jltNO',NULL,'','0000-00-00 00:00:00','0000-00-00 00:00:00','string','string');

									/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
									/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
									/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
									/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
