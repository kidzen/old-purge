select listagg(colname,'') within group (order by colname) list_colname from all_col_name;

--create view analytic as (
with checkin as (
select 
  ii.UPDATED_BY ii_UPDATED_BY,ii.CHECKIN_TRANSACTION_ID ii_CHECKIN_TRANSACTION_ID,ii.INVENTORY_ID ii_INVENTORY_ID
  ,ii.DELETED ii_DELETED,ii.SKU ii_SKU,ii.ID ii_ID,ii.CHECKOUT_TRANSACTION_ID ii_CHECKOUT_TRANSACTION_ID
  ,ii.CREATED_BY ii_CREATED_BY,ii.CREATED_AT ii_CREATED_AT,ii.DELETED_AT ii_DELETED_AT,ii.UPDATED_AT ii_UPDATED_AT
  ,ii.UNIT_PRICE ii_UNIT_PRICE
  ,ic.UPDATED_AT ic_UPDATED_AT,ic.APPROVED_AT ic_APPROVED_AT,ic.CREATED_BY ic_CREATED_BY
  ,ic.CREATED_AT ic_CREATED_AT,ic.DELETED ic_DELETED,ic.APPROVED ic_APPROVED,ic.CHECK_BY ic_CHECK_BY
  ,ic.ITEMS_TOTAL_PRICE ic_ITEMS_TOTAL_PRICE,ic.INVENTORY_ID ic_INVENTORY_ID,ic.DELETED_AT ic_DELETED_AT
  ,ic.APPROVED_BY ic_APPROVED_BY,ic.CHECK_DATE ic_CHECK_DATE,ic.UPDATED_BY ic_UPDATED_BY
  ,ic.ITEMS_QUANTITY ic_ITEMS_QUANTITY,ic.VENDOR_ID ic_VENDOR_ID,ic.ID ic_ID,ic.TRANSACTION_ID ic_TRANSACTION_ID
  ,i.APPROVED i_APPROVED,i.APPROVED_BY i_APPROVED_BY,i.CATEGORY_ID i_CATEGORY_ID,i.CREATED_BY i_CREATED_BY
  ,i.CREATED_AT i_CREATED_AT,i.APPROVED_AT i_APPROVED_AT,i.LOCATION i_LOCATION,i.CARD_NO i_CARD_NO,i.DELETED i_DELETED
  ,i.DESCRIPTION i_DESCRIPTION,i.DELETED_AT i_DELETED_AT,i.UPDATED_BY i_UPDATED_BY,i.UPDATED_AT i_UPDATED_AT
  ,i.MIN_STOCK i_MIN_STOCK,i.CODE_NO i_CODE_NO,i.QUANTITY i_QUANTITY,i.ID i_ID,c.DELETED c_DELETED,c.NAME c_NAME
  ,c.UPDATED_BY c_UPDATED_BY,c.CREATED_AT c_CREATED_AT,c.CREATED_BY c_CREATED_BY,c.DELETED_AT c_DELETED_AT,c.ID c_ID
  ,c.UPDATED_AT c_UPDATED_AT,t.CREATED_BY t_CREATED_BY,t.UPDATED_AT t_UPDATED_AT,t.DELETED t_DELETED
  ,t.DELETED_AT t_DELETED_AT,t.UPDATED_BY t_UPDATED_BY,t.CHECK_BY t_CHECK_BY,t.ID t_ID,t.CREATED_AT t_CREATED_AT
  ,t.CHECK_DATE t_CHECK_DATE,t.TYPE t_TYPE
from inventory_items ii
left join inventories_checkin ic on ic.id = ii.checkin_transaction_id
left join transactions t on t.id = ic.transaction_id
left join inventories i on i.id = ic.inventory_id
left join categories c on c.id = i.category_id
)
, checkout as (
select 
  ii.UPDATED_BY ii_UPDATED_BY,ii.CHECKIN_TRANSACTION_ID ii_CHECKIN_TRANSACTION_ID,ii.INVENTORY_ID ii_INVENTORY_ID
  ,ii.DELETED ii_DELETED,ii.SKU ii_SKU,ii.ID ii_ID,ii.CHECKOUT_TRANSACTION_ID ii_CHECKOUT_TRANSACTION_ID
  ,ii.CREATED_BY ii_CREATED_BY,ii.CREATED_AT ii_CREATED_AT,ii.DELETED_AT ii_DELETED_AT,ii.UPDATED_AT ii_UPDATED_AT
  ,ii.UNIT_PRICE ii_UNIT_PRICE
  ,oi.UNIT_PRICE oi_UNIT_PRICE,oi.DELETED_AT oi_DELETED_AT,oi.CREATED_AT oi_CREATED_AT
  ,oi.RQ_QUANTITY oi_RQ_QUANTITY,oi.CREATED_BY oi_CREATED_BY,oi.APP_QUANTITY oi_APP_QUANTITY
  ,oi.CURRENT_BALANCE oi_CURRENT_BALANCE,oi.INVENTORY_ID oi_INVENTORY_ID,oi.UPDATED_BY oi_UPDATED_BY
  ,oi.ORDER_ID oi_ORDER_ID,oi.DELETED oi_DELETED,oi.UPDATED_AT oi_UPDATED_AT,oi.ID oi_ID,o.APPROVED o_APPROVED
  ,o.ORDERED_BY o_ORDERED_BY,o.REQUIRED_DATE o_REQUIRED_DATE,o.CREATED_AT o_CREATED_AT,o.APPROVED_BY o_APPROVED_BY
  ,o.ID o_ID,o.DELETED_AT o_DELETED_AT,o.DELETED o_DELETED,o.UPDATED_BY o_UPDATED_BY,o.CHECKOUT_DATE o_CHECKOUT_DATE
  ,o.CREATED_BY o_CREATED_BY,o.CHECKOUT_BY o_CHECKOUT_BY,o.APPROVED_AT o_APPROVED_AT,o.TRANSACTION_ID o_TRANSACTION_ID
  ,o.VEHICLE_ID o_VEHICLE_ID,o.ORDER_NO o_ORDER_NO,o.ORDER_DATE o_ORDER_DATE,o.ARAHAN_KERJA_ID o_ARAHAN_KERJA_ID
  ,o.UPDATED_AT o_UPDATED_AT
  ,i.APPROVED i_APPROVED,i.APPROVED_BY i_APPROVED_BY,i.CATEGORY_ID i_CATEGORY_ID,i.CREATED_BY i_CREATED_BY
  ,i.CREATED_AT i_CREATED_AT,i.APPROVED_AT i_APPROVED_AT,i.LOCATION i_LOCATION,i.CARD_NO i_CARD_NO,i.DELETED i_DELETED
  ,i.DESCRIPTION i_DESCRIPTION,i.DELETED_AT i_DELETED_AT,i.UPDATED_BY i_UPDATED_BY,i.UPDATED_AT i_UPDATED_AT
  ,i.MIN_STOCK i_MIN_STOCK,i.CODE_NO i_CODE_NO,i.QUANTITY i_QUANTITY,i.ID i_ID,c.DELETED c_DELETED,c.NAME c_NAME
  ,c.UPDATED_BY c_UPDATED_BY,c.CREATED_AT c_CREATED_AT,c.CREATED_BY c_CREATED_BY,c.DELETED_AT c_DELETED_AT,c.ID c_ID
  ,c.UPDATED_AT c_UPDATED_AT,t.CREATED_BY t_CREATED_BY,t.UPDATED_AT t_UPDATED_AT,t.DELETED t_DELETED
  ,t.DELETED_AT t_DELETED_AT,t.UPDATED_BY t_UPDATED_BY,t.CHECK_BY t_CHECK_BY,t.ID t_ID,t.CREATED_AT t_CREATED_AT
  ,t.CHECK_DATE t_CHECK_DATE,t.TYPE t_TYPE
from inventory_items ii
left join order_items oi on oi.id = ii.checkout_transaction_id
left join orders o on o.id = oi.order_id
left join transactions t on t.id = o.transaction_id
left join inventories i on i.id = oi.inventory_id
left join categories c on c.id = i.category_id
where ii.checkout_transaction_id is not null
)
--, freq_checkin as (
--select COUNT(*) over (partition by inventory_id) freq from checkin
--)
--, freq_checkout as (
--select COUNT(*) over (partition by inventory_id) freq from checkout
--)
--, all_check as (
--select * from freq_checkin
----union select * from checkout_u
--)
, freq_checkout_approved as (
  select distinct
    EXTRACT(year FROM o_approved_at) year
    ,EXTRACT(month FROM o_approved_at) month
    ,i_code_no
    ,i_card_no
    ,i_description
--    ,listagg(o_order_no,',') within group (order by o_order_no) refference
    ,COUNT(*) over (partition by EXTRACT(month FROM o_approved_at),EXTRACT(year FROM o_approved_at),i_id) freq 
  --  ,rank() OVER (partition by EXTRACT(month FROM t_created_at),EXTRACT(year FROM t_created_at) ORDER BY t_created_at) rank
  from checkout
  where o_approved = 1
  order by year,month,freq desc
  --select * from all_check
)
, freq_checkin as (
  select distinct
    EXTRACT(year FROM t_created_at) year
    ,EXTRACT(month FROM t_created_at) month
    ,i_code_no
    ,i_card_no
    ,i_description
    ,COUNT(*) over (partition by EXTRACT(month FROM t_created_at),EXTRACT(year FROM t_created_at),i_id) freq 
  --  ,rank() OVER (partition by EXTRACT(month FROM t_created_at),EXTRACT(year FROM t_created_at) ORDER BY t_created_at) rank
  from checkin
  order by year,month,freq desc
  --select * from all_check
)

select c_id from checkin where ii_sku = '1090211-1116-000000'
--select * from checkout where o_order_no = '2016/11-44'
--select 
--  freq_checkout_approved.*
--  ,dense_rank() over (partition by year, month order by year,month,freq desc) dense_rank
--from freq_checkout_approved
--order by i_code_no desc
--select 
--  ii_id,oi_id,o_id,t_id,i_id,c_id
----  *
----  o_id,oi_inventory_id,i_code_no 
--from checkout 
--where 1=0
--  or ii_id is null
--  or oi_id is null
--  or o_id is null
--  or t_id is null
--  or i_id is null
--  or c_id is null
--    EXTRACT(year FROM t_created_at) =2016
--    and EXTRACT(month FROM t_created_at) = 11
--    and o_approved = 1
--    and o_id = 209
--order by i_code_no desc


--select 
--  freq_checkin.*
--  ,dense_rank() over (partition by year, month order by year,month,freq desc) dense_rank
--from freq_checkin
--order by year,month,freq desc
;

select * from inventories order by id;
select * from order_items where order_id = 209;
select * from orders where id = 209;
select * from analytic;

select * from inventory_items 
left join 
where id = 32402;
update inventories set checkout_transaction_id = null where id =32402;
update order_items set checkout_transaction_id = null where id =241;
update orders set checkout_transaction_id = null where id =209;
update transactions set checkout_transaction_id = null where id =700;

select created_at from transactions where id =700;18-NOV-16 12.00.15.000000000 AM
;
--select * from 
select * from inventories where id=654;
--654	11	A32B	77777777	test	17	0		2			18-NOV-16 12.00.15.000000000 AM	2	25-JAN-17 04.32.26.000000000 AM	1	0	
DELETE FROM inventories WHERE id = 654;
insert into inventories (
  APPROVED,APPROVED_AT,APPROVED_BY,CARD_NO,CATEGORY_ID,CODE_NO,CREATED_AT,CREATED_BY,DELETED
  ,DESCRIPTION,ID,QUANTITY,UPDATED_AT,UPDATED_BY
  ) values (
  1,'25-JAN-17 04.32.26.000000000 AM',2,'DECEASED',11,'DECEASED','18-NOV-16 12.00.15.000000000 AM',2,0
  ,'DECEASED',654,0,'25-JAN-17 04.32.26.000000000 AM',2
  );
select listagg(column_name,',') within group (order by column_name) from all_tab_columns 
where table_name = 'INVENTORIES'
and owner = 'ESTOR'
;

select order_no from orders;



select order_no from orders order by order_no;
--check all orders
with data as (
select
  extract(year from created_at) year
  ,extract(month from created_at) month
  ,TO_CHAR(created_at,'mm') month2
  ,SUBSTR(ORDER_NO,9) sub
  ,max(CAST(SUBSTR(ORDER_NO,9) AS INT)) over (partition by extract(year from created_at),extract(month from created_at) order by created_at) max
  ,count(*) over (partition by extract(year from created_at),extract(month from created_at) order by created_at) cnt
  ,concat(concat(CONCAT(CONCAT(extract(year from created_at), '/'), to_char(created_at,'mm')),'-'),count(*) over (partition by extract(year from created_at),extract(month from created_at) order by created_at)) od2
  ,order_no
  ,created_at 
from orders
)
select * from data
where od2 != order_no
;
--check query
select order_no,concat(od3,cnt) from (
  select 
    id
    ,order_no
    ,concat(concat(CONCAT(CONCAT(extract(year from created_at), '/'), to_char(created_at,'mm')),'-'),count(*) over (partition by extract(year from created_at),extract(month from created_at) order by created_at)) od1
    ,concat(concat(CONCAT(CONCAT(extract(year from created_at), '/'), extract(month from created_at)),'-'),max(CAST(SUBSTR(ORDER_NO,9) AS INT)) over (partition by extract(year from created_at),extract(month from created_at) order by created_at)) od2
    ,concat(CONCAT(CONCAT(extract(year from created_at), '/'), to_char(created_at,'mm')),'-') od3
    ,count(*) over (partition by extract(year from created_at),extract(month from created_at) order by created_at) cnt
  from orders
)
where id = 42;
--update orders

select * from orders where order_no like 'MPSP/%';

select id,order_no from orders where id = 42;
update orders set order_no = 'MPSP/2016/09-3' 
where id = 42;
update orders o1 set o1.order_no = (
  select concat(od,cnt) from (
    select 
      id
      ,order_no
      ,concat(CONCAT(CONCAT(extract(year from created_at), '/'), to_char(created_at,'mm')),'-') od
      ,count(*) over (partition by extract(year from created_at),extract(month from created_at) order by created_at) cnt
    from orders
  ) o2
  where o2.id = o1.id
  )
;

select 
  order_no 
from orders order by created_at
;